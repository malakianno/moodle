<?php
function getCourseListing($is_http_require) {
	if($is_http_require)
		require_once('../config.php');
	$num = 1;
	if($mycourses = enrol_get_my_courses()) {
        	$shown=0;
		$courselisting = "<div id='myfrontpagecourselisting' class='courselisting'>";
        	$courselisting .= "<table>";
        	$courselisting .= "<thead><tr><th>№</th><th>Курс</th><th>Полное название курса</th></tr></thead>";
        	foreach ($mycourses as $mycourse) {
                	if ($mycourse->category) {
                        	context_helper::preload_from_record($mycourse);
                        	$ccontext = context_course::instance($mycourse->id);
                        	$classes = '';
                        	if ($mycourse->visible == 0) {
                                	if (!has_capability('moodle/course:viewhiddencourses', $ccontext)) {
                                        	continue;
                                	}
                                	$class = 'class="dimmed"';
                        	}
                        	if($num % 2 == 0)
                                	$classOddEven = "even";
                        	else $classOddEven = "odd";
                        	$courselisting .= "<tr class=$classOddEven>
                                	<td class='coursenum'>$num.</td>
                                	<td><a href=\"{$CFG->wwwroot}/course/view.php?id={$mycourse->id}\" $class >
                                        	<span class='myshortcoursename'> ".$ccontext->get_context_name(false, true)." </span>
                                	</a></td>
                                	<td><a href=\"{$CFG->wwwroot}/course/view.php?id={$mycourse->id}\" $class >
                                        	<span class='myfullcoursename'>".$ccontext->get_context_name(false)."</span>
                                	</a></td>
                        		</tr> ";
                        	$num++;
                	}
        	}
        	$courselisting .= "</table></div>";
		return $courselisting;
	}
	else {
		return '';
	} 
}

function getFrontPageComboList($is_http_require) {
	if($is_http_require)
                require_once('../config.php');
	$courserenderer = $PAGE->get_renderer('core', 'course');	
	return $courserenderer->frontpage_combo_list();
}
