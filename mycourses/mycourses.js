// Changes the view mode of the main page (my courses)
function changeViewMode() {
	var list = document.getElementById("myfrontpagecourselisting");
	if(!mycoursesplace)
	   return;
	var req = new XMLHttpRequest();
	req.onreadystatechange = function() { 
        	if (req.readyState == 4) {
            		if(req.status == 200) {
                		var div = document.getElementById('mycoursesplace');
				if(div) {
				   div.innerHTML = req.responseText;
				   if(!list)
					document.getElementById("changeViewModeBtn_id").style.backgroundImage = "url('/theme/academi/pix/tree.png')";
				   else document.getElementById("changeViewModeBtn_id").style.backgroundImage = "url('/theme/academi/pix/list.png')"; 
				}
            		}
        	}
    	}
	if(!list)
    	   req.open('GET', '/mycourses/renderer.php?getobj=list', true);
	else 
	   req.open('GET', '/mycourses/renderer.php?getobj=tree', true);
    	req.send(null);
}

function removeClass(obj, cls) {
        var classes = obj.className.split(' ');

        for(i = 0; i < classes.length; i++) {
                if (classes[i] == cls)  {
                        classes.splice(i, 1);
                        i--; // (*)
                }
        }
        obj.className = classes.join(' ');
}
