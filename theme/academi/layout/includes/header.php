<?php global $CFG; ?>
<header id="header">
        <div class="header-top">
                <div class="navbar">
                        <div class="navbar-inner">
                                <div class="container-fluid">
                                	<div class="logo">
                                        	<a href="<?php echo $CFG->wwwroot;?>"><img src="<?php echo get_logo_url(); ?>" width="251" height="37" alt="Academi"></a>
                                        	<a href="<?php echo new moodle_url('/'); ?>">СДО БАГСУ</a>
                                	</div>

					<a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                        </a>
                                        <div class="nav-collapse collapse navbar-responsive-collapse">
                                        	<?php echo $OUTPUT->custom_menu(); ?>
                                        	<ul class="nav pull-left">
							<li><a href="<?php echo new moodle_url('/'); ?>">Мои курсы</a></li>
                                                	<li><a href="<?php echo new moodle_url('/course/index.php'); ?>">Все курсы</a></li>
							<li><a href="<?php echo new moodle_url('/grade/report/overview/index.php'); ?>">Оценки</a></li>
                                              	  	<li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                                                	<?php if($CFG->branch < "28"): ?>
                                                        	<li class="navbar-text"><?php echo $OUTPUT->login_info() ?></li>
                                                	<?php endif; ?>
                                        	</ul>
                                        	<?php if($CFG->branch > "27"): ?>
                                                	<?php echo $OUTPUT->user_menu(); ?>
                                        	<?php endif; ?>
					</div>
                                </div>
                        </div>
                </div>
        </div>
</div>
<div class="header-main">
</div>
</header>
<!--E.O.Header-->
