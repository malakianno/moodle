<?php
$footnote = theme_academi_get_setting('footnote', 'format_html');

$fburl = theme_academi_get_setting('fburl');
$pinurl = theme_academi_get_setting('pinurl');
$twurl = theme_academi_get_setting('twurl');
$gpurl = theme_academi_get_setting('gpurl');

$address = theme_academi_get_setting('address');
$emailid = theme_academi_get_setting('emailid');
$phoneno = theme_academi_get_setting('phoneno');

?>
<footer id="footer">
	<div class="footer-main">
  	<div class="container-fluid">
    	<div class="row-fluid">
      	<div class="span5">
        	<div class="infoarea">
             <?php echo $footnote; ?>
          </div>
        </div>
      	<div class="span3">
        	<div class="foot-links">
          	<h2>Ссылки:</h2>
            <ul>            	
              <li><a href="<?php echo new moodle_url('/'); ?>">Мои курсы</a></li>
	      <li><a href="<?php echo new moodle_url('/course/index.php'); ?>">Все курсы</a></li>
              <li><a href="<?php echo new moodle_url('/grade/report/overview/index.php'); ?>">Оценки</a></li>
	    </ul>
          </div>
        </div>
      	<div class="span4">
          <div class="contact-info">
            <h2 class="nopadding">Контакты:</h2>
            <p><?php echo $address; ?><br>
              <i class="fa fa-phone-square"></i> Тел: <?php echo $phoneno; ?><br>
              <i class="fa fa-envelope"></i> E-mail: <a class="mail-link" href="mailto:<?php echo $emailid; ?>"><?php echo $emailid; ?></a><br>
            </p>
          </div>
          <div class="social-media">
            <ul>
              <li class="smedia-01"><a href="<?php echo $fburl; ?>"><i class="fa fa-facebook-square"></i></a></li>
              <li class="smedia-02"><a href="<?php echo $twurl; ?>"><i class="fa fa-twitter-square"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<!--E.O.Footer-->

<footer>
<?php  echo $OUTPUT->standard_footer_html(); ?>
</footer>
<?php  echo $OUTPUT->standard_end_of_body_html() ?>
