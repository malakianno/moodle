function ShowFullPath()
{
	var fullPath = document.getElementsByClassName("breadcrumb-nav");
	var mobilePath = document.getElementsByClassName("breadcrumb-nav-mobile");
	if(mobilePath.length == 0 || fullPath.length == 0)
		return false;
	mobilePath[0].className += " pathHidden";
	removeClass(fullPath[0], "pathHidden");
	// Разрешаем отображать полный путь к странице при данном разрешении экрана
	removeClass(fullPath[0], "hidden-fullPath");
	return false;
}

function ShowMobilePath()
{       
        var fullPath = document.getElementsByClassName("breadcrumb-nav");
        var mobilePath = document.getElementsByClassName("breadcrumb-nav-mobile");
        if(mobilePath.length == 0 || fullPath.length == 0)
                return false;
	fullPath[0].className += " pathHidden";
        removeClass(mobilePath[0], "pathHidden");
	// Разрешаем отображать свернутый путь к странице при данном разрешении экрана
        removeClass(mobilePath[0], "hidden-mobilePath");
	return false;
}

function removeClass(obj, cls)
{
	var classes = obj.className.split(' ');
 
	for(i = 0; i < classes.length; i++)
	{
		if (classes[i] == cls) 
		{
      			classes.splice(i, 1); // удалить класс  
			i--; // (*)
    		}
  	}
  	obj.className = classes.join(' ');	
}
