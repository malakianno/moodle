<?php
defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2012080900;              // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2012061700;              // Requires this Moodle version
$plugin->component = 'report_detailedreview'; // Full name of the plugin (used for diagnostics)
