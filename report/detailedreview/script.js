function uncon(cid) {
		if (document.getElementById('hours_'+cid).className == 'hidden') {
			undo_close_h(cid);
		} else {
			close_h(cid);
		}
	}


	document.body.onmousedown = function(e)
	  {
			if(  navigator.userAgent.indexOf('IE') != -1 ) {
				e = (e) || window.event;
			}
			var str;
			if (e.button == 2) {
				if(  navigator.userAgent.indexOf('IE') != -1 ) {
					var str = e.srcElement.id;
				} else {
					var str = e.target.id;
				}
				
				if (str.substr(0,5) == "hours")	{ 
					uncon(str.substr(6));
				return false;	
				}
				else return true;
				
			} else return true;
			
			
	   return false;
	  }*/
	
	 
	 var asyncRequest;
	 var CourseId;
	 
	 function close_h(cid)
	 {
	 try
	 {
		 CourseId = cid;
	 asyncRequest = new XMLHttpRequest();
	 asyncRequest.onreadystatechange = closing_stateChange;
	 asyncRequest.open('GET', 'close_h.php?cid='+cid+'&', true);
	 asyncRequest.send(null);
	 }
 
	 catch(exception)
	 {
	 alert('Request Failed.');
	 }
	 }
 
	 function undo_close_h(cid)
	 {
	 try
	 {
		 CourseId = cid;
	 asyncRequest = new XMLHttpRequest();
	 asyncRequest.onreadystatechange = unclosing_stateChange;
	 asyncRequest.open('GET', 'unclose_h.php?cid='+cid+'&', true);
	 asyncRequest.send(null);
	 }
 
	 catch(exception)
	 {
	 alert('Request Failed.');
	 }
	 }
	  
	 function closing_stateChange()
	 {
	 if (asyncRequest.responseText == 'Done!') {
		 document.getElementById('hours_'+CourseId).className='closed';
		 //document.getElementById('hours_'+CourseId).style.color='#777';
		 document.getElementById('course_'+CourseId).className='closed';
		 //document.getElementById('course_'+CourseId).style.color='#777';
		 
	 	}
	 }
 
 	function unclosing_stateChange()
	 {
	 if (asyncRequest.responseText == 'Done!') {
		 document.getElementById('hours_'+CourseId).className='';
		 document.getElementById('hours_'+CourseId).style.color='#06365B';
		 document.getElementById('course_'+CourseId).className='';
		 document.getElementById('course_'+CourseId).style.color='#06365B';
	 }
	 }
  