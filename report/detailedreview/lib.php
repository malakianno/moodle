<?php

function mod_cost($mod) {
	global $DB;
	//берет из базы стоимость одного элемента в часах
	$cost=null;
	$sql = "SELECT cost FROM mdl_hours_cost WHERE type='$mod'";
	$res = $DB->get_records_sql($sql);
	if ($res)
		foreach ($res as $row)	{
		$cost = $row->cost;
	}
	return $cost;
}

function getDateFromClosing($cid) {
	global $DB;
	$res = "0";
	if (is_closed($cid)) {
		$sql = "SELECT closing_time FROM mdl_hours_courses WHERE cid=$cid";
		$time = $DB->get_field_sql($sql);
		
		if ($time > 0) {
			$pre = get_half_year_from_time($time);
			$aft = explode(" ", $pre);
			$res = $aft[1];
		}
	}
	return $res;
}

function get_half_year_from_time($mtime) {
	$month = date("F",$mtime);
	$day = date("d",$mtime);
	if ( (($day>=15) & ($month=="July")) //since 15th July
			|($month=="August")
			|($month=="September")
			|($month=="October")
			|($month=="November")
			|($month=="December")
			|($month=="January")
			|(($day<=15) & ($month=="February"))) //till 15th February
	{
		if (($month=="January")|($month=="February"))
			return "1 ".(date("Y", $mtime)-1)."/".date("Y", $mtime);
		else
			return "1 ".date("Y", $mtime)."/".(date("Y", $mtime)+1);
	}
	else {
		return "2 ".(date("Y", $mtime)-1)."/".date("Y", $mtime);
	}
}

function get_half_year() {
	$month = date("F",round(microtime(true)));
	$day = date("d",round(microtime(true)));
	if ( (($day>=15) & ($month=="July")) //since 15th July
			|($month=="August")
			|($month=="September")
			|($month=="October")
			|($month=="November")
			|($month=="December")
			|($month=="January")
			|(($day<=15) & ($month=="February"))) //till 15th February
	{
		if (($month=="January")|($month=="February"))
			return "1 ".(date("Y", round(microtime(true)))-1)."/".date("Y", round(microtime(true)));
		else
			return "1 ".date("Y", round(microtime(true)))."/".(date("Y", round(microtime(true)))+1);
	}
	else {
		return "2 ".(date("Y", round(microtime(true)))-1)."/".date("Y", round(microtime(true)));
	}
}

function getPlanned($cid) {
	global $DB;
	$plan_db = $DB->get_recordset_sql("SELECT planned, half_year FROM mdl_hours_closing WHERE cid=$cid");
	$planned = "";
	foreach ($plan_db as $db) {
		$planned = $db->planned;
		$half_year = $db->half_year;
	}
	
	if ($planned != "") {
		$plan = array();
		$sc = substr_count($half_year, ' '); 
		if ($sc > 0) {
		$date = explode(" ",$half_year);
		$plan[] = array( 'hours'=>$planned, 'half'=>$date[0], 'year'=>$date[1]);
		}
		else {
			$plan[] = array( 'hours'=>$planned, 'half'=>'012', 'year'=>$half_year);
		}
		
		return $plan;
	}
	else return 0;
}

function setPlanned($cid,$planned,$half_year) {
	global $DB;
	//check if course has planned hours
	if ($DB->get_record_sql("SELECT count(cid) FROM mdl_hours_closing WHERE cid=$cid")->count > 0) {
		$sql="UPDATE mdl_hours_closing SET planned=$planned, half_year='$half_year' WHERE cid=$cid";
		$DB->execute($sql);
	} else {
		$sql="INSERT INTO mdl_hours_closing (cid,planned,half_year) VALUES ($cid, $planned, '$half_year')";
		$DB->execute($sql);
	}
	return "Plan Completed";
}

function close_course($cid, $uid, $comment, $time=null) {
	global $DB;
	if ($time == null) $time = round(microtime(true));
		$sql = "UPDATE mdl_hours_courses SET closed = true, closing_time = $time WHERE cid = $cid";
		$DB->execute($sql);
	//check if course has planned hours
	
		if ($DB->get_record_sql("SELECT COUNT(cid) FROM mdl_hours_closing WHERE cid=$cid")->count > 0) {
			$sql="UPDATE mdl_hours_closing SET closing_userid=$uid, time=$time, comment='$comment' WHERE cid=$cid";
		} else 
		{
			$sql = "INSERT INTO mdl_hours_closing (cid, closing_userid, time, comment) VALUES  ($cid, $uid, $time, '$comment')";
		}
	$DB->execute($sql);
}

function undo_close_course($cid) {
	global $DB;
	$sql = "UPDATE mdl_hours_courses SET closed = false WHERE cid = $cid";
	$DB->execute($sql);
}

function getComment($cid) {
	global $DB;
	$comment_db = $DB->get_recordset_sql("SELECT comment FROM mdl_hours_closing WHERE cid=$cid");
	$comment = "";
	foreach ($comment_db as $cdb)
		$comment = $cdb->comment;
	return $comment;
}

function close_course_hours($cid, $uid, $qty, $time=null, $type='auto') {
	global $DB;
	if ($uid == "") $uid = $USER->id;
	if ($time == null) $time = round(microtime(true));
	//error_log('cid='.$cid.';uid='.$uid.';qty='.$qty.';time='.$time.';type='.$type);
	if (($type == 'stud')|($type == 'method')) {
		$id = $DB->get_field_sql("SELECT MAX(id) FROM mdl_hours_closing_instances");
		if ($id == "") $id = 0; //если в таблице нет записей
		$id++;
		$sql = "INSERT INTO mdl_hours_closing_instances VALUES ($id, $cid, '$type', $qty, $time)";
		//error_log(">>>".$sql."<<<");
		$DB->execute($sql);
		//error_log(">>>GOOD<<<");
		//теперь проверяем можно ли закрыть почасовку по всему курсу
		$done = $DB->get_field_sql("SELECT SUM(quantity) FROM mdl_hours_closing_instances WHERE cid=$cid");
		$plan = $DB->get_field_sql("SELECT DISTINCT(planned) FROM mdl_hours_closing WHERE cid=$cid");
		/*if (($done >= $plan)&(get_course_hours($cid) <= ($done+0.1))) {
			close_course($cid, $uid, $time);
		}*/
	}
	
	if ($type == 'auto') {
		$id = $DB->get_field_sql("SELECT MAX(id) FROM mdl_hours_closing_instances");
		if ($id == "") $id = 0; //если в таблице нет записей
		$id++;
		$plan = $DB->get_field_sql("SELECT DISTINCT(planned) FROM mdl_hours_closing WHERE cid=$cid");
		if ($plan == "") {
			$sql = "INSERT INTO mdl_hours_closing_instances VALUES ($id, $cid, 'stud', $qty, $time)"; //пишем часы
			$DB->execute($sql);
		//	close_course($cid, $uid, $time);
		}
		if (($plan == "")|($plan == 0)) {
			$sql = "INSERT INTO mdl_hours_closing_instances VALUES ($id, $cid, 'method', $qty, $time)"; //пишем часы
			$DB->execute($sql);
			//close_course($cid, $uid, $time);
		}
		else {
			if ($plan >= $qty) { //выполнено меньше чем запланировано
				$sql = "INSERT INTO mdl_hours_closing_instances VALUES ($id, $cid, 'method', $qty, $time)"; //пишем часы
				$DB->execute($sql);
			//	close_course($cid, $uid, $time); //под вопросом, стоит ли закрывать такую почасовку
			}
			else {
				$sql1 = "INSERT INTO mdl_hours_closing_instances VALUES ($id, $cid, 'stud', $plan, $time)"; //пишем часы в учебную нагрузку
				$sql2 = "INSERT INTO mdl_hours_closing_instances VALUES ($id, $cid, 'method', ".$qty-$plan.", $time)"; //пишем часы в учебно-методическую нагрузку
				$DB->execute($sql1);
				$DB->execute($sql2);
			//	close_course($cid, $uid, $time);
			}
		}
		
		
	}
}


function undo_close_course_hours($cid, $type='auto') {
	global $DB;
	if (($type == 'stud')|($type == 'method')) {
		$sql = "DELETE FROM mdl_hours_closing_instances WHERE cid=$cid AND type='$type'";
		$DB->execute($sql);
	}	
	if ($type == 'auto') {
		$sql = "DELETE FROM mdl_hours_closing_instances WHERE cid=$cid";
		$DB->execute($sql);
	}
	
	$sql = "UPDATE mdl_hours_courses SET closed = false, closing_time = NULL WHERE cid = $cid";
	$DB->execute($sql);
}

function getClosedHours($cid, $type='auto') {
	global $DB;
	
	if ($type == 'auto') {
		$sql = "SELECT SUM(quantity) FROM mdl_hours_closing_instances WHERE cid=$cid";
		$res = $DB->get_field_sql($sql);
		if ($res != 0)	 {
			$sql = "SELECT MAX(closing_time) FROM mdl_hours_closing_instances WHERE cid=$cid";
			$res_time = $DB->get_field_sql($sql);
			$result = array();
			$result[] = array('hours'=>$res, 'time'=>$res_time); 
			
			return $result;
		}
		else return 0; 
	}
	if (($type == 'stud')|($type == 'method')) {
		$sql = "SELECT SUM(quantity) FROM mdl_hours_closing_instances WHERE cid=$cid AND type='$type'";
		$res = $DB->get_field_sql($sql);
		if ($res != "") {
			$sql = "SELECT MAX(closing_time) FROM mdl_hours_closing_instances WHERE cid=$cid AND type='$type'";
			$res_time = $DB->get_field_sql($sql);
			$result = array();
			$result[] = array('hours'=>$res, 'time'=>$res_time); 
			
			return $result;
		}
		else return false;
	}
}

function is_closed($cid) {
	global $DB;
	$sql = "SELECT DISTINCT closed FROM mdl_hours_courses WHERE cid=$cid";
	$ret = $DB->get_field_sql($sql);
	if ($ret == "t" ) return true;
	else return false;
}

function get_course_hours($cid) {
	global $DB;
	$sql = "SELECT hours FROM mdl_hours_courses WHERE cid=$cid";
	$hours = $DB->get_field_sql($sql);
	return $hours;
}

function recalc_all_courses($silent=true) {
	global $DB;
	$sql = "SELECT DISTINCT(id) FROM mdl_course";
	$record = $DB->get_records_sql($sql);
	if ($silent) {
	foreach($record as $rec) {
		if (!is_closed($rec->id)) get_course_modules($rec->id, true); //calculating only actual courses
		
	}}
	else {
		foreach($record as $rec) {
			echo 'processing course '.$rec->id;
			if (!is_closed($rec->id)) {
				get_course_modules($rec->id, true); //calculating only actual courses
				echo "...Done!<br>";
			} else {
				echo "...skipped<br>";
			}
		
		}	
	}
	return "Done!";
}

function calc_forums($cid) {
	global $DB;
	// 		начинаем подсчет единиц учета во всех <<<Форумах>>>, которые не были импортированы
	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='forum';"); //catching the module's id

	$sql_forums="SELECT
	(SELECT count(id)
	FROM
	mdl_forum
	WHERE
	course=$cid AND
	mdl_forum.id NOT IN
	(SELECT mod_id FROM mdl_hours_import_log
	WHERE course=$cid AND type = $modid)
	) as new,
	(SELECT count(id)
	FROM
	public.mdl_forum,
	public.mdl_hours_import_log
	WHERE
	mdl_forum.course=$cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_forum.id = mdl_hours_import_log.mod_id AND
	mdl_forum.timemodified < mdl_hours_import_log.import_date) as old,
	(SELECT count(id)
	FROM
	public.mdl_forum,
	public.mdl_hours_import_log
	WHERE
	mdl_forum.course=$cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_forum.id = mdl_hours_import_log.mod_id AND
	mdl_forum.timemodified > mdl_hours_import_log.import_date) as mod,
	(SELECT
	count(mdl_forum_posts.id)
	FROM
	mdl_forum_posts,
	mdl_forum_discussions
	WHERE
	mdl_forum_posts.discussion = mdl_forum_discussions.id AND
	mdl_forum_discussions.course = $cid AND
	mdl_forum_discussions.forum NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid)AND
	mdl_forum_posts.userid IN (SELECT userid FROM mdl_role_assignments
	WHERE contextid IN (
	SELECT id FROM mdl_context
	WHERE instanceid=$cid)
	AND roleid=3)) as msg_prep_new,
	(SELECT
	count(mdl_forum_posts.id)
	FROM
	mdl_forum_posts,
	mdl_forum_discussions
	WHERE
	mdl_forum_posts.discussion = mdl_forum_discussions.id AND
	mdl_forum_discussions.course = $cid AND
	mdl_forum_discussions.forum NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid)AND
	mdl_forum_posts.userid NOT IN (SELECT userid FROM mdl_role_assignments
	WHERE contextid IN (
	SELECT id FROM mdl_context
	WHERE instanceid=$cid)
	AND roleid=3)) as msg_stud_new,
	(SELECT
	count(mdl_forum_posts.id)
	FROM
	mdl_forum_posts,
	mdl_forum_discussions,
	mdl_hours_import_log
	WHERE
	mdl_forum_posts.discussion = mdl_forum_discussions.id AND
	mdl_hours_import_log.course = $cid AND
	mdl_forum_discussions.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_forum_discussions.forum AND
	mdl_hours_import_log.import_date >= mdl_forum_posts.modified AND
	mdl_hours_import_log.import_date >= mdl_forum_posts.created AND
	mdl_forum_posts.userid IN (SELECT userid FROM mdl_role_assignments
	WHERE contextid IN (
	SELECT id FROM mdl_context
	WHERE instanceid=$cid)
	AND roleid=3)) as msg_prep_old,
	(SELECT
	count(mdl_forum_posts.id)
	FROM
	mdl_forum_posts,
	mdl_forum_discussions,
	mdl_hours_import_log
	WHERE
	mdl_forum_posts.discussion = mdl_forum_discussions.id AND
	mdl_hours_import_log.course = $cid AND
	mdl_forum_discussions.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_forum_discussions.forum AND
	mdl_hours_import_log.import_date >= mdl_forum_posts.modified AND
	mdl_hours_import_log.import_date >= mdl_forum_posts.created AND
	mdl_forum_posts.userid NOT IN (SELECT userid FROM mdl_role_assignments
	WHERE contextid IN (
	SELECT id FROM mdl_context
	WHERE instanceid=$cid)
	AND roleid=3)) as msg_stud_old,
	(SELECT
	count(mdl_forum_posts.id)
	FROM
	mdl_forum_posts,
	mdl_forum_discussions,
	mdl_hours_import_log
	WHERE
	mdl_forum_posts.discussion = mdl_forum_discussions.id AND
	mdl_hours_import_log.course = $cid AND
	mdl_forum_discussions.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_forum_discussions.forum AND
	mdl_hours_import_log.import_date < mdl_forum_posts.modified AND
	mdl_hours_import_log.import_date >= mdl_forum_posts.created AND
	mdl_forum_posts.userid IN (SELECT userid FROM mdl_role_assignments
	WHERE contextid IN (
	SELECT id FROM mdl_context
	WHERE instanceid=$cid)
	AND roleid=3)) as msg_prep_mod,
	(SELECT
	count(mdl_forum_posts.id)
	FROM
	mdl_forum_posts,
	mdl_forum_discussions,
	mdl_hours_import_log
	WHERE
	mdl_forum_posts.discussion = mdl_forum_discussions.id AND
	mdl_hours_import_log.course = $cid AND
	mdl_forum_discussions.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_forum_discussions.forum AND
	mdl_hours_import_log.import_date < mdl_forum_posts.modified AND
	mdl_hours_import_log.import_date >= mdl_forum_posts.created AND
	mdl_forum_posts.userid NOT IN (SELECT userid FROM mdl_role_assignments
	WHERE contextid IN (
	SELECT id FROM mdl_context
	WHERE instanceid=$cid)
	AND roleid=3)) as msg_stud_mod";
	$par_quantity=$DB->get_records_sql($sql_forums);
	foreach($par_quantity as $pq) {
		$forums_new = $pq->new;
		$forums_old = $pq->old;
		$forums_mod = $pq->mod;
		$msg_prep_new = $pq->msg_prep_new;
		$msg_prep_old = $pq->msg_prep_old;
		$msg_prep_mod = $pq->msg_prep_mod;
		$msg_stud_new = $pq->msg_stud_new;
		$msg_stud_old = $pq->msg_stud_old;
		$msg_stud_mod = $pq->msg_stud_mod;
	}

	$results = array();
	$results[] = array( 'forums_new'=>$forums_new,
			'forums_old'=>$forums_old,
			'forums_mod'=>$forums_mod,
			'msg_prep_new'=>$msg_prep_new,
			'msg_prep_old'=>$msg_prep_old,
			'msg_prep_mod'=>$msg_prep_mod,
			'msg_stud_new'=>$msg_stud_new,
			'msg_stud_old'=>$msg_stud_old,
			'msg_stud_mod'=>$msg_stud_mod);
	return $results;
}

/*  //функция устарела в связи с переходом к moodle 2.3
 function calc_files($cid) {
global $DB;
//Ищем ресурсы типа "Файл"

$sql_files="
SELECT
(SELECT
		count (mdl_resource_old.id)
		FROM
		mdl_resource_old
		WHERE
		mdl_resource_old.type = 'file' AND
		mdl_resource_old.course = $cid AND
		mdl_resource_old.reference NOT LIKE 'http://%' AND
		reference NOT IN (
				SELECT name
				FROM mdl_hours_files_import_log
				WHERE course=$cid)
) as new,
(SELECT
		count (mdl_resource_old.id)
		FROM
		mdl_hours_files_import_log,
		mdl_resource_old
		WHERE
		mdl_resource_old.type = 'file' AND
		mdl_resource_old.course = $cid AND
		mdl_hours_files_import_log.course =$cid AND
		mdl_resource_old.reference NOT LIKE 'http://%' AND
		mdl_resource_old.reference = mdl_hours_files_import_log.name AND
		mdl_resource_old.timemodified <= mdl_hours_files_import_log.time )as old,
(SELECT
		count (mdl_resource_old.id)
		FROM
		mdl_hours_files_import_log,
		mdl_resource_old
		WHERE
		mdl_resource_old.type = 'file' AND
		mdl_resource_old.course = $cid AND
		mdl_hours_files_import_log.course =$cid AND
		mdl_resource_old.reference NOT LIKE 'http://%' AND
		mdl_resource_old.reference = mdl_hours_files_import_log.name AND
		mdl_resource_old.timemodified > mdl_hours_files_import_log.time ) as modified";
//Ищем ресурсы типа "Ссылка на файл"
$sql_links="
SELECT
(SELECT
		count (mdl_resource_old.id)
		FROM
		mdl_resource_old
		WHERE
		mdl_resource_old.type = 'file' AND
		mdl_resource_old.course = $cid AND
		mdl_resource_old.reference LIKE 'http://%' AND
		reference NOT IN (
				SELECT name
				FROM mdl_hours_files_import_log
				WHERE course=$cid)
) as new,
(SELECT
		count (mdl_resource_old.id)
		FROM
		mdl_hours_files_import_log,
		mdl_resource_old
		WHERE
		mdl_resource_old.type = 'file' AND
		mdl_resource_old.course = $cid AND
		mdl_hours_files_import_log.course =$cid AND
		mdl_resource_old.reference LIKE 'http://%' AND
		mdl_resource_old.reference = mdl_hours_files_import_log.name AND
		mdl_resource_old.timemodified <= mdl_hours_files_import_log.time )as old,
(SELECT
		count (mdl_resource_old.id)
		FROM
		mdl_hours_files_import_log,
		mdl_resource_old
		WHERE
		mdl_resource_old.type = 'file' AND
		mdl_resource_old.course = $cid AND
		mdl_hours_files_import_log.course =$cid AND
		mdl_resource_old.reference LIKE 'http://%' AND
		mdl_resource_old.reference = mdl_hours_files_import_log.name AND
		mdl_resource_old.timemodified > mdl_hours_files_import_log.time ) as modified
";
$files=$DB->get_records_sql($sql_files);
$links=$DB->get_records_sql($sql_links);
//парсим все файлы
foreach($files as $f)	{
$files_new=$f->new;
$files_old=$f->old;
$files_mod=$f->modified;
}
//парсим все ссылки на файлы
foreach($links as $l)	{
$links_new=$l->new;
$links_old=$l->old;
$links_mod=$l->modified;
}
$results = array();
$results[] = array('files_new'=>$files_new,'files_old'=>$files_old, 'files_mod'=>$files_mod, 'links_new'=>$links_new,'links_old'=>$links_old, 'links_mod'=>$links_mod);
return $results;
}
*/

function calc_pages($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='page';"); //catching the module's id

	$sql_pages="SELECT
	(SELECT count(id)
	FROM
	mdl_page
	WHERE course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course=$cid AND type=$modid)) as new,

	(SELECT
	count(id)
	FROM
	mdl_page,
	mdl_hours_import_log
	WHERE
	mdl_page.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_page.id AND
	mdl_page.timemodified <= mdl_hours_import_log.import_date) as old,

	(SELECT
	count(id)
	FROM
	mdl_page,
	mdl_hours_import_log
	WHERE
	mdl_page.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_page.id AND
	mdl_page.timemodified > mdl_hours_import_log.import_date) as mod";
	$pages=$DB->get_records_sql($sql_pages);
	$img_new = 0;
	$href_new = 0;
	$img_mod = 0;
	$href_mod = 0;

	foreach($pages as $p)	{
		$pages_new=$p->new;
		$pages_old=$p->old;
		$pages_mod=$p->mod;
	}

	if ($pages_new > 0) {
		$sql_new_pages="SELECT ROW_NUMBER() OVER(ORDER BY content), content
		FROM
		mdl_page
		WHERE course = $cid AND
		id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course=$cid AND type=$modid)";
		$par=$DB->get_records_sql($sql_new_pages);
		if ($par){
			foreach ($par as $page) //ловим каждую страницу
			{
				$img_new = $img_new+substr_count($page->content,'<img');
				$href_new = $href_new+substr_count($page->content,'href');
			}
		}
	}
	if ($pages_mod > 0) {
		$sql_mod_pages="SELECT
		content
		FROM
		mdl_page,
		mdl_hours_import_log
		WHERE
		mdl_page.course = $cid AND
		mdl_hours_import_log.course = $cid AND
		mdl_hours_import_log.type = $modid AND
		mdl_hours_import_log.mod_id = mdl_page.id AND
		mdl_page.timemodified > mdl_hours_import_log.import_date";
		$par=$DB->get_records_sql($sql_mod_pages);
		//echo $par;
		if ($par){
			foreach ($par as $page) //ловим каждую страницу
			{
				$img_mod = $img_mod+substr_count($page->content,'<img');
				$href_mod = $href_mod+substr_count($page->content,'href');
			}
		}
	}

	$results = array();
	$results[] = array('pages_new'=>$pages_new,'pages_old'=>$pages_old, 'pages_mod'=>$pages_mod, 'img_new'=>$img_new, 'href_new'=>$href_new, 'img_mod'=>$img_mod, 'href_mod'=>$href_mod);
	return $results;
}

/* функция устарела в связи с переходом на moodle 2.3
 function calc_htmlpages($cid) {
global $DB;
$sql_htmls="SELECT
(SELECT count(id)
		FROM
		mdl_resource_old
		WHERE course = $cid AND
		type = 'html' AND
		id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course=$cid AND type=14)) as new,

(SELECT
		count(id)
		FROM
		mdl_resource_old,
		mdl_hours_import_log
		WHERE
		mdl_resource_old.course = $cid AND
		mdl_resource_old.type = 'html' AND
		mdl_hours_import_log.course = $cid AND
		mdl_hours_import_log.mod_id = mdl_resource_old.id AND
		mdl_resource_old.timemodified <= mdl_hours_import_log.import_date) as old,

(SELECT
		count(id)
		FROM
		mdl_resource_old,
		mdl_hours_import_log
		WHERE
		mdl_resource_old.course = $cid AND
		mdl_resource_old.type = 'html' AND
		mdl_hours_import_log.course = $cid AND
		mdl_hours_import_log.mod_id = mdl_resource_old.id AND
		mdl_resource_old.timemodified > mdl_hours_import_log.import_date) as mod";
$htmls=$DB->get_records_sql($sql_htmls);

$img_new = 0;
$href_new = 0;
$img_mod = 0;
$href_mod = 0;

foreach($htmls as $l)	{
$htmls_new=$l->new;
$htmls_old=$l->old;
$htmls_mod=$l->mod;
}
if ($htmls_new > 0) {
$sql_new_htmls="SELECT ROW_NUMBER() OVER(ORDER BY alltext), alltext
FROM
mdl_resource_old
WHERE course = $cid AND
type = 'html' AND
id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course=$cid AND type=14)";
$par=$DB->get_records_sql($sql_new_htmls);
if ($par){
foreach ($par as $page) //ловим каждую страницу
{
$img_new = $img_new+substr_count($page->alltext,'<img');
$href_new = $href_new+substr_count($page->alltext,'href');
}
}
}
if ($htmls_mod > 0) {
$sql_mod_htmls="SELECT
alltext
FROM
mdl_resource_old,
mdl_hours_import_log
WHERE
mdl_resource_old.course = $cid AND
mdl_hours_import_log.course = $cid AND
mdl_resource_old.type = 14 AND
mdl_hours_import_log.mod_id = mdl_resource_old.id AND
mdl_resource_old.timemodified > mdl_hours_import_log.import_date";
$par=$DB->get_records_sql($sql_mod_htmls);
echo $par;
if ($par){
foreach ($par as $page) //ловим каждую страницу
{
$img_mod = $img_mod+substr_count($page->alltext,'<img');
$href_mod = $href_mod+substr_count($page->alltext,'href');
}
}
}

$results = array();
$results[] = array('htmls_new'=>$htmls_new,'htmls_old'=>$htmls_old, 'htmls_mod'=>$htmls_mod, 'img_new'=>$img_new, 'href_new'=>$href_new, 'img_mod'=>$img_mod, 'href_mod'=>$href_mod);
return $results;
}
*/
function calc_folders($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='folder';"); //catching the module's id

	$sql_folders="
	SELECT
	(SELECT
	count (mdl_folder.id)
	FROM
	mdl_folder
	WHERE
	mdl_folder.course = $cid AND
	name NOT IN (
	SELECT name
	FROM mdl_hours_import_log
	WHERE course=$cid AND type=$modid)
	) as new,
	(SELECT
	count (mdl_folder.id)
	FROM
	mdl_hours_import_log,
	mdl_folder
	WHERE
	mdl_folder.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_folder.id = mdl_hours_import_log.mod_id AND
	mdl_folder.timemodified <= mdl_hours_import_log.import_date )as old,
	(SELECT
	count (mdl_folder.id)
	FROM
	mdl_hours_import_log,
	mdl_folder
	WHERE
	mdl_folder.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_folder.id = mdl_hours_import_log.mod_id AND
	mdl_folder.timemodified > mdl_hours_import_log.import_date ) as mod";
	$folders=$DB->get_records_sql($sql_folders);
	foreach($folders as $f)	{
		$folders_new=$f->new;
		$folders_old=$f->old;
		$folders_mod=$f->mod;
	}
	$results = array();
	$results[] = array('folders_new'=>$folders_new,'folders_old'=>$folders_old, 'folders_mod'=>$folders_mod);
	return $results;
}

function calc_opms($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='openmeetings';"); //catching the module's id

	$sql_opm="SELECT
	(SELECT
	count(id)
	FROM
	mdl_openmeetings
	WHERE
	mdl_openmeetings.course = $cid AND
	mdl_openmeetings.id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course=$cid AND type=$modid)) as new,

	(SELECT
	count(id)
	FROM
	mdl_openmeetings,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_openmeetings.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_openmeetings.id AND
	mdl_hours_import_log.import_date >= mdl_openmeetings.timemodified) as old,
	(SELECT
	count(id)
	FROM
	mdl_openmeetings,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_openmeetings.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_openmeetings.id AND
	mdl_hours_import_log.import_date < mdl_openmeetings.timemodified) as mod";
	$opms=$DB->get_records_sql($sql_opm);
	foreach($opms as $o)	{
		$opms_new=$o->new;
		$opms_old=$o->old;
		$opms_mod=$o->mod;
	}
	$results = array();
	$results[] = array('opms_new'=>$opms_new,'opms_old'=>$opms_old, 'opms_mod'=>$opms_mod);
	return $results;
}

function calc_scorms($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='scorm';"); //catching the module's id

	$sql_scorms="SELECT
	(SELECT count(id)
	FROM mdl_scorm
	WHERE course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type=$modid)) as new,
	(SELECT
	count(id)
	FROM
	mdl_scorm,
	mdl_hours_import_log
	WHERE
	mdl_scorm.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_scorm.id = mdl_hours_import_log.mod_id AND
	mdl_scorm.timemodified <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(id)
	FROM
	mdl_scorm,
	mdl_hours_import_log
	WHERE
	mdl_scorm.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_scorm.id = mdl_hours_import_log.mod_id AND
	mdl_scorm.timemodified > mdl_hours_import_log.import_date) as mod";
	$scorms=$DB->get_records_sql($sql_scorms);
	foreach($scorms as $s)	{
		$scorms_new=$s->new;
		$scorms_old=$s->old;
		$scorms_mod=$s->mod;
	}
	$results = array();
	$results[] = array('scorms_new'=>$scorms_new,'scorms_old'=>$scorms_old, 'scorms_mod'=>$scorms_mod);
	return $results;
}

function calc_choices($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='choice';"); //catching the module's id

	$sql_choices="SELECT
	(SELECT count(id)
	FROM mdl_choice
	WHERE course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type=$modid)) as new,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_choice
	WHERE
	mdl_choice.id = mdl_hours_import_log.mod_id AND
	mdl_choice.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_choice.timemodified <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_choice
	WHERE
	mdl_choice.id = mdl_hours_import_log.mod_id AND
	mdl_choice.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_choice.timemodified > mdl_hours_import_log.import_date) as mod";
	$choices=$DB->get_records_sql($sql_choices);
	foreach($choices as $s)	{
		$choices_new=$s->new;
		$choices_old=$s->old;
		$choices_mod=$s->mod;
	}
	$results = array();
	$results[] = array('choices_new'=>$choices_new,'choices_old'=>$choices_old, 'choices_mod'=>$choices_mod);
	return $results;
}

function calc_datas($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='data';"); //catching the module's id

	$sql_datas="SELECT
	(SELECT count(id)
	FROM mdl_data
	WHERE course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type=$modid)) as new,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_data
	WHERE
	mdl_data.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_data.id = mdl_hours_import_log.mod_id AND
	mdl_data.timeavailablefrom <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_data
	WHERE
	mdl_data.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_data.id = mdl_hours_import_log.mod_id AND
	mdl_data.timeavailablefrom > mdl_hours_import_log.import_date) as mod";
	$datas=$DB->get_records_sql($sql_datas);
	foreach($datas as $d)	{
		$datas_new=$d->new;
		$datas_old=$d->old;
		$datas_mod=$d->mod;
	}
	$results = array();
	$results[] = array('datas_new'=>$datas_new,'datas_old'=>$datas_old, 'datas_mod'=>$datas_mod);
	return $results;
}

function calc_surveys($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='survey';"); //catching the module's id

	$sql_surveys="SELECT
	(SELECT count(id)
	FROM mdl_survey
	WHERE course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type=$modid)) as new,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_survey
	WHERE
	mdl_survey.id = mdl_hours_import_log.mod_id AND
	mdl_survey.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_survey.timemodified <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_survey
	WHERE
	mdl_survey.id = mdl_hours_import_log.mod_id AND
	mdl_survey.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_survey.timemodified > mdl_hours_import_log.import_date) as mod";
	$surveys=$DB->get_records_sql($sql_surveys);
	foreach($surveys as $d)	{
		$surveys_new=$d->new;
		$surveys_old=$d->old;
		$surveys_mod=$d->mod;
	}
	$results = array();
	$results[] = array('surveys_new'=>$surveys_new,'surveys_old'=>$surveys_old, 'surveys_mod'=>$surveys_mod);
	return $results;
}
//должна быть  отключена в связи с переходом к 2.3
function calc_questionnaires($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='questionnaire';"); //catching the module's id

	$sql_questionnaire="SELECT
	(SELECT count(id)
	FROM mdl_questionnaire
	WHERE course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type=$modid)) as new,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_questionnaire
	WHERE
	mdl_questionnaire.id = mdl_hours_import_log.mod_id AND
	mdl_questionnaire.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_questionnaire.timemodified <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_questionnaire
	WHERE
	mdl_questionnaire.id = mdl_hours_import_log.mod_id AND
	mdl_questionnaire.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_questionnaire.timemodified > mdl_hours_import_log.import_date) as mod";
	$questionnaire=$DB->get_records_sql($sql_questionnaire);
	foreach($questionnaire as $d)	{
		$questionnaire_new=$d->new;
		$questionnaire_old=$d->old;
		$questionnaire_mod=$d->mod;
	}
	$results = array();
	$results[] = array('questionnaires_new'=>$questionnaire_new,'questionnaires_old'=>$questionnaire_old, 'questionnaires_mod'=>$questionnaire_mod);
	return $results;
}


function calc_workshops($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='workshop';"); //catching the module's id

	$sql_workshops="SELECT
	(SELECT count(id)
	FROM mdl_workshop
	WHERE course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type=$modid)) as new,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_workshop
	WHERE
	mdl_workshop.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_workshop.id = mdl_hours_import_log.mod_id AND
	mdl_workshop.timemodified <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_workshop
	WHERE
	mdl_workshop.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_workshop.id = mdl_hours_import_log.mod_id AND
	mdl_workshop.timemodified > mdl_hours_import_log.import_date) as mod";
	$workshops=$DB->get_records_sql($sql_workshops);
	foreach($workshops as $d)	{
		$workshops_new=$d->new;
		$workshops_old=$d->old;
		$workshops_mod=$d->mod;
	}
	$results = array();
	$results[] = array('workshops_new'=>$workshops_new,'workshops_old'=>$workshops_old, 'workshops_mod'=>$workshops_mod);
	return $results;
}
//должна быть отключена в связи с переходом к версии 2.3
function calc_journals($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='journal';"); //catching the module's id

	$sql_journals="SELECT
	(SELECT count(id)
	FROM mdl_journal
	WHERE course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type=$modid )) as new,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_journal
	WHERE
	mdl_journal.id = mdl_hours_import_log.mod_id AND
	mdl_journal.course = $cid AND
	mdl_hours_import_log.type = $modid  AND
	mdl_hours_import_log.course = $cid AND
	mdl_journal.timemodified <= mdl_hours_import_log.import_date
	) as old,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_journal
	WHERE
	mdl_journal.id = mdl_hours_import_log.mod_id AND
	mdl_journal.course = $cid AND
	mdl_hours_import_log.type = $modid  AND
	mdl_hours_import_log.course = $cid AND
	mdl_journal.timemodified > mdl_hours_import_log.import_date
	) as mod";
	$journals=$DB->get_records_sql($sql_journals);
	foreach($journals as $d)	{
		$journals_new=$d->new;
		$journals_old=$d->old;
		$journals_mod=$d->mod;
	}
	$results = array();
	$results[] = array('journals_new'=>$journals_new,'journals_old'=>$journals_old, 'journals_mod'=>$journals_mod);
	return $results;
}



function calc_lessons($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='lesson';"); //catching the module's id

	//для начала берем все количества без подсчета картинок и ссылок
	$sql_lessons="SELECT
	(SELECT count(id)
	FROM mdl_lesson
	WHERE
	course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course=$cid AND type=$modid)) as new,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_lesson
	WHERE
	mdl_hours_import_log.mod_id = mdl_lesson.id AND
	mdl_lesson.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_lesson.timemodified <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(DISTINCT mdl_lesson.id)
	FROM
	mdl_hours_import_log,
	mdl_lesson
	WHERE
	mdl_hours_import_log.mod_id = mdl_lesson.id AND
	mdl_lesson.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_lesson.timemodified > mdl_hours_import_log.import_date) as mod,
	((SELECT
	count(mdl_lesson_pages.id)
	FROM
	mdl_lesson,
	mdl_lesson_pages
	WHERE
	mdl_lesson.course = $cid AND
	mdl_lesson_pages.lessonid = mdl_lesson.id AND
	mdl_lesson_pages.qtype > 10 AND
	mdl_lesson.id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE type=$modid AND course = $cid)) +
	(SELECT
	count(mdl_lesson_pages.id)
	FROM
	mdl_lesson,
	mdl_lesson_pages,
	mdl_hours_import_log
	WHERE
	mdl_lesson.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_lesson_pages.lessonid = mdl_lesson.id AND
	mdl_lesson_pages.qtype > 10 AND
	mdl_lesson_pages.timecreated > mdl_hours_import_log.import_date AND
	mdl_hours_import_log.mod_id = mdl_lesson.id)
	) as new_pgs,
	(SELECT
	count(mdl_lesson_pages.id)
	FROM
	mdl_lesson,
	mdl_lesson_pages,
	mdl_hours_import_log
	WHERE
	mdl_lesson.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_lesson_pages.lessonid = mdl_lesson.id AND
	mdl_lesson_pages.qtype > 10 AND
	mdl_lesson_pages.timemodified <= mdl_hours_import_log.import_date AND
	mdl_hours_import_log.mod_id = mdl_lesson.id) as old_pgs,
	(SELECT
	count(mdl_lesson_pages.id)
	FROM
	mdl_lesson,
	mdl_lesson_pages,
	mdl_hours_import_log
	WHERE
	mdl_lesson.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_lesson_pages.lessonid = mdl_lesson.id AND
	mdl_lesson_pages.qtype > 10 AND
	mdl_lesson_pages.timemodified > mdl_hours_import_log.import_date AND
	mdl_hours_import_log.mod_id = mdl_lesson.id) as mod_pgs,
	((SELECT
	count(mdl_lesson_pages.id)
	FROM
	mdl_lesson,
	mdl_lesson_pages
	WHERE
	mdl_lesson.course = $cid AND
	mdl_lesson_pages.lessonid = mdl_lesson.id AND
	mdl_lesson_pages.qtype <= 10 AND
	mdl_lesson.id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE type=$modid AND course = $cid)) +
	(SELECT
	count(mdl_lesson_pages.id)
	FROM
	mdl_lesson,
	mdl_lesson_pages,
	mdl_hours_import_log
	WHERE
	mdl_lesson.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_lesson_pages.qtype <= 10 AND
	mdl_lesson_pages.lessonid = mdl_lesson.id AND
	mdl_lesson_pages.timecreated > mdl_hours_import_log.import_date AND
	mdl_hours_import_log.mod_id = mdl_lesson.id)
	) as new_pgs_wq,
	(SELECT
	count(mdl_lesson_pages.id)
	FROM
	mdl_lesson,
	mdl_lesson_pages,
	mdl_hours_import_log
	WHERE
	mdl_lesson.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_lesson_pages.lessonid = mdl_lesson.id AND
	mdl_lesson_pages.qtype <= 10 AND
	mdl_lesson_pages.timemodified <= mdl_hours_import_log.import_date AND
	mdl_hours_import_log.mod_id = mdl_lesson.id) as old_pgs_wq,
	(SELECT
	count(mdl_lesson_pages.id)
	FROM
	mdl_lesson,
	mdl_lesson_pages,
	mdl_hours_import_log
	WHERE
	mdl_lesson.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_lesson_pages.lessonid = mdl_lesson.id AND
	mdl_lesson_pages.qtype <= 10 AND
	mdl_lesson_pages.timemodified > mdl_hours_import_log.import_date AND
	mdl_hours_import_log.mod_id = mdl_lesson.id) as mod_pgs_wq";
	$lessons=$DB->get_records_sql($sql_lessons);
	foreach($lessons as $l)	{
		$lessons_new=$l->new;
		$lessons_old=$l->old;
		$lessons_mod=$l->mod;
		$new_pgs=$l->new_pgs;
		$old_pgs=$l->old_pgs;
		$mod_pgs=$l->mod_pgs;
		$new_pgs_wq=$l->new_pgs_wq;
		$old_pgs_wq=$l->old_pgs_wq;
		$mod_pgs_wq=$l->mod_pgs_wq;
	}

	//qtype<=10 - с вопросами
	//qtype>10 - рубрикаторы
	//теперь будем будем искать картинки и ссылки при условии наличия страниц

	$imgs_new=0;
	$hrefs_new=0;
	$imgs_old=0;
	$hrefs_old=0;
	$imgs_mod=0;
	$hrefs_mod=0;

	if (($new_pgs+$new_pgs_wq)>0) { //если есть новые страницы
		$sql_new="SELECT
		mdl_lesson_pages.contents
		FROM
		mdl_lesson_pages
		WHERE
		lessonid NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid) AND
		lessonid IN (SELECT id FROM mdl_lesson WHERE course = $cid)
		UNION
		(SELECT
		mdl_lesson_pages.contents
		FROM
		mdl_lesson_pages,
		mdl_hours_import_log
		WHERE
		mdl_hours_import_log.course = $cid AND
		mdl_hours_import_log.type = $modid AND
		mdl_lesson_pages.lessonid = mdl_hours_import_log.mod_id AND
		mdl_lesson_pages.timecreated > mdl_hours_import_log.import_date)";
		$par=$DB->get_records_sql($sql_new);

		foreach ($par as $page) {
			$imgs_new=$imgs_new+substr_count($page->contents,'<img');
			$hrefs_new=$hrefs_new+substr_count($page->contents,'href');
		}

	}


	if (($old_pgs+$old_pgs_wq)>0) {	//если есть старые страницы
		$sql_old="SELECT
		ROW_NUMBER() OVER(ORDER BY contents),
		mdl_lesson_pages.contents
		FROM
		mdl_lesson_pages,
		mdl_hours_import_log
		WHERE
		mdl_hours_import_log.course = $cid AND
		mdl_hours_import_log.type = $modid AND
		mdl_lesson_pages.lessonid = mdl_hours_import_log.mod_id AND
		mdl_lesson_pages.timemodified <= mdl_hours_import_log.import_date AND
		mdl_lesson_pages.timecreated <= mdl_hours_import_log.import_date";
		$par=$DB->get_records_sql($sql_old);

		foreach ($par as $page) {
			$imgs_old=$imgs_old+substr_count($page->contents,'<img');
			$hrefs_old=$hrefs_old+substr_count($page->contents,'href');
		}
	}

	if (($mod_pgs+$mod_pgs_wq)>0) {			//если есть измененные страницы
		$sql_mod="SELECT
		mdl_lesson_pages.contents
		FROM
		mdl_lesson_pages,
		mdl_hours_import_log
		WHERE
		mdl_hours_import_log.course = $cid AND
		mdl_hours_import_log.type = $modid AND
		mdl_lesson_pages.lessonid = mdl_hours_import_log.mod_id AND
		mdl_lesson_pages.timemodified > mdl_hours_import_log.import_date AND
		mdl_lesson_pages.timecreated <= mdl_hours_import_log.import_date";
		$par=$DB->get_records_sql($sql_mod);
			
		foreach ($par as $page) {
			$imgs_mod=$imgs_mod+substr_count($page->contents,'<img');
			$hrefs_mod=$hrefs_mod+substr_count($page->contents,'href');
		}
	}

	$results = array();
	$results[] = array('lessons_new'=>$lessons_new,'lessons_old'=>$lessons_old, 'lessons_mod'=>$lessons_mod, 'new_pgs'=>$new_pgs, 'old_pgs'=>$old_pgs, 'mod_pgs'=>$mod_pgs, 'new_pgs_wq'=>$new_pgs_wq, 'old_pgs_wq'=>$old_pgs_wq, 'mod_pgs_wq'=>$mod_pgs_wq, 'imgs_new'=>$imgs_new, 'hrefs_new'=>$hrefs_new, 'imgs_old'=>$imgs_old, 'hrefs_old'=>$hrefs_old, 'imgs_mod'=>$imgs_mod, 'hrefs_mod'=>$hrefs_mod);
	return $results;
}

function calc_glossaries($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='glossary';"); //catching the module's id

	$sql_glossaries="SELECT
	(SELECT count(id)
	FROM mdl_glossary
	WHERE
	course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid)) as new,
	(SELECT
	count(id)
	FROM
	mdl_glossary,
	mdl_hours_import_log
	WHERE
	mdl_glossary.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_glossary.id = mdl_hours_import_log.mod_id AND
	mdl_glossary.timecreated <= mdl_hours_import_log.import_date AND
	mdl_glossary.timemodified <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(id)
	FROM
	mdl_glossary,
	mdl_hours_import_log
	WHERE
	mdl_glossary.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_glossary.id = mdl_hours_import_log.mod_id AND
	mdl_glossary.timecreated <= mdl_hours_import_log.import_date AND
	mdl_glossary.timemodified > mdl_hours_import_log.import_date) as mod,
	((SELECT
	count(id)
	FROM
	mdl_glossary_entries
	WHERE
	glossaryid IN (SELECT id FROM mdl_glossary WHERE course = $cid) AND
	glossaryid NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid)) +
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_glossary_entries
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_glossary_entries.glossaryid = mdl_hours_import_log.mod_id AND
	mdl_glossary_entries.timecreated > mdl_hours_import_log.import_date
	)) as new_entries,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_glossary_entries
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_glossary_entries.glossaryid = mdl_hours_import_log.mod_id AND
	mdl_glossary_entries.timemodified <= mdl_hours_import_log.import_date AND
	mdl_glossary_entries.timecreated <= mdl_hours_import_log.import_date) as old_entries,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_glossary_entries
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_glossary_entries.glossaryid = mdl_hours_import_log.mod_id AND
	mdl_glossary_entries.timemodified > mdl_hours_import_log.import_date AND
	mdl_glossary_entries.timecreated <= mdl_hours_import_log.import_date) as mod_entries";
	$glossaries=$DB->get_records_sql($sql_glossaries);
	$imgs_new=0;
	$hrefs_new=0;
	$imgs_old=0;
	$hrefs_old=0;
	$imgs_mod=0;
	$hrefs_mod=0;
	foreach($glossaries as $g)	{
		$glossaries_new=$g->new;
		$glossaries_old=$g->old;
		$glossaries_mod=$g->mod;
		$new_entries=$g->new_entries;
		$old_entries=$g->old_entries;
		$mod_entries=$g->mod_entries;


		if ($new_entries > 0) {
			//Если есть новые записи
			$sql_new = "SELECT
			definition
			FROM
			mdl_glossary_entries,
			mdl_glossary
			WHERE
			glossaryid NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type= $modid) AND
			mdl_glossary_entries.glossaryid = mdl_glossary.id AND
			mdl_glossary.course = $cid
			UNION
			SELECT
			definition
			FROM
			mdl_glossary_entries,
			mdl_hours_import_log
			WHERE
			mdl_hours_import_log.course = $cid AND
			mdl_hours_import_log.type = $modid AND
			mdl_glossary_entries.glossaryid = mdl_hours_import_log.mod_id AND
			mdl_glossary_entries.timecreated > mdl_hours_import_log.import_date";
			$par=$DB->get_records_sql($sql_new);
				
			foreach ($par as $page) {
				$imgs_new=$imgs_new+substr_count($page->definition,'<img');
				$hrefs_new=$hrefs_new+substr_count($page->definition,'href');
			}
		}

		if ($old_entries > 0) {//Если есть импортированные записи
			$sql_old = "SELECT
			ROW_NUMBER() OVER(ORDER BY definition),
			mdl_glossary_entries.definition
			FROM
			public.mdl_hours_import_log,
			public.mdl_glossary_entries
			WHERE
			mdl_hours_import_log.course = $cid AND
			mdl_hours_import_log.type = $modid AND
			mdl_glossary_entries.glossaryid = mdl_hours_import_log.mod_id AND
			mdl_glossary_entries.timemodified <= mdl_hours_import_log.import_date AND
			mdl_glossary_entries.timecreated <= mdl_hours_import_log.import_date";
			$par=$DB->get_records_sql($sql_old);

			foreach ($par as $page) {
				$imgs_old=$imgs_old+substr_count($page->definition,'<img');
				$hrefs_old=$hrefs_old+substr_count($page->definition,'href');
			}
		}
		if ($mod_entries > 0) { //Если есть модифицированные записи
			$sql_old = "SELECT
			ROW_NUMBER() OVER(ORDER BY definition),
			mdl_glossary_entries.definition
			FROM
			public.mdl_hours_import_log,
			public.mdl_glossary_entries
			WHERE
			mdl_hours_import_log.course = $cid AND
			mdl_hours_import_log.type = $modid AND
			mdl_glossary_entries.glossaryid = mdl_hours_import_log.mod_id AND
			mdl_glossary_entries.timemodified > mdl_hours_import_log.import_date AND
			mdl_glossary_entries.timecreated <= mdl_hours_import_log.import_date";
			$par=$DB->get_records_sql($sql_mod);

			foreach ($par as $page) {
				$imgs_mod=$imgs_mod+substr_count($page->definition,'<img');
				$hrefs_mod=$hrefs_mod+substr_count($page->definition,'href');
			}
		}
	}
	$results = array();
	$results[] = array('glossaries_new'=>$glossaries_new,'glossaries_old'=>$glossaries_old, 'glossaries_mod'=>$glossaries_mod, 'new_entries'=>$new_entries, 'old_entries'=>$old_entries, 'mod_entries'=>$mod_entries, 'imgs_new'=>$imgs_new, 'hrefs_new'=>$hrefs_new, 'imgs_old'=>$imgs_old, 'hrefs_old'=>$hrefs_old, 'imgs_mod'=>$imgs_mod, 'hrefs_mod'=>$hrefs_mod);
	return $results;
}

function calc_assignments($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='assignment';"); //catching the module's id

	$sql_assignments = "SELECT
	(SELECT
	count(id)
	FROM
	mdl_assignment
	WHERE
	course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type=$modid)) as new,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_assignment
	WHERE
	mdl_hours_import_log.mod_id = mdl_assignment.id AND
	mdl_assignment.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_assignment.timemodified <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_assignment
	WHERE
	mdl_hours_import_log.mod_id = mdl_assignment.id AND
	mdl_assignment.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_assignment.timemodified > mdl_hours_import_log.import_date) as mod";
	$assignments=$DB->get_records_sql($sql_assignments);
	$imgs_new=0;
	$hrefs_new=0;
	$imgs_old=0;
	$hrefs_old=0;
	$imgs_mod=0;
	$hrefs_mod=0;
	foreach($assignments as $a)	{
		$assignments_new=$a->new;
		$assignments_old=$a->old;
		$assignments_mod=$a->mod;
	}



	if ($assignments_new > 0) {
		$sql_new="SELECT
		intro
		FROM
		mdl_assignment
		WHERE
		course = $cid AND
		id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type=$modid)";
		$par=$DB->get_records_sql($sql_new);
		foreach ($par as $page) {
			$imgs_new=$imgs_new+substr_count($page->intro,'<img');
			$hrefs_new=$hrefs_new+substr_count($page->intro,'href');
		}
	}
	if ($assignments_old > 0) {
		$sql_old = "SELECT
		intro
		FROM
		mdl_hours_import_log,
		mdl_assignment
		WHERE
		mdl_hours_import_log.mod_id = mdl_assignment.id AND
		mdl_assignment.course = $cid AND
		mdl_hours_import_log.type = $modid AND
		mdl_hours_import_log.course = $cid AND
		mdl_assignment.timemodified <= mdl_hours_import_log.import_date";
		$par=$DB->get_records_sql($sql_old);
		foreach ($par as $page) {
			$imgs_old=$imgs_old+substr_count($page->intro,'<img');
			$hrefs_old=$hrefs_old+substr_count($page->intro,'href');
		}
	}
	if ($assignments_mod > 0) {
		$sql_mod = "SELECT
		intro
		FROM
		mdl_hours_import_log,
		mdl_assignment
		WHERE
		mdl_hours_import_log.mod_id = mdl_assignment.id AND
		mdl_assignment.course = $cid AND
		mdl_hours_import_log.type = $modid AND
		mdl_hours_import_log.course = $cid AND
		mdl_assignment.timemodified > mdl_hours_import_log.import_date";
		$par=$DB->get_records_sql($sql_mod);
		foreach ($par as $page) {
			$imgs_mod=$imgs_mod+substr_count($page->intro,'<img');
			$hrefs_mod=$hrefs_mod+substr_count($page->intro,'href');
		}
	}
	$results = array();
	$results[] = array('assignments_new'=>$assignments_new, 'assignments_old'=>$assignments_old, 'assignments_mod'=>$assignments_mod, 'imgs_new'=>$imgs_new, 'hrefs_new'=>$hrefs_new, 'imgs_old'=>$imgs_old, 'hrefs_old'=>$hrefs_old, 'imgs_mod'=>$imgs_mod, 'hrefs_mod'=>$hrefs_mod);
	return $results;
}

function calc_assigns($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='assign';"); //catching the module's id

	$sql_assigns = "SELECT
	(SELECT
	count(id)
	FROM
	mdl_assign
	WHERE
	course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type=$modid)) as new,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_assign
	WHERE
	mdl_hours_import_log.mod_id = mdl_assign.id AND
	mdl_assign.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_assign.timemodified <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_assign
	WHERE
	mdl_hours_import_log.mod_id = mdl_assign.id AND
	mdl_assign.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_assign.timemodified > mdl_hours_import_log.import_date) as mod";
	$assigns=$DB->get_records_sql($sql_assigns);
	$imgs_new=0;
	$hrefs_new=0;
	$imgs_old=0;
	$hrefs_old=0;
	$imgs_mod=0;
	$hrefs_mod=0;
	foreach($assigns as $a)	{
		$assigns_new=$a->new;
		$assigns_old=$a->old;
		$assigns_mod=$a->mod;
	}



	if ($assigns_new > 0) {
		$sql_new="SELECT
		intro
		FROM
		mdl_assign
		WHERE
		course = $cid AND
		id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type=$modid)";
		$par=$DB->get_records_sql($sql_new);
		foreach ($par as $page) {
			$imgs_new=$imgs_new+substr_count($page->intro,'<img');
			$hrefs_new=$hrefs_new+substr_count($page->intro,'href');
		}
	}
	if ($assigns_old > 0) {
		$sql_old = "SELECT
		intro
		FROM
		mdl_hours_import_log,
		mdl_assign
		WHERE
		mdl_hours_import_log.mod_id = mdl_assign.id AND
		mdl_assign.course = $cid AND
		mdl_hours_import_log.type = $modid AND
		mdl_hours_import_log.course = $cid AND
		mdl_assign.timemodified <= mdl_hours_import_log.import_date";
		$par=$DB->get_records_sql($sql_old);
		foreach ($par as $page) {
			$imgs_old=$imgs_old+substr_count($page->intro,'<img');
			$hrefs_old=$hrefs_old+substr_count($page->intro,'href');
		}
	}
	if ($assigns_mod > 0) {
		$sql_mod = "SELECT
		intro
		FROM
		mdl_hours_import_log,
		mdl_assign
		WHERE
		mdl_hours_import_log.mod_id = mdl_assign.id AND
		mdl_assign.course = $cid AND
		mdl_hours_import_log.type = $modid AND
		mdl_hours_import_log.course = $cid AND
		mdl_assign.timemodified > mdl_hours_import_log.import_date";
		$par=$DB->get_records_sql($sql_mod);
		foreach ($par as $page) {
			$imgs_mod=$imgs_mod+substr_count($page->intro,'<img');
			$hrefs_mod=$hrefs_mod+substr_count($page->intro,'href');
		}
	}
	$results = array();
	$results[] = array('assigns_new'=>$assigns_new, 'assigns_old'=>$assigns_old, 'assigns_mod'=>$assigns_mod, 'imgs_new'=>$imgs_new, 'hrefs_new'=>$hrefs_new, 'imgs_old'=>$imgs_old, 'hrefs_old'=>$hrefs_old, 'imgs_mod'=>$imgs_mod, 'hrefs_mod'=>$hrefs_mod);
	return $results;
}

function calc_wikis($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='wiki';"); //catching the module's id

	$sql_wikis="SELECT
	(SELECT
	count(id)
	FROM
	mdl_wiki
	WHERE
	mdl_wiki.course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course=$cid AND type=$modid)) as new,
	(SELECT
	count(id)
	FROM
	mdl_wiki,
	mdl_hours_import_log
	WHERE
	mdl_wiki.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_wiki.id = mdl_hours_import_log.mod_id AND
	mdl_wiki.timemodified <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(id)
	FROM
	mdl_wiki,
	mdl_hours_import_log
	WHERE
	mdl_wiki.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_wiki.id = mdl_hours_import_log.mod_id AND
	mdl_wiki.timemodified > mdl_hours_import_log.import_date) as mod";
	$wikis=$DB->get_records_sql($sql_wikis);
	foreach($wikis as $w)	{
		$wikis_new=$w->new;
		$wikis_old=$w->old;
		$wikis_mod=$w->mod;
	}
	$results = array();
	$results[] = array('wikis_new'=>$wikis_new, 'wikis_old'=>$wikis_old, 'wikis_mod'=>$wikis_mod);
	return $results;
}


function calc_feedbacks($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='feedback';"); //catching the module's id

	$sql_feedbacks="SELECT
	(SELECT
	count(id)
	FROM
	mdl_feedback
	WHERE
	mdl_feedback.course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course=$cid AND type=$modid)) as new,
	(SELECT
	count(id)
	FROM
	mdl_feedback,
	mdl_hours_import_log
	WHERE
	mdl_feedback.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_feedback.id = mdl_hours_import_log.mod_id AND
	mdl_feedback.timemodified <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(id)
	FROM
	mdl_feedback,
	mdl_hours_import_log
	WHERE
	mdl_feedback.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_feedback.id = mdl_hours_import_log.mod_id AND
	mdl_feedback.timemodified > mdl_hours_import_log.import_date) as mod";
	$feedbacks=$DB->get_records_sql($sql_feedbacks);
	foreach($feedbacks as $f)	{
		$feedbacks_new=$f->new;
		$feedbacks_old=$f->old;
		$feedbacks_mod=$f->mod;
	}
	$results = array();
	$results[] = array('feedbacks_new'=>$feedbacks_new, 'feedbacks_old'=>$feedbacks_old, 'feedbacks_mod'=>$feedbacks_mod);
	return $results;
}


function calc_labels($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='label';"); //catching the module's id

	$sql_labels="SELECT
	(SELECT count(id)
	FROM mdl_label WHERE
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course=$cid AND type=$modid)
	AND course = $cid) as new,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_label
	WHERE
	mdl_hours_import_log.mod_id = mdl_label.id AND
	mdl_label.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course =$cid AND
	mdl_label.timemodified <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_label
	WHERE
	mdl_hours_import_log.mod_id = mdl_label.id AND
	mdl_label.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course =$cid AND
	mdl_label.timemodified > mdl_hours_import_log.import_date) as mod";
	$labels=$DB->get_records_sql($sql_labels);
	foreach($labels as $l)	{
		$labels_new=$l->new;
		$labels_old=$l->old;
		$labels_mod=$l->mod;
	}
	$imgs_new=0;
	$hrefs_new=0;
	$imgs_old=0;
	$hrefs_old=0;
	$imgs_mod=0;
	$hrefs_mod=0;

	if ($labels_new > 0) {
		$sql_new = "SELECT ROW_NUMBER() OVER(ORDER BY intro), intro
		FROM mdl_label WHERE
		id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course=$cid AND type=$modid)
		AND course = $cid";
		$par=$DB->get_records_sql($sql_new);
		foreach ($par as $page) {
			$imgs_new=$imgs_new+substr_count($page->intro,'<img');
			$hrefs_new=$hrefs_new+substr_count($page->intro,'href');
		}
	}
	if ($labels_old > 0) {
		$sql_old = "SELECT
		ROW_NUMBER() OVER(ORDER BY intro),
		intro
		FROM
		mdl_hours_import_log,
		mdl_label
		WHERE
		mdl_hours_import_log.mod_id = mdl_label.id AND
		mdl_label.course = $cid AND
		mdl_hours_import_log.type = $modid AND
		mdl_hours_import_log.course =$cid AND
		mdl_label.timemodified <= mdl_hours_import_log.import_date";
		$par=$DB->get_records_sql($sql_old);
		foreach ($par as $page) {
			$imgs_old=$imgs_old+substr_count($page->intro,'<img');
			$hrefs_old=$hrefs_old+substr_count($page->intro,'href');
		}
	}
	if ($labels_mod > 0) {
		$sql_mod = "SELECT
		ROW_NUMBER() OVER(ORDER BY intro),
		intro
		FROM
		mdl_hours_import_log,
		mdl_label
		WHERE
		mdl_hours_import_log.mod_id = mdl_label.id AND
		mdl_label.course = $cid AND
		mdl_hours_import_log.type = $modid AND
		mdl_hours_import_log.course =$cid AND
		mdl_label.timemodified > mdl_hours_import_log.import_date";
		$par=$DB->get_records_sql($sql_mod);
		foreach ($par as $page) {
			$imgs_mod=$imgs_mod+substr_count($page->intro,'<img');
			$hrefs_mod=$hrefs_mod+substr_count($page->intro,'href');
		}
	}

	$results = array();
	$results[] = array('labels_new'=>$labels_new, 'labels_old'=>$labels_old, 'labels_mod'=>$labels_mod, 'imgs_new'=>$imgs_new, 'hrefs_new'=>$hrefs_new, 'imgs_old'=>$imgs_old, 'hrefs_old'=>$hrefs_old, 'imgs_mod'=>$imgs_mod, 'hrefs_mod'=>$hrefs_mod);
	return $results;
}


function calc_quizes($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='quiz';"); //catching the module's id
	$modid1 = $modid."1"; //question category's id
	$modid2 = $modid."2"; //question's id

	//question types setiing
	$type_calculated = $modid."1"; //calculated question and deprecated category's value
	$type_essay = $modid."2"; //essay question
	$type_match = $modid."3"; //match question
	$type_multianswer = $modid."4"; //multianswer question
	$type_multichoice = $modid."5"; //multichoice question
	$type_numerical = $modid."6"; //numerical question
	$type_random = $modid."7"; //random question
	$type_randomsamatch = $modid."8"; //randomsamatch question
	$type_shortanswer = $modid."9"; //shortanswer question
	$type_truefalse = $modid."10"; //truefalse question


	$sql_quizes="SELECT
	(SELECT
	count(id)
	FROM
	mdl_quiz
	WHERE
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid) AND
	course = $cid) +
	(SELECT count(mdl_quiz.id)
	FROM
	mdl_quiz, mdl_hours_import_log
	WHERE
	mdl_quiz.id = mdl_hours_import_log.mod_id AND
	mdl_quiz.course = $cid AND
	mdl_hours_import_log.course = $cid AND
	mdl_quiz.timecreated > mdl_hours_import_log.import_date AND
	mdl_quiz.timemodified > mdl_hours_import_log.import_date) as new,
	(SELECT
	count(id)
	FROM
	mdl_hours_import_log,
	mdl_quiz
	WHERE
	mdl_quiz.id = mdl_hours_import_log.mod_id AND
	mdl_quiz.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_quiz.timecreated <= mdl_hours_import_log.import_date AND
	mdl_quiz.timemodified <= mdl_hours_import_log.import_date) as old,
	(SELECT
	count(DISTINCT mdl_quiz.id)
	FROM
	mdl_hours_import_log,
	mdl_quiz
	WHERE
	mdl_quiz.id = mdl_hours_import_log.mod_id AND
	mdl_quiz.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.course = $cid AND
	mdl_quiz.timecreated <= mdl_hours_import_log.import_date AND
	mdl_quiz.timemodified > mdl_hours_import_log.import_date) as mod,
	((SELECT count(DISTINCT (category)) FROM mdl_question WHERE id IN (
SELECT DISTINCT(question) FROM mdl_quiz_question_instances WHERE quiz IN (
	SELECT id FROM mdl_quiz WHERE course = $cid
	)))) as cat_new,
	(SELECT
	count(id)
	FROM
	mdl_question_categories
	WHERE
	id IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_calculated) AND
	contextid IN (SELECT id FROM mdl_context WHERE instanceid IN (SELECT $cid as id UNION SELECT id FROM mdl_course_modules WHERE course=$cid AND module=$modid))) as cat_old,
	(SELECT
	COUNT(DISTINCT mdl_question.id)
	FROM mdl_question
	INNER JOIN mdl_question_answers
	ON mdl_question.id=mdl_question_answers.question
	WHERE category IN
	(SELECT DISTINCT (category) FROM mdl_question WHERE id IN (
SELECT DISTINCT(question) FROM mdl_quiz_question_instances WHERE quiz IN (
	SELECT id FROM mdl_quiz WHERE course = $cid
	))
	)
	AND mdl_question.id NOT IN (
	SELECT question FROM mdl_question_calculated WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_calculated)
	UNION
	SELECT question FROM mdl_question_match WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_match)
	UNION
	SELECT question FROM mdl_question_multianswer WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_multianswer)
	UNION
	SELECT question FROM mdl_question_multichoice WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_multichoice)
	UNION
	SELECT question FROM mdl_question_numerical WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_numerical)
	UNION
	SELECT id FROM mdl_question WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type IN ($type_essay, $type_random))
	UNION
	SELECT question FROM mdl_question_randomsamatch WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_randomsamatch)
	UNION
	SELECT question FROM mdl_question_shortanswer WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_shortanswer)
	UNION
	SELECT question FROM mdl_question_truefalse WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_truefalse))
	AND feedback='' ) as qst_no_fb_new,
		
	(SELECT
	COUNT(DISTINCT mdl_question.id)
	FROM mdl_question
	INNER JOIN mdl_question_answers
	ON mdl_question.id=mdl_question_answers.question
	WHERE category IN
	(SELECT DISTINCT (category) FROM mdl_question WHERE id IN (
SELECT DISTINCT(question) FROM mdl_quiz_question_instances WHERE quiz IN (
	SELECT id FROM mdl_quiz WHERE course = $cid
	))
	)
	AND mdl_question.id NOT IN (SELECT question FROM mdl_question_calculated WHERE id IN(
	SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_calculated)
	UNION
	SELECT question FROM mdl_question_match WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_match)
	UNION
	SELECT question FROM mdl_question_multianswer WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_multianswer)
	UNION
	SELECT question FROM mdl_question_multichoice WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_multichoice)
	UNION
	SELECT question FROM mdl_question_numerical WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_numerical)
	UNION
	SELECT id FROM mdl_question WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type IN ($type_essay, $type_random))
	UNION
	SELECT question FROM mdl_question_randomsamatch WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_randomsamatch)
	UNION
	SELECT question FROM mdl_question_shortanswer WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_shortanswer)
	UNION
	SELECT question FROM mdl_question_truefalse WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_truefalse))
	AND feedback<>'') as qst_new,
	(SELECT
	count(DISTINCT mdl_question.id)
	FROM
	mdl_question_answers,
	mdl_question,
	mdl_hours_import_log
	WHERE
	mdl_question_answers.question = mdl_question.id AND
	mdl_question_answers.feedback = '' AND
	mdl_question.id IN (
	SELECT question FROM mdl_question_calculated WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_calculated)
	UNION
	SELECT question FROM mdl_question_match WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_match)
	UNION
	SELECT question FROM mdl_question_multianswer WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_multianswer)
	UNION
	SELECT question FROM mdl_question_multichoice WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_multichoice)
	UNION
	SELECT question FROM mdl_question_numerical WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_numerical)
	UNION
	SELECT id FROM mdl_question WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type IN ($type_essay, $type_random))
	UNION
	SELECT question FROM mdl_question_randomsamatch WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_randomsamatch)
	UNION
	SELECT question FROM mdl_question_shortanswer WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_shortanswer)
	UNION
	SELECT question FROM mdl_question_truefalse WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_truefalse)
	)
	AND
	mdl_question.timecreated <= mdl_hours_import_log.import_date AND
	mdl_question.timemodified <= mdl_hours_import_log.import_date) as qst_no_fb_old,
	(SELECT
	count(DISTINCT mdl_question.id)
	FROM
	mdl_question_answers,
	mdl_question,
	mdl_hours_import_log
	WHERE
	mdl_question_answers.question = mdl_question.id AND
	mdl_question_answers.feedback <> '' AND
	mdl_question.id IN (
	SELECT question FROM mdl_question_calculated WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_calculated)
	UNION
	SELECT question FROM mdl_question_match WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_match)
	UNION
	SELECT question FROM mdl_question_multianswer WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_multianswer)
	UNION
	SELECT question FROM mdl_question_multichoice WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_multichoice)
	UNION
	SELECT question FROM mdl_question_numerical WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_numerical)
	UNION
	SELECT id FROM mdl_question WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type IN ($type_essay, $type_random))
	UNION
	SELECT question FROM mdl_question_randomsamatch WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_randomsamatch)
	UNION
	SELECT question FROM mdl_question_shortanswer WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_shortanswer)
	UNION
	SELECT question FROM mdl_question_truefalse WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_truefalse)
	) AND
	mdl_question.timecreated <= mdl_hours_import_log.import_date AND
	mdl_question.timemodified <= mdl_hours_import_log.import_date) as qst_old,
	(SELECT
	count(DISTINCT mdl_question.id)
	FROM
	mdl_question_answers,
	mdl_question,
	mdl_hours_import_log
	WHERE
	mdl_question_answers.question = mdl_question.id AND
	mdl_question_answers.feedback = '' AND
	mdl_question.id IN (
	SELECT question FROM mdl_question_calculated WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_calculated)
	UNION
	SELECT question FROM mdl_question_match WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_match)
	UNION
	SELECT question FROM mdl_question_multianswer WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_multianswer)
	UNION
	SELECT question FROM mdl_question_multichoice WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_multichoice)
	UNION
	SELECT question FROM mdl_question_numerical WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_numerical)
	UNION
	SELECT id FROM mdl_question WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type IN ($type_essay, $type_random))
	UNION
	SELECT question FROM mdl_question_randomsamatch WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_randomsamatch)
	UNION
	SELECT question FROM mdl_question_shortanswer WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_shortanswer)
	UNION
	SELECT question FROM mdl_question_truefalse WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_truefalse)
	) AND
	mdl_question.timecreated <= mdl_hours_import_log.import_date AND
	mdl_question.timemodified > mdl_hours_import_log.import_date) as qst_no_fb_mod,
	(SELECT
	count(DISTINCT mdl_question.id)
	FROM
	mdl_question_answers,
	mdl_question,
	mdl_hours_import_log
	WHERE
	mdl_question_answers.question = mdl_question.id AND
	mdl_question_answers.feedback <> '' AND
	mdl_question.id IN (
	SELECT question FROM mdl_question_calculated WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_calculated)
	UNION
	SELECT question FROM mdl_question_match WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_match)
	UNION
	SELECT question FROM mdl_question_multianswer WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_multianswer)
	UNION
	SELECT question FROM mdl_question_multichoice WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_multichoice)
	UNION
	SELECT question FROM mdl_question_numerical WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_numerical)
	UNION
	SELECT id FROM mdl_question WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type IN ($type_essay, $type_random))
	UNION
	SELECT question FROM mdl_question_randomsamatch WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_randomsamatch)
	UNION
	SELECT question FROM mdl_question_shortanswer WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_shortanswer)
	UNION
	SELECT question FROM mdl_question_truefalse WHERE id IN(SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $type_truefalse)
	) AND
	mdl_question.timecreated <= mdl_hours_import_log.import_date AND
	mdl_question.timemodified > mdl_hours_import_log.import_date) as qst_mod";
	$quizes=$DB->get_records_sql($sql_quizes);
	foreach($quizes as $q)	{
		$quizes_new=$q->new;
		$quizes_old=$q->old;
		$quizes_mod=$q->mod;
		$cat_new=$q->cat_new;
		$cat_old=$q->cat_old;
		//$cat_mod=$q->cat_mod;
		$qst_no_fb_new=$q->qst_no_fb_new;
		$qst_no_fb_old=$q->qst_no_fb_old;
		$qst_no_fb_mod=$q->qst_no_fb_mod;
		$qst_new=$q->qst_new;
		$qst_old=$q->qst_old;
		$qst_mod=$q->qst_mod;
	}


	if ($cat_new > 0) {
		$sql_cats="SELECT DISTINCT(category) as id FROM mdl_question WHERE id IN (
	SELECT DISTINCT(question) FROM mdl_quiz_question_instances WHERE quiz IN (
	SELECT id FROM mdl_quiz WHERE course = $cid	))";

		$catids=$DB->get_records_sql($sql_cats);

		foreach ($catids as $catid) {
			$sql_mincat= "SELECT MIN(id) as orig_id FROM mdl_question_categories WHERE stamp IN (
			SELECT stamp FROM mdl_question_categories WHERE id = $catid->id)";
			$rez = $DB->get_records_sql($sql_mincat);
			foreach($rez as $r) {
				if ($catid->id != $r->orig_id) {
					$cat_new--;
					$cat_old++;
				}
			}
		}
			
			
			
	}


	//counting images & links

	$imgs_new=0;
	$hrefs_new=0;
	$imgs_old=0;
	$hrefs_old=0;
	$imgs_mod=0;
	$hrefs_mod=0;

	if (($qst_no_fb_new + $qst_new) > 0) {
		$sql_new="SELECT
		DISTINCT mdl_question.questiontext
		FROM mdl_question
		INNER JOIN mdl_question_answers
		ON mdl_question.id=mdl_question_answers.question
		WHERE category IN
		(SELECT id
		FROM mdl_question_categories
		WHERE
		contextid IN
		(SELECT id
		FROM mdl_context
		WHERE instanceid IN
		(SELECT $cid as id
		UNION
		SELECT id FROM mdl_course_modules WHERE course=$cid AND module=$modid)
		)
		)
		AND mdl_question.id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid2)
		UNION
		SELECT
		mdl_question_answers.answer
		FROM mdl_question
		INNER JOIN mdl_question_answers
		ON mdl_question.id=mdl_question_answers.question
		WHERE category IN
		(SELECT id
		FROM mdl_question_categories
		WHERE
		contextid IN
		(SELECT id
		FROM mdl_context
		WHERE instanceid IN
		(SELECT $cid as id
		UNION
		SELECT id FROM mdl_course_modules WHERE course=$cid AND module=$modid)
		)
		)
		AND mdl_question.id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid2)
		UNION
		SELECT
		mdl_question_answers.feedback
		FROM mdl_question
		INNER JOIN mdl_question_answers
		ON mdl_question.id=mdl_question_answers.question
		WHERE category IN
		(SELECT id
		FROM mdl_question_categories
		WHERE
		contextid IN
		(SELECT id
		FROM mdl_context
		WHERE instanceid IN
		(SELECT $cid as id
		UNION
		SELECT id FROM mdl_course_modules WHERE course=$cid AND module=$modid)
		)
		)
		AND mdl_question.id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid2)";
		$par=$DB->get_records_sql($sql_new);
		foreach ($par as $page) {
			$imgs_new=$imgs_new+substr_count($page->questiontext,'<img');
			$hrefs_new=$hrefs_new+substr_count($page->questiontext,'href');
		}
	}

	if (($qst_no_fb_old + $qst_old) > 0) {
		$sql_old = "SELECT
		DISTINCT mdl_question.questiontext
		FROM
		mdl_question_answers,
		mdl_question,
		mdl_hours_import_log
		WHERE
		mdl_question_answers.question = mdl_question.id AND
		mdl_hours_import_log.type = $modid2 AND
		mdl_hours_import_log.course = $cid AND
		mdl_hours_import_log.mod_id = mdl_question.id AND
		mdl_question.timecreated <= mdl_hours_import_log.import_date AND
		mdl_question.timemodified <= mdl_hours_import_log.import_date
		UNION
		SELECT
		answer
		FROM
		mdl_question_answers,
		mdl_question,
		mdl_hours_import_log
		WHERE
		mdl_question_answers.question = mdl_question.id AND
		mdl_hours_import_log.type = $modid2 AND
		mdl_hours_import_log.course = $cid AND
		mdl_hours_import_log.mod_id = mdl_question.id AND
		mdl_question.timecreated <= mdl_hours_import_log.import_date AND
		mdl_question.timemodified <= mdl_hours_import_log.import_date
		UNION
		SELECT
		feedback
		FROM
		mdl_question_answers,
		mdl_question,
		mdl_hours_import_log
		WHERE
		mdl_question_answers.question = mdl_question.id AND
		mdl_hours_import_log.type = $modid2 AND
		mdl_hours_import_log.course = $cid AND
		mdl_hours_import_log.mod_id = mdl_question.id AND
		mdl_question.timecreated <= mdl_hours_import_log.import_date AND
		mdl_question.timemodified <= mdl_hours_import_log.import_date";
		$par=$DB->get_records_sql($sql_old);
		foreach ($par as $page) {
			$imgs_old=$imgs_old+substr_count($page->questiontext,'<img');
			$hrefs_old=$hrefs_old+substr_count($page->questiontext,'href');
		}
	}

	if (($qst_no_fb_mod + $qst_mod) > 0) {
		$sql_mod ="SELECT
		DISTINCT mdl_question.questiontext
		FROM
		mdl_question_answers,
		mdl_question,
		mdl_hours_import_log
		WHERE
		mdl_question_answers.question = mdl_question.id AND
		mdl_hours_import_log.type = $modid2 AND
		mdl_hours_import_log.course = $cid AND
		mdl_hours_import_log.mod_id = mdl_question.id AND
		mdl_question.timecreated <= mdl_hours_import_log.import_date AND
		mdl_question.timemodified > mdl_hours_import_log.import_date
		UNION
		SELECT
		answer
		FROM
		mdl_question_answers,
		mdl_question,
		mdl_hours_import_log
		WHERE
		mdl_question_answers.question = mdl_question.id AND
		mdl_hours_import_log.type = $modid2 AND
		mdl_hours_import_log.course = $cid AND
		mdl_hours_import_log.mod_id = mdl_question.id AND
		mdl_question.timecreated <= mdl_hours_import_log.import_date AND
		mdl_question.timemodified > mdl_hours_import_log.import_date
		UNION
		SELECT
		feedback
		FROM
		mdl_question_answers,
		mdl_question,
		mdl_hours_import_log
		WHERE
		mdl_question_answers.question = mdl_question.id AND
		mdl_hours_import_log.type = $modid2 AND
		mdl_hours_import_log.course = $cid AND
		mdl_hours_import_log.mod_id = mdl_question.id AND
		mdl_question.timecreated <= mdl_hours_import_log.import_date AND
		mdl_question.timemodified > mdl_hours_import_log.import_date";
		$par=$DB->get_records_sql($sql_mod);
		foreach ($par as $page) {
			$imgs_mod=$imgs_mod+substr_count($page->questiontext,'<img');
			$hrefs_mod=$hrefs_mod+substr_count($page->questiontext,'href');
		}
	}
	$results = array();
	$results[] = array( 'quizes_new'=>$quizes_new,
			'quizes_old'=>$quizes_old,
			'quizes_mod'=>$quizes_mod,
			'cat_new'=>$cat_new,
			'cat_old'=>$cat_old,
			//'cat_mod'=>$cat_mod,
			'qst_no_fb_new'=>$qst_no_fb_new,
			'qst_no_fb_old'=>$qst_no_fb_old,
			'qst_no_fb_mod'=>$qst_no_fb_mod,
			'qst_new'=>$qst_new,
			'qst_old'=>$qst_old,
			'qst_mod'=>$qst_mod,
			'imgs_new'=>$imgs_new,
			'hrefs_new'=>$hrefs_new,
			'imgs_old'=>$imgs_old,
			'hrefs_old'=>$hrefs_old,
			'imgs_mod'=>$imgs_mod,
			'hrefs_mod'=>$hrefs_mod);
	return $results;
}

function calc_chats($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='chat';"); //catching the module's id

	$sql_chats = "SELECT
	(SELECT count(id)
	FROM mdl_chat
	WHERE course = $cid AND
	id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid)) as new,
	(SELECT
	count(id)
	FROM
	mdl_chat,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_chat.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.import_date >= mdl_chat.timemodified AND
	mdl_hours_import_log.mod_id = mdl_chat.id) as old,
	(SELECT
	count(id)
	FROM
	mdl_chat,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_chat.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.import_date < mdl_chat.timemodified AND
	mdl_hours_import_log.mod_id = mdl_chat.id) as mod,
	(SELECT
	(SELECT
	count(mdl_chat_messages.id)
	FROM
	mdl_chat_messages
	WHERE
	chatid NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid) AND
	chatid IN (SELECT id FROM mdl_chat WHERE course = $cid) AND
	userid IN (SELECT userid FROM mdl_role_assignments WHERE contextid IN (SELECT id FROM mdl_context WHERE instanceid=$cid) AND roleid=3)) +
	(SELECT
	count(mdl_chat_messages.id)
	FROM
	mdl_chat_messages,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_chat_messages.chatid AND
	mdl_hours_import_log.import_date < mdl_chat_messages.timestamp AND
	mdl_chat_messages.userid IN (SELECT userid FROM mdl_role_assignments WHERE contextid IN (SELECT id FROM mdl_context WHERE instanceid=$cid) AND roleid=3))) as msgs_prep_new,
	(SELECT
	(SELECT
	count(mdl_chat_messages.id)
	FROM
	mdl_chat_messages
	WHERE
	chatid NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid) AND
	chatid IN (SELECT id FROM mdl_chat WHERE course = $cid) AND
	userid NOT IN (SELECT userid FROM mdl_role_assignments WHERE contextid IN (SELECT id FROM mdl_context WHERE instanceid=$cid) AND roleid=3)) +
	(SELECT
	count(mdl_chat_messages.id)
	FROM
	mdl_chat_messages,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_chat_messages.chatid AND
	mdl_hours_import_log.import_date < mdl_chat_messages.timestamp AND
	mdl_chat_messages.userid NOT IN (SELECT userid FROM mdl_role_assignments WHERE contextid IN (SELECT id FROM mdl_context WHERE instanceid=$cid) AND roleid=3))) as msgs_stud_new,
	(SELECT
	count(id)
	FROM
	mdl_chat_messages,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_chat_messages.chatid = mdl_hours_import_log.mod_id AND
	mdl_chat_messages.timestamp <= mdl_hours_import_log.import_date AND
	userid IN (SELECT userid FROM mdl_role_assignments WHERE contextid IN (SELECT id FROM mdl_context WHERE instanceid=$cid) AND roleid=3)) as msgs_prep_old,
	(SELECT
	count(mdl_chat_messages.id)
	FROM
	mdl_chat_messages,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_chat_messages.chatid = mdl_hours_import_log.mod_id AND
	mdl_chat_messages.timestamp <= mdl_hours_import_log.import_date AND
	userid NOT IN (SELECT userid FROM mdl_role_assignments WHERE contextid IN (SELECT id FROM mdl_context WHERE instanceid=$cid) AND roleid=3)) as msgs_stud_old,
	(SELECT
	count(id)
	FROM
	mdl_chat_messages,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_chat_messages.chatid = mdl_hours_import_log.mod_id AND
	mdl_chat_messages.timestamp > mdl_hours_import_log.import_date AND
	userid IN (SELECT userid FROM mdl_role_assignments WHERE contextid IN (SELECT id FROM mdl_context WHERE instanceid=$cid) AND roleid=3)) as msgs_prep_mod,
	(SELECT
	count(id)
	FROM
	mdl_chat_messages,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_chat_messages.chatid = mdl_hours_import_log.mod_id AND
	mdl_chat_messages.timestamp > mdl_hours_import_log.import_date AND
	userid NOT IN (SELECT userid FROM mdl_role_assignments WHERE contextid IN (SELECT id FROM mdl_context WHERE instanceid=$cid) AND roleid=3)) as msgs_stud_mod";
	$chats=$DB->get_records_sql($sql_chats);
	foreach($chats as $c)	{
		$chats_new = $c->new;
		$chats_old = $c->old;
		$chats_mod = $c->mod;
		$msgs_prep_new = $c->msgs_prep_new;
		$msgs_prep_old = $c->msgs_prep_old;
		$msgs_prep_mod = $c->msgs_prep_mod;
		$msgs_stud_new = $c->msgs_stud_new;
		$msgs_stud_old = $c->msgs_stud_old;
		$msgs_stud_mod = $c->msgs_stud_mod;
	}
	$results = array();
	$results[] = array( 'chats_new'=>$chats_new,
			'chats_old'=>$chats_old,
			'chats_mod'=>$chats_mod,
			'msgs_prep_new'=>$msgs_prep_new,
			'msgs_prep_old'=>$msgs_prep_old,
			'msgs_prep_mod'=>$msgs_prep_mod,
			'msgs_stud_new'=>$msgs_stud_new,
			'msgs_stud_old'=>$msgs_stud_old,
			'msgs_stud_mod'=>$msgs_stud_mod	);
	return $results;
}
//должна быть отключена в связи с переходом к 2.3
function calc_flvs($cid) {
	global $DB;
	$sql_flvs="SELECT
	(SELECT
	count(id)
	FROM
	mdl_flv
	WHERE id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = 19)
	AND course = $cid) as new,
	(SELECT
	count(id)
	FROM
	mdl_flv,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_flv.course = $cid AND
	mdl_hours_import_log.type = 19 AND
	mdl_hours_import_log.mod_id = mdl_flv.id AND
	mdl_hours_import_log.import_date >= mdl_flv.timecreated AND
	mdl_hours_import_log.import_date >= mdl_flv.timemodified) as old,
	(SELECT
	count(id)
	FROM
	mdl_flv,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_flv.course = $cid AND
	mdl_hours_import_log.type = 19 AND
	mdl_hours_import_log.mod_id = mdl_flv.id AND
	mdl_hours_import_log.import_date >= mdl_flv.timecreated AND
	mdl_hours_import_log.import_date < mdl_flv.timemodified) as mod";
	$flvs=$DB->get_records_sql($sql_flvs);
	foreach($flvs as $f)	{
		$flvs_new = $f->new;
		$flvs_old = $f->old;
		$flvs_mod = $f->mod;
	}
	$results = array();
	$results[] = array( 'flvs_new'=>$flvs_new,
			'flvs_old'=>$flvs_old,
			'flvs_mod'=>$flvs_mod);
	return $results;
}


function calc_urls($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='url';"); //catching the module's id

	$sql_urls="SELECT
	(SELECT
	count(id)
	FROM
	mdl_url
	WHERE id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid)
	AND course = $cid) as new,
	(SELECT
	count(id)
	FROM
	mdl_url,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_url.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_url.id AND
	mdl_hours_import_log.import_date >= mdl_url.timemodified) as old,
	(SELECT
	count(id)
	FROM
	mdl_url,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_url.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_url.id AND
	mdl_hours_import_log.import_date < mdl_url.timemodified) as mod";
	$urls=$DB->get_records_sql($sql_urls);
	foreach($urls as $u)	{
		$urls_new = $u->new;
		$urls_old = $u->old;
		$urls_mod = $u->mod;
	}
	$results = array();
	$results[] = array( 'urls_new'=>$urls_new,
			'urls_old'=>$urls_old,
			'urls_mod'=>$urls_mod);
	return $results;

}

function calc_resources($cid) {
	global $DB;

	$modid = $DB->get_field_sql("SELECT id FROM mdl_modules WHERE name='resource';"); //catching the module's id

	$sql_resources="SELECT
	(SELECT
	count(id)
	FROM
	mdl_resource
	WHERE id NOT IN (SELECT mod_id FROM mdl_hours_import_log WHERE course = $cid AND type = $modid)
	AND course = $cid) as new,
	(SELECT
	count(id)
	FROM
	mdl_resource,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_resource.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_resource.id AND
	mdl_hours_import_log.import_date >= mdl_resource.timemodified) as old,
	(SELECT
	count(id)
	FROM
	mdl_resource,
	mdl_hours_import_log
	WHERE
	mdl_hours_import_log.course = $cid AND
	mdl_resource.course = $cid AND
	mdl_hours_import_log.type = $modid AND
	mdl_hours_import_log.mod_id = mdl_resource.id AND
	mdl_hours_import_log.import_date < mdl_resource.timemodified) as mod";
	$resources=$DB->get_records_sql($sql_resources);
	foreach($resources as $r)	{
		$resources_new = $r->new;
		$resources_old = $r->old;
		$resources_mod = $r->mod;
	}
	$results = array();
	$results[] = array( 'resources_new'=>$resources_new,
			'resources_old'=>$resources_old,
			'resources_mod'=>$resources_mod);
	return $results;

}

function get_course_modules($corid=0, $silent=false) {
	global $cid;
	global $DB;
	if ($corid!=0) $cid=$corid;
	$sql="SELECT DISTINCT mdl_modules.name, (select count(*) from mdl_course_modules where mdl_modules.id=mdl_course_modules.module AND mdl_course_modules.course='$cid') FROM mdl_course_modules, mdl_modules WHERE mdl_course_modules.course='$cid' AND mdl_course_modules.module=mdl_modules.id";
	$res =  $DB->get_records_sql($sql);
	global $display0;
	global $display1;
	$summary_cost_new = 0;
	$summary_cost_old = 0;
	$summary_cost_mod = 0;
	$summary_cost = 0;
	if($res) {
		//если есть элементы курса
		//Таблицы для свежих элементов курса
		$table1 = '<table class="of"><tr><th class="of">'.get_string('type','report_detailedreview').'</th><th class="of">'.get_string('number','report_detailedreview').'</th><th class="of">'.get_string('img', 'report_detailedreview').'</th><th class="of">'.get_string('href', 'report_detailedreview').'</th><th class="of">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table2 = '<table class="of"><tr><th class="of">'.get_string('type','report_detailedreview').'</th><th class="of">'.get_string('number','report_detailedreview').'</th><th class="of">'.get_string('qty','report_detailedreview').'</th><th class="of">'.get_string('img', 'report_detailedreview').'</th><th class="of">'.get_string('href', 'report_detailedreview').'</th><th class="of">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table3 = '<table class="of"><tr><th class="of">'.get_string('type','report_detailedreview').'</th><th class="of">'.get_string('number','report_detailedreview').'</th><th class="of">'.get_string('teacher_msgs','report_detailedreview').'</th><th class="of">'.get_string('student_msgs', 'report_detailedreview').'</th><th class="of">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table4 = '<table class="of"><tr><th class="of">'.get_string('type','report_detailedreview').'</th><th class="of">'.get_string('number','report_detailedreview').'</th><th class="of">'.get_string('quiz_cat', 'report_detailedreview').'</th><th class="of">'.get_string('quest', 'report_detailedreview').'</th><th class="of">'.get_string('stud_quest', 'report_detailedreview').'</th><th class="of">'.get_string('quiz_img', 'report_detailedreview').'</th><th class="of">'.get_string('quiz_href', 'report_detailedreview').'</th><th class="of">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table5 = '<table class="of"><tr><th class="of">'.get_string('type','report_detailedreview').'</th><th class="of">'.get_string('number','report_detailedreview').'</th><th class="of">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table6 = '<table class="of"><tr><th class="of">'.get_string('type','report_detailedreview').'</th><th class="of">'.get_string('number','report_detailedreview').'</th><th class="of">'.get_string('card_rub', 'report_detailedreview').'</th><th class="of">'.get_string('card_quest', 'report_detailedreview').'</th><th class="of">'.get_string('img', 'report_detailedreview').'</th><th class="of">'.get_string('href', 'report_detailedreview').'</th><th class="of">'.get_string('hours','report_detailedreview').'</th></tr>';

		//Таблицы для импортированных элементов курса
		$table1_imported = '<table class="of"><tr><th class="ofi">'.get_string('type','report_detailedreview').'</th><th class="ofi">'.get_string('number','report_detailedreview').'</th><th class="ofi">'.get_string('img', 'report_detailedreview').'</th><th class="ofi">'.get_string('href', 'report_detailedreview').'</th><th class="ofi">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table2_imported = '<table class="of"><tr><th class="ofi">'.get_string('type','report_detailedreview').'</th><th class="ofi">'.get_string('number','report_detailedreview').'</th><th class="ofi">'.get_string('qty','report_detailedreview').'</th><th class="ofi">'.get_string('img', 'report_detailedreview').'</th><th class="ofi">'.get_string('href', 'report_detailedreview').'</th><th class="ofi">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table3_imported = '<table class="of"><tr><th class="ofi">'.get_string('type','report_detailedreview').'</th><th class="ofi">'.get_string('number','report_detailedreview').'</th><th class="ofi">'.get_string('teacher_msgs','report_detailedreview').'</th><th class="ofi">'.get_string('student_msgs', 'report_detailedreview').'</th><th class="ofi">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table4_imported = '<table class="of"><tr><th class="ofi">'.get_string('type','report_detailedreview').'</th><th class="ofi">'.get_string('number','report_detailedreview').'</th><th class="ofi">'.get_string('quiz_cat', 'report_detailedreview').'</th><th class="ofi">'.get_string('quest', 'report_detailedreview').'</th><th class="ofi">'.get_string('stud_quest', 'report_detailedreview').'</th><th class="ofi">'.get_string('quiz_img', 'report_detailedreview').'</th><th class="ofi">'.get_string('quiz_href', 'report_detailedreview').'</th><th class="ofi">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table5_imported = '<table class="of"><tr><th class="ofi">'.get_string('type','report_detailedreview').'</th><th class="ofi">'.get_string('number','report_detailedreview').'</th><th class="ofi">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table6_imported = '<table class="of"><tr><th class="ofi">'.get_string('type','report_detailedreview').'</th><th class="ofi">'.get_string('number','report_detailedreview').'</th><th class="ofi">'.get_string('card_rub', 'report_detailedreview').'</th><th class="ofi">'.get_string('card_quest', 'report_detailedreview').'</th><th class="ofi">'.get_string('img', 'report_detailedreview').'</th><th class="ofi">'.get_string('href', 'report_detailedreview').'</th><th class="ofi">'.get_string('hours','report_detailedreview').'</th></tr>';

		//Таблицы для элементов курса, которые были импортированы и затем изменены
		$table1_impochanged = '<table class="of"><tr><th class="ofo">'.get_string('type','report_detailedreview').'</th><th class="ofo">'.get_string('number','report_detailedreview').'</th><th class="ofo">'.get_string('img', 'report_detailedreview').'</th><th class="ofo">'.get_string('href', 'report_detailedreview').'</th><th class="ofo">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table2_impochanged = '<table class="of"><tr><th class="ofo">'.get_string('type','report_detailedreview').'</th><th class="ofo">'.get_string('number','report_detailedreview').'</th><th class="ofo">'.get_string('qty','report_detailedreview').'</th><th class="ofo">'.get_string('img', 'report_detailedreview').'</th><th class="ofo">'.get_string('href', 'report_detailedreview').'</th><th class="ofo">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table3_impochanged = '<table class="of"><tr><th class="ofo">'.get_string('type','report_detailedreview').'</th><th class="ofo">'.get_string('number','report_detailedreview').'</th><th class="ofo">'.get_string('teacher_msgs','report_detailedreview').'</th><th class="ofo">'.get_string('student_msgs', 'report_detailedreview').'</th><th class="ofo">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table4_impochanged = '<table class="of"><tr><th class="ofo">'.get_string('type','report_detailedreview').'</th><th class="ofo">'.get_string('number','report_detailedreview').'</th><th class="ofo">'.get_string('quiz_cat', 'report_detailedreview').'</th><th class="ofo">'.get_string('quest', 'report_detailedreview').'</th><th class="ofo">'.get_string('stud_quest', 'report_detailedreview').'</th><th class="ofo">'.get_string('quiz_img', 'report_detailedreview').'</th><th class="ofo">'.get_string('quiz_href', 'report_detailedreview').'</th><th class="ofo">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table5_impochanged = '<table class="of"><tr><th class="ofo">'.get_string('type','report_detailedreview').'</th><th class="ofo">'.get_string('number','report_detailedreview').'</th><th class="ofo">'.get_string('hours','report_detailedreview').'</th></tr>';
		$table6_impochanged = '<table class="of"><tr><th class="ofo">'.get_string('type','report_detailedreview').'</th><th class="ofo">'.get_string('number','report_detailedreview').'</th><th class="ofo">'.get_string('card_rub', 'report_detailedreview').'</th><th class="ofo">'.get_string('card_quest', 'report_detailedreview').'</th><th class="ofo">'.get_string('img', 'report_detailedreview').'</th><th class="ofo">'.get_string('href', 'report_detailedreview').'</th><th class="ofo">'.get_string('hours','report_detailedreview').'</th></tr>';

		//обнуляем все флаги
		$table1_null=true;
		$table2_null=true;
		$table3_null=true;
		$table4_null=true;
		$table5_null=true;
		$table6_null=true;
		$table1_imported_null=true;
		$table2_imported_null=true;
		$table3_imported_null=true;
		$table4_imported_null=true;
		$table5_imported_null=true;
		$table6_imported_null=true;
		$table1_impochanged_null=true;
		$table2_impochanged_null=true;
		$table3_impochanged_null=true;
		$table4_impochanged_null=true;
		$table5_impochanged_null=true;
		$table6_impochanged_null=true;

		foreach($res as $row) {
			$i=0;
			$img_resource_number=0;
			$href_resource_number=0;
				
			switch($row->name) {
					
				case 'resource' :{
					foreach(calc_resources($cid) as $result) {
						$resources_new = $result['resources_new'];
						$resources_old = $result['resources_old'];
						$resources_mod = $result['resources_mod'];
					}
					if ($resources_new > 0) {
						$table5_null = false;
						$cost = round($resources_new*mod_cost('file')*100)/100;
						$table5 = $table5.'<td class="of nowrap">'.get_string('resource','report_detailedreview').'</td><td class="of nowrap">'.$resources_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($resources_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($resources_old*mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('resource','report_detailedreview').'</td><td class="of nowrap">'.$resources_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($resources_mod > 0) {
						$table5_impochanged_null = false;
						$cost_mod = round(($urls_mod*mod_cost('file')*mod_cost('changed') + $urls_mod*mod_cost('imported'))*100)/100;
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('resource','report_detailedreview').'</td><td class="of nowrap">'.$resources_mod.'</td><td class="of nowrap">'.$cost_mod.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;
					
				case 'folder':{
					foreach (calc_folders($cid) as $result) {
						$folders_new=$result['folders_new'];
						$folders_old=$result['folders_old'];
						$folders_mod=$result['folders_mod'];
					}
					if ($folders_new > 0) {
						$table5_null = false;
						$cost = round($folders_new * mod_cost('directory')*100)/100;
						$table5 = $table5.'<td class="of nowrap">'.get_string('folder','report_detailedreview').'</td><td class="of nowrap">'.$folders_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($folders_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($folders_old * mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('folder','report_detailedreview').'</td><td class="of nowrap">'.$folders_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($folders_mod > 0) {
						$table5_impochanged_null = false;
						$cost_mod = round(($folders_mod * mod_cost('imported') + $dirs_mod * mod_cost('directory')*mod_cost('changed'))*100)/100;
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('folder','report_detailedreview').'</td><td class="of nowrap">'.$folders_mod.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;
					
				case 'url' :{
					foreach(calc_urls($cid) as $result) {
						$urls_new = $result['urls_new'];
						$urls_old = $result['urls_old'];
						$urls_mod = $result['urls_mod'];
					}
					if ($urls_new > 0) {
						$table5_null = false;
						$cost = round($urls_new*mod_cost('link')*100)/100;
						$table5 = $table5.'<td class="of nowrap">'.get_string('url','report_detailedreview').'</td><td class="of nowrap">'.$urls_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($urls_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($urls_old*mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('url','report_detailedreview').'</td><td class="of nowrap">'.$urls_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($urls_mod > 0) {
						$table5_impochanged_null = false;
						$cost_mod = round(($urls_mod*mod_cost('href')*mod_cost('changed') + $urls_mod*mod_cost('imported'))*100)/100;
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('url','report_detailedreview').'</td><td class="of nowrap">'.$urls_mod.'</td><td class="of nowrap">'.$cost_mod.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;
					
				case 'page':{
					foreach (calc_pages($cid) as $result) {
						$pages_new = $result['pages_new'];
						$pages_old = $result['pages_old'];
						$pages_mod = $result['pages_mod'];
						$img_new = $result['img_new'];
						$img_mod = $result['img_mod'];
						$href_new = $result['href_new'];
						$href_mod = $result['href_mod'];
					}
					if (($pages_new > 0)) {
						$table1_null = false;
						$cost = round(($pages_new*mod_cost('page') + $img_new*mod_cost('img') + $href_new*mod_cost('href'))*100)/100;
						$table1 = $table1.'<td class="of nowrap">'.get_string('page','report_detailedreview').'</td><td class="of nowrap">'.$pages_new.'</td><td class="of nowrap">'.$img_new.'</td><td class="of nowrap">'.$href_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($pages_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($pages_old*mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('page','report_detailedreview').'</td><td class="of nowrap">'.$pages_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($pages_mod > 0) {
						$table1_impochanged_null = false;
						$cost_mod = round((($pages_mod*mod_cost('page') + $img_mod*mod_cost('img') + $href_mod*mod_cost('href'))*mod_cost('changed') + $pages_mod*mod_cost('imported'))*100)/100;
						$table1_impochanged = $table1_impochanged.'<td class="of nowrap">'.get_string('page','report_detailedreview').'</td><td class="of nowrap">'.$pages_mod.'</td><td class="of nowrap">'.$img_mod.'</td><td class="of nowrap">'.$href_mod.'</td><td class="of nowrap">'.$cost_mod.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
						
				} break;

				case 'openmeetings':{
					foreach (calc_opms($cid) as $result) {
						$opms_new=$result['opms_new'];
						$opms_old=$result['opms_old'];
						$opms_mod=$result['opms_mod'];
					}
					if ($opms_new > 0) {
						$table5_null = false;
						$cost = round($opms_new * mod_cost('openmeetings')*100)/100;
						$table5 = $table5.'<td class="of nowrap">'.get_string('openmeetings','report_detailedreview').'</td><td class="of nowrap">'.$opms_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($opms_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($opms_old * mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('openmeetings','report_detailedreview').'</td><td class="of nowrap">'.$opms_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($opms_mod > 0) {
						$table5_impochanged_null = false;
						$cost_mod = round(($opms_mod * mod_cost('imported') + $opms_mod * mod_cost('openmeetings')*mod_cost('changed'))*100)/100;
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('openmeetings','report_detailedreview').'</td><td class="of nowrap">'.$opms_mod.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;

				case 'scorm':{
					foreach (calc_scorms($cid) as $result) {
						$scorms_new=$result['scorms_new'];
						$scorms_old=$result['scorms_old'];
						$scorms_mod=$result['scorms_mod'];
					}
					if ($scorms_new > 0) {
						$table5_null = false;
						$cost = round($scorms_new * mod_cost('scorm')*100)/100;
						$table5 = $table5.'<td class="of nowrap">'.get_string('scorm','report_detailedreview').'</td><td class="of nowrap">'.$scorms_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($scorms_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($scorms_old * mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('scorm','report_detailedreview').'</td><td class="of nowrap">'.$scorms_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($scorms_mod > 0) {
						$table5_impochanged_null = false;
						$cost_mod = round(($scorms_mod * mod_cost('imported') + $scorms_mod * mod_cost('scorm')*mod_cost('changed'))*100)/100;
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('scorm','report_detailedreview').'</td><td class="of nowrap">'.$scorms_mod.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;

				case 'choice':{
					foreach (calc_choices($cid) as $result) {
						$choices_new=$result['choices_new'];
						$choices_old=$result['choices_old'];
						$choices_mod=$result['choices_mod'];
					}
					if ($choices_new > 0) {
						$table5_null = false;
						$cost = round($choices_new * mod_cost('choice')*100)/100;
						$table5 = $table5.'<td class="of nowrap">'.get_string('choice','report_detailedreview').'</td><td class="of nowrap">'.$choices_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($choices_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($choices_old * mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('choice','report_detailedreview').'</td><td class="of nowrap">'.$choices_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($choices_mod > 0) {
						$table5_impochanged_null = false;
						$cost_mod = round(($choices_mod * mod_cost('imported') + $choices_mod * mod_cost('choice')*mod_cost('changed'))*100)/100;
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('choice','report_detailedreview').'</td><td class="of nowrap">'.$choices_mod.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;

				case 'data':{
					foreach (calc_datas($cid) as $result) {
						$datas_new=$result['datas_new'];
						$datas_old=$result['datas_old'];
						$datas_mod=$result['datas_mod'];
					}
					if ($datas_new > 0) {
						$table5_null = false;
						$cost = round($datas_new * mod_cost('data')*100)/100;
						$table5 = $table5.'<td class="of nowrap">'.get_string('data','report_detailedreview').'</td><td class="of nowrap">'.$datas_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($datas_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($datas_old * mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('data','report_detailedreview').'</td><td class="of nowrap">'.$datas_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($datas_mod > 0) {
						$table5_impochanged_null = false;
						$cost_mod = round(($datas_mod * mod_cost('imported') + $datas_mod * mod_cost('data')*mod_cost('changed'))*100)/100;
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('data','report_detailedreview').'</td><td class="of nowrap">'.$datas_mod.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;

				case 'survey':{
					foreach (calc_surveys($cid) as $result) {
						$surveys_new=$result['surveys_new'];
						$surveys_old=$result['surveys_old'];
						$surveys_mod=$result['surveys_mod'];
					}
					if ($surveys_new > 0) {
						$table5_null = false;
						$cost = round($surveys_new * mod_cost('survey')*100)/100;
						$table5 = $table5.'<td class="of nowrap">'.get_string('survey','report_detailedreview').'</td><td class="of nowrap">'.$surveys_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($surveys_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($surveys_old * mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('survey','report_detailedreview').'</td><td class="of nowrap">'.$surveys_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($surveys_mod > 0) {
						$table5_impochanged_null = false;
						$cost_mod = round(($surveys_mod * mod_cost('imported') + $surveys_mod * mod_cost('survey')*mod_cost('changed'))*100)/100;
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('survey','report_detailedreview').'</td><td class="of nowrap">'.$surveys_mod.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;

				case 'questionnaire':{
					foreach (calc_questionnaires($cid) as $result) {
						$questionnaires_new=$result['questionnaires_new'];
						$questionnaires_old=$result['questionnaires_old'];
						$questionnaires_mod=$result['questionnaires_mod'];
					}
					if ($questionnaires_new > 0) {
						$table5_null = false;
						$cost = round($questionnaires_new * mod_cost('questionnaire')*100)/100;
						$table5 = $table5.'<td class="of nowrap">'.get_string('questionnaire','report_detailedreview').'</td><td class="of nowrap">'.$questionnaires_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($questionnaires_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($questionnaires_old * mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('questionnaire','report_detailedreview').'</td><td class="of nowrap">'.$questionnaires_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($questionnaires_mod > 0) {
						$table5_impochanged_null = false;
						$cost_mod = round(($questionnaires_mod * mod_cost('imported') + $questionnaires_mod * mod_cost('questionnaire')*mod_cost('changed'))*100)/100;
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('questionnaire','report_detailedreview').'</td><td class="of nowrap">'.$questionnaires_mod.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;

				case 'feedback':{
					foreach (calc_feedbacks($cid) as $result) {
						$feedbacks_new=$result['feedbacks_new'];
						$feedbacks_old=$result['feedbacks_old'];
						$feedbacks_mod=$result['feedbacks_mod'];
					}
					if ($feedbacks_new > 0) {
						$table5_null = false;
						$cost = round($feedbacks_new * mod_cost('feedback')*100)/100;
						$table5 = $table5.'<td class="of nowrap">'.get_string('feedback','report_detailedreview').'</td><td class="of nowrap">'.$feedbacks_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($feedbacks_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($feedbacks_old * mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('feedback','report_detailedreview').'</td><td class="of nowrap">'.$feedbacks_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($feedbacks_mod > 0) {
						$table5_impochanged_null = false;
						$cost_mod = round(($feedbacks_mod * mod_cost('imported') + $feedbacks_mod * mod_cost('feedback')*mod_cost('changed'))*100)/100;
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('feedback','report_detailedreview').'</td><td class="of nowrap">'.$feedbacks_mod.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;
				
				case 'workshop':{
					foreach (calc_workshops($cid) as $result) {
						$workshops_new=$result['workshops_new'];
						$workshops_old=$result['workshops_old'];
						$workshops_mod=$result['workshops_mod'];
					}
					if ($workshops_new > 0) {
						$table5_null = false;
						//	$cost = $workshops_new * mod_cost('workshop');
						$table5 = $table5.'<td class="of nowrap">'.get_string('workshop','report_detailedreview').'</td><td class="of nowrap">'.$workshops_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
					}
					if ($workshops_old > 0) {
						$table5_imported_null = false;
						//	$cost_old = $workshops_old * mod_cost('imported');
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('workshop','report_detailedreview').'</td><td class="of nowrap">'.$workshops_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
					}
					if ($workshops_mod > 0) {
						$table5_impochanged_null = false;
						//	$cost = $workshops_mod * mod_cost('imported') + $workshops_mod * mod_cost('workshop')*mod_cost('changed');
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('workshop','report_detailedreview').'</td><td class="of nowrap">'.$workshops_mod.'</td><td class="of nowrap">'.$cost.'</td></tr>';
					}
				} break;

				case 'journal':{
					foreach (calc_journals($cid) as $result) {
						$journals_new=$result['journals_new'];
						$journals_old=$result['journals_old'];
						$journals_mod=$result['journals_mod'];
					}
					if ($journals_new > 0) {
						$table5_null = false;
						//	$cost = $journals_new * mod_cost('journal');
						$table5 = $table5.'<td class="of nowrap">'.get_string('journal','report_detailedreview').'</td><td class="of nowrap">'.$journals_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
					}
					if ($journals_old > 0) {
						$table5_imported_null = false;
						//	$cost_old = $journals_old * mod_cost('imported');
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('journal','report_detailedreview').'</td><td class="of nowrap">'.$journals_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
					}
					if ($journals_mod > 0) {
						$table5_impochanged_null = false;
						//	$cost = $journals_mod * mod_cost('imported') + $journals_mod * mod_cost('journal')*mod_cost('changed');
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('journal','report_detailedreview').'</td><td class="of nowrap">'.$journals_mod.'</td><td class="of nowrap">'.$cost.'</td></tr>';
					}
				} break;

				case 'lesson': {
					foreach (calc_lessons($cid) as $result) {
						$lessons_new = $result['lessons_new'];
						$lessons_old = $result['lessons_old'];
						$lessons_mod = $result['lessons_mod'];
						$new_pgs = $result['new_pgs'];
						$old_pgs = $result['old_pgs'];
						$mod_pgs = $result['mod_pgs'];
						$new_pgs_wq = $result['new_pgs_wq'];
						$old_pgs_wq = $result['old_pgs_wq'];
						$mod_pgs_wq = $result['mod_pgs_wq'];
						$imgs_new = $result['imgs_new'];
						$hrefs_new = $result['hrefs_new'];
						$imgs_old = $result['imgs_old'];
						$hrefs_old = $result['hrefs_old'];
						$imgs_mod = $result['imgs_mod'];
						$hrefs_mod = $result['hrefs_mod'];
					}

					if (($lessons_new > 0)|($new_pgs > 0)|($new_pgs_wq > 0)|($imgs_new > 0)|($hrefs_new > 0)) {
						$table6_null = false;
						$cost = round(($lessons_new*mod_cost('lesson') + $new_pgs*mod_cost('card_rub') + $new_pgs_wq*mod_cost('card_quest') + $imgs_new*mod_cost('img') + $hrefs_new*mod_cost('href'))*100)/100;
						$table6 = $table6.'<td class="of nowrap">'.get_string('lesson','report_detailedreview').'</td><td class="of nowrap">'.$lessons_new.'</td><td class="of nowrap">'.$new_pgs.'</td><td class="of nowrap">'.$new_pgs_wq.'</td><td class="of nowrap">'.$imgs_new.'</td><td class="of nowrap">'.$hrefs_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if (($lessons_old > 0)) {
						$table5_imported_null = false;
						$cost_old = round($lessons_old*mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('lesson','report_detailedreview').'</td><td class="of nowrap">'.$lessons_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if (($lessons_mod > 0)|($mod_pgs > 0)|($mod_pgs_wq > 0)|($imgs_mod > 0)|($hrefs_mod > 0)) {
						$table6_impochanged_null = false;
						$cost_mod = round(($lessons_mod*mod_cost('imported') + ($lessons_mod*mod_cost('lesson') + $mod_pgs*mod_cost('card_rub') + $mod_pgs_wq*mod_cost('card_quest') + $imgs_mod*mod_cost('img') + $hrefs_mod*mod_cost('href'))*mod_cost('changed'))*100)/100;
						$table6_impochanged = $table6_impochanged.'<td class="of nowrap">'.get_string('lesson','report_detailedreview').'</td><td class="of nowrap">'.$lessons_mod.'</td><td class="of nowrap">'.$mod_pgs.'</td><td class="of nowrap">'.$mod_pgs_wq.'</td><td class="of nowrap">'.$imgs_mod.'</td><td class="of nowrap">'.$hrefs_mod.'</td><td class="of nowrap">'.$cost_mod.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}

				} break;

				case 'glossary': {
					foreach (calc_glossaries($cid) as $result) {
						$glossaries_new = $result['glossaries_new'];
						$glossaries_old = $result['glossaries_old'];
						$glossaries_mod = $result['glossaries_mod'];
						$new_entries = $result['new_entries'];
						$old_entries = $result['old_entries'];
						$mod_entries = $result['mod_entries'];
						$imgs_new = $result['imgs_new'];
						$hrefs_new = $result['hrefs_new'];
						$imgs_old = $result['imgs_old'];
						$hrefs_old = $result['hrefs_old'];
						$imgs_mod = $result['imgs_mod'];
						$hrefs_mod = $result['hrefs_mod'];
					}
						
					if (($glossaries_new > 0)|($new_entries > 0)|($imgs_new > 0)|($hrefs_new > 0)) {
						$table2_null = false;
						$cost = round(($glossaries_new*mod_cost('glossary') + $new_entries*mod_cost('glossary_qty') + $imgs_new*mod_cost('img') + $hrefs_new*mod_cost('href'))*100)/100;
						$table2 = $table2.'<td class="of nowrap">'.get_string('glossary','report_detailedreview').'</td><td class="of nowrap">'.$glossaries_new.'</td><td class="of nowrap">'.$new_entries.'</td><td class="of nowrap">'.$imgs_new.'</td><td class="of nowrap">'.$hrefs_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($glossaries_old > 0) {
						$table5_imported_null = false;
						$cost_imp = round($glossaries_old*mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('glossary','report_detailedreview').'</td><td class="of nowrap">'.$glossaries_old.'</td><td class="of nowrap">'.$cost_imp.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if (($glossaries_mod > 0)|($mod_entries > 0)|($imgs_mod > 0)|($hrefs_mod > 0)) {
						$table2_impochanged_null = false;
						$cost_mod = round(($glossaries_mod*mod_cost('imported') + ($glossaries_mod*mod_cost('glossary') + $mod_entries*mod_cost('glossary_qty') + $imgs_mod*mod_cost('img') + $hrefs_mod*mod_cost('href'))*mod_cost('changed'))*100)/100;
						$table2_impochanged = $table2_impochanged.'<td class="of nowrap">'.get_string('glossary','report_detailedreview').'</td><td class="of nowrap">'.$glossaries_mod.'</td><td class="of nowrap">'.$mod_entries.'</td><td class="of nowrap">'.$imgs_mod.'</td><td class="of nowrap">'.$hrefs_mod.'</td><td class="of nowrap">'.$cost_mod.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;

				case 'assignment': {
					foreach (calc_assignments($cid) as $result) {
						$assignments_new = $result['assignments_new'];
						$assignments_old = $result['assignments_old'];
						$assignments_mod = $result['assignments_mod'];
						$imgs_new = $result['imgs_new'];
						$hrefs_new = $result['hrefs_new'];
						$imgs_old = $result['imgs_old'];
						$hrefs_old = $result['hrefs_old'];
						$imgs_mod = $result['imgs_mod'];
						$hrefs_mod = $result['hrefs_mod'];
					}
					if ($assignments_new > 0) {
						$table1_null = false;
						$cost = round(($assignments_new * mod_cost('assignment') + $imgs_new*mod_cost('img') + $hrefs_new*mod_cost('href'))*100)/100;
						$table1 = $table1.'<td class="of nowrap">'.get_string('assignment','report_detailedreview').'</td><td class="of nowrap">'.$assignments_new.'</td><td class="of nowrap">'.$imgs_new.'</td><td class="of nowrap">'.$hrefs_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($assignments_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($assignments_old * mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('assignment','report_detailedreview').'</td><td class="of nowrap">'.$assignments_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($assignments_mod > 0) {
						$table1_impochanged_null = false;
						$cost_mod =  round(($assignments_mod * mod_cost('imported') + ($assignments_mod * mod_cost('assignment') + $imgs_mod*mod_cost('img') + $hrefs_mod*mod_cost('href'))*mod_cost('changed'))*100)/100;
						$table1_impochanged = $table1_impochanged.'<td class="of nowrap">'.get_string('assignment','report_detailedreview').'</td><td class="of nowrap">'.$assignments_mod.'</td><td class="of nowrap">'.$imgs_mod.'</td><td class="of nowrap">'.$hrefs_mod.'</td><td class="of nowrap">'.$cost_mod.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;

				case 'assign': {
					foreach (calc_assigns($cid) as $result) {
						$assigns_new = $result['assigns_new'];
						$assigns_old = $result['assigns_old'];
						$assigns_mod = $result['assigns_mod'];
						$imgs_new = $result['imgs_new'];
						$hrefs_new = $result['hrefs_new'];
						$imgs_old = $result['imgs_old'];
						$hrefs_old = $result['hrefs_old'];
						$imgs_mod = $result['imgs_mod'];
						$hrefs_mod = $result['hrefs_mod'];
					}
					if ($assigns_new > 0) {
						$table1_null = false; //стоимость assign равна стоимости assignment, правила оценки одинаковые
						$cost = round(($assigns_new * mod_cost('assignment') + $imgs_new*mod_cost('img') + $hrefs_new*mod_cost('href'))*100)/100;
						$table1 = $table1.'<td class="of nowrap">'.get_string('assign','report_detailedreview').'</td><td class="of nowrap">'.$assigns_new.'</td><td class="of nowrap">'.$imgs_new.'</td><td class="of nowrap">'.$hrefs_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($assigns_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($assigns_old * mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('assign','report_detailedreview').'</td><td class="of nowrap">'.$assigns_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($assigns_mod > 0) {
						$table1_impochanged_null = false;
						$cost_mod =  round(($assigns_mod * mod_cost('imported') + ($assigns_mod * mod_cost('assignment') + $imgs_mod*mod_cost('img') + $hrefs_mod*mod_cost('href'))*mod_cost('changed'))*100)/100;
						$table1_impochanged = $table1_impochanged.'<td class="of nowrap">'.get_string('assign','report_detailedreview').'</td><td class="of nowrap">'.$assigns_mod.'</td><td class="of nowrap">'.$imgs_mod.'</td><td class="of nowrap">'.$hrefs_mod.'</td><td class="of nowrap">'.$cost_mod.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;
				
				case 'wiki': {
					foreach(calc_wikis($cid) as $result) {
						$wikis_new = $result['wikis_new'];
						$wikis_old = $result['wikis_old'];
						$wikis_mod = $result['wikis_mod'];
					}
					if ($wikis_new > 0) {
						$table5_null = false;
						$cost = round($wikis_new*mod_cost('wiki')*100)/100;
						$table5 = $table5.'<td class="of nowrap">'.get_string('wiki','report_detailedreview').'</td><td class="of nowrap">'.$wikis_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($wikis_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($wikis_old*mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('wiki','report_detailedreview').'</td><td class="of nowrap">'.$wikis_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($wikis_mod > 0) {
						$table5_impochanged_null = false;
						$cost_mod = round(($wikis_mod*mod_cost('wiki')*mod_cost('changed') + $wikis_mod*mod_cost('imported'))*100)/100;
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('wiki','report_detailedreview').'</td><td class="of nowrap">'.$wikis_mod.'</td><td class="of nowrap">'.$cost_mod.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;

				case 'label': {
					foreach(calc_labels($cid) as $result) {
						$labels_new = $result['labels_new'];
						$labels_old = $result['labels_old'];
						$labels_mod = $result['labels_mod'];
						$imgs_new = $result['imgs_new'];
						$hrefs_new = $result['hrefs_new'];
						$imgs_old = $result['imgs_old'];
						$hrefs_old = $result['hrefs_old'];
						$imgs_mod = $result['imgs_mod'];
						$hrefs_mod = $result['hrefs_mod'];
					}
					if ($labels_new > 0) {
						$table1_null = false;
						$cost =  round(($labels_new*mod_cost('label') + $imgs_new*mod_cost('img') + $hrefs_new*mod_cost('href'))*100)/100;
						$table1 = $table1.'<td class="of nowrap">'.get_string('label','report_detailedreview').'</td><td class="of nowrap">'.$labels_new.'</td><td class="of nowrap">'.$imgs_new.'</td><td class="of nowrap">'.$hrefs_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($labels_old > 0) {
						$table5_imported_null = false;
						$cost_old =  round($labels_old*mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('label','report_detailedreview').'</td><td class="of nowrap">'.$labels_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($labels_mod > 0) {
						$table1_impochanged_null = false;
						$cost_mod = round((($labels_mod*mod_cost('label') + $imgs_mod*mod_cost('img') + $hrefs_mod*mod_cost('href'))*mod_cost('changed') + $labels_mod*mod_cost('imported'))*100)/100;
						$table1_impochanged = $table1_impochanged.'<td class="of nowrap">'.get_string('label','report_detailedreview').'</td><td class="of nowrap">'.$labels_mod.'</td><td class="of nowrap">'.$imgs_mod.'</td><td class="of nowrap">'.$hrefs_mod.'</td><td class="of nowrap">'.$cost_mod.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;

				case 'quiz': {
					foreach(calc_quizes($cid) as $result) {
						$quizes_new=$result['quizes_new'];
						$quizes_old=$result['quizes_old'];
						$quizes_mod=$result['quizes_mod'];
						$cat_new=$result['cat_new'];
						$cat_old=$result['cat_old'];
						//$cat_mod=$result['cat_mod'];
						$qst_no_fb_new=$result['qst_no_fb_new'];
						$qst_no_fb_old=$result['qst_no_fb_old'];
						$qst_no_fb_mod=$result['qst_no_fb_mod'];
						$qst_new=$result['qst_new'];
						$qst_old=$result['qst_old'];
						$qst_mod=$result['qst_mod'];
						$imgs_new=$result['imgs_new'];
						$hrefs_new=$result['hrefs_new'];
						$imgs_old=$result['imgs_old'];
						$hrefs_old=$result['hrefs_old'];
						$imgs_mod=$result['imgs_mod'];
						$hrefs_mod=$result['hrefs_mod'];
					}

					if (($quizes_new > 0)|($cat_new > 0)|($qst_no_fb_new > 0)|($qst_new > 0)|($imgs_new > 0)|($hrefs_new > 0)) {
						$table4_null = false;
						$cost = round(($quizes_new*mod_cost('quiz') + $cat_new*mod_cost('quiz_cat') + $qst_no_fb_new*mod_cost('quest') + $qst_new*mod_cost('stud_quest') + $imgs_new*mod_cost('quiz_img') + $hrefs_new*mod_cost('quiz_href'))*100)/100;
						$table4 = $table4.'<td class="of nowrap">'.get_string('quiz','report_detailedreview').'</td><td class="of nowrap">'.$quizes_new.'</td><td class="of nowrap">'.$cat_new.'</td><td class="of nowrap">'.$qst_no_fb_new.'</td></td><td class="of nowrap">'.$qst_new.'</td><td class="of nowrap">'.$imgs_new.'</td><td class="of nowrap">'.$hrefs_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}

					if (($quizes_old > 0)|($qst_no_fb_old > 0)|($qst_old > 0)) {
						/*$table4_imported_null = false; //для более углубленного анализа
							$cost_old = round(($quizes_old*mod_cost('quiz') + $cat_old*mod_cost('quiz_cat') + $qst_no_fb_old*mod_cost('quest') + $qst_old*mod_cost('stud_quest') + $imgs_old*mod_cost('quiz_img') + $hrefs_old*mod_cost('quiz_href'))*mod_cost('imported')*100)/100;
						$table4_imported = $table4_imported.'<td class="of nowrap">'.get_string('quiz','report_detailedreview').'</td><td class="of nowrap">'.$quizes_old.'</td><td class="of nowrap">'.$cat_old.'</td><td class="of nowrap">'.$qst_no_fb_old.'</td></td><td class="of nowrap">'.$qst_old.'</td><td class="of nowrap">'.$imgs_old.'</td><td class="of nowrap">'.$hrefs_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						*/
						$table5_imported_null = false;
						$cost_old = round($quizes_old*mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('quiz','report_detailedreview').'</td><td class="of nowrap">'.$quizes_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
							
						$summary_cost_old +=$cost_old;
					}

					//if (($quizes_mod > 0)|($cat_mod > 0)|($qst_no_fb_mod > 0)|($qst_mod > 0)|($imgs_mod > 0)|($hrefs_mod > 0)) {
					if (($quizes_mod > 0)|($qst_no_fb_mod > 0)|($qst_mod > 0)|($imgs_mod > 0)|($hrefs_mod > 0)) {
						$table4_impochanged_null = false;
						//	$cost_mod = round((($quizes_mod*mod_cost('quiz') + $cat_mod*mod_cost('quiz_cat') + $qst_no_fb_mod*mod_cost('quest') + $qst_mod*mod_cost('stud_quest') + $imgs_mod*mod_cost('quiz_img') + $hrefs_mod*mod_cost('quiz_href'))*mod_cost('changed') + $quizes_mod*mod_cost('imported'))*100)/100;
						$cost_mod = round((($quizes_mod*mod_cost('quiz') + $qst_no_fb_mod*mod_cost('quest') + $qst_mod*mod_cost('stud_quest') + $imgs_mod*mod_cost('quiz_img') + $hrefs_mod*mod_cost('quiz_href'))*mod_cost('changed') + $quizes_mod*mod_cost('imported'))*100)/100;
						//	$table4_impochanged = $table4_impochanged.'<td class="of nowrap">'.get_string('quiz','report_detailedreview').'</td><td class="of nowrap">'.$quizes_mod.'</td><td class="of nowrap">'.$cat_mod.'</td><td class="of nowrap">'.$qst_no_fb_mod.'</td></td><td class="of nowrap">'.$qst_mod.'</td><td class="of nowrap">'.$imgs_mod.'</td><td class="of nowrap">'.$hrefs_mod.'</td><td class="of nowrap">'.$cost_mod.'</td></tr>';
						$table4_impochanged = $table4_impochanged.'<td class="of nowrap">'.get_string('quiz','report_detailedreview').'</td><td class="of nowrap">'.$quizes_mod.'</td><td class="of nowrap">'.'0'.'</td><td class="of nowrap">'.$qst_no_fb_mod.'</td></td><td class="of nowrap">'.$qst_mod.'</td><td class="of nowrap">'.$imgs_mod.'</td><td class="of nowrap">'.$hrefs_mod.'</td><td class="of nowrap">'.$cost_mod.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;

				case 'chat': {
					foreach(calc_chats($cid) as $result) {
						$chats_new = $result['chats_new'];
						$chats_old = $result['chats_old'];
						$chats_mod = $result['chats_mod'];
						$msgs_prep_new = $result['msgs_prep_new'];
						$msgs_prep_old = $result['msgs_prep_old'];
						$msgs_prep_mod = $result['msgs_prep_mod'];
						$msgs_stud_new = $result['msgs_stud_new'];
						$msgs_stud_old = $result['msgs_stud_old'];
						$msgs_stud_mod = $result['msgs_stud_mod'];
					}
					if (($chats_new > 0)|($msgs_prep_new > 0)|($msgs_stud_new > 0)) {
						$table3_null = false;
						$cost = round(($chats_new*mod_cost('chat') + $msgs_prep_new*mod_cost('chat_teacher_msgs') + $msgs_stud_new*mod_cost('chat_student_msgs'))*100)/100;
						$table3 = $table3.'<td class="of nowrap">'.get_string('chat','report_detailedreview').'</td><td class="of nowrap">'.$chats_new.'</td><td class="of nowrap">'.$msgs_prep_new.'</td><td class="of nowrap">'.$msgs_stud_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($chats_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($chats_old*mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('chat','report_detailedreview').'</td><td class="of nowrap">'.$chats_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if (($chats_mod > 0)|($msgs_prep_mod > 0)|($msgs_stud_mod > 0)) {
						$table3_impochanged_null = false;
						$cost_mod = round((($chats_mod*mod_cost('chat') + $msgs_prep_mod*mod_cost('chat_teacher_msgs') + $msgs_stud_mod*mod_cost('chat_student_msgs'))*mod_cost('changed') + $chats_mod*mod_cost('imported'))*100)/100;
						$table3_impochanged = $table3_impochanged.'<td class="of nowrap">'.get_string('chat','report_detailedreview').'</td><td class="of nowrap">'.$chats_mod.'</td><td class="of nowrap">'.$msgs_prep_mod.'</td><td class="of nowrap">'.$msgs_stud_mod.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;

				case 'forum': {

					foreach (calc_forums($cid) as $result) {
						$forums_new = $result['forums_new'];
						$forums_old = $result['forums_old'];
						$forums_mod = $result['forums_mod'];
						$msg_prep_new = $result['msg_prep_new'];
						$msg_prep_old = $result['msg_prep_old'];
						$msg_prep_mod = $result['msg_prep_mod'];
						$msg_stud_new = $result['msg_stud_new'];
						$msg_stud_old = $result['msg_stud_old'];
						$msg_stud_mod = $result['msg_stud_mod'];
					}

					if (!(($forums_new==1)&($msg_prep_new==0)&($msg_stud_new==0))) {
						$table3_null = false;
						$cost= round(($forums_new*mod_cost('forum') + $msg_prep_new*mod_cost('forum_teacher_msgs') + $msg_stud_new*mod_cost('forum_student_msgs'))*100)/100;
						$table3 = $table3.'<td class="of nowrap">'.get_string('forum','report_detailedreview').'</td><td class="of nowrap">'.$forums_new.'</td><td class="of nowrap">'.$msg_prep_new.'</td><td class="of nowrap">'.$msg_stud_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					else $cost=0;//если создан только новостной форум и он пуст
					if ($forums_new==0) $table3_null = true; //fix появление всех форумов с нулями
					if ($forums_old!=0) {
						$table5_imported_null = false;
						$cost_old = round($forums_old*mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('forum','report_detailedreview').'</td><td class="of nowrap">'.$forums_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if (($forums_mod!=0)|($msg_stud_mod!=0)|($msg_prep_mod!=0)) {
						$table3_impochanged_null = false;
						$cost_mod = round(($forums_mod*(mod_cost('imported')+mod_cost('forum')*mod_cost('changed'))+$msg_prep_mod*mod_cost('forum_teacher_msgs')*mod_cost('changed')+$msg_stud_mod*mod_cost('forum_student_msgs')*mod_cost('changed'))*100)/100;
						$table3_impochanged = $table3_impochanged.'<td class="of nowrap">'.get_string('forum','report_detailedreview').'</td><td class="of nowrap">'.$forums_mod.'</td><td class="of nowrap">'.$msg_prep_mod.'</td><td class="of nowrap">'.$msg_stud_mod.'</td><td class="of nowrap">'.$cost_mod.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;

				case 'flv' : {
					foreach(calc_flvs($cid) as $result) {
						$flvs_new = $result['flvs_new'];
						$flvs_old = $result['flvs_old'];
						$flvs_mod = $result['flvs_mod'];
					}
					if ($flvs_new > 0) {
						$table5_null = false;
						$cost = round($flvs_new*mod_cost('flv')*100)/100;
						$table5 = $table5.'<td class="of nowrap">'.get_string('flv','report_detailedreview').'</td><td class="of nowrap">'.$flvs_new.'</td><td class="of nowrap">'.$cost.'</td></tr>';
						$summary_cost_new +=$cost;
					}
					if ($flvs_old > 0) {
						$table5_imported_null = false;
						$cost_old = round($flvs_old*mod_cost('imported')*100)/100;
						$table5_imported = $table5_imported.'<td class="of nowrap">'.get_string('flv','report_detailedreview').'</td><td class="of nowrap">'.$flvs_old.'</td><td class="of nowrap">'.$cost_old.'</td></tr>';
						$summary_cost_old +=$cost_old;
					}
					if ($flvs_mod > 0) {
						$table5_impochanged_null = false;
						$cost_mod = round(($flvs_mod*mod_cost('flv')*mod_cost('changed') + $flvs_mod*mod_cost('imported'))*100)/100;
						$table5_impochanged = $table5_impochanged.'<td class="of nowrap">'.get_string('flv','report_detailedreview').'</td><td class="of nowrap">'.$flvs_mod.'</td><td class="of nowrap">'.$cost_mod.'</td></tr>';
						$summary_cost_mod +=$cost_mod;
					}
				} break;



				default: {
					if (($row->name=='wiki')|($row->name=='glossary'))
						$table2 = $table2.'<td class="of nowrap"></td><td class="of nowrap"></td><td class="of nowrap"></td></tr>';
					else if (($row->name=='forum')|($row->name=='chat'))
						$table3 = $table3.'<td class="of nowrap"></td><td class="of nowrap"></td><td class="of nowrap"></td></tr>';
					else if ($row->name=='quiz')
						$table4 = $table4.'<td class="of nowrap"></td><td class="of nowrap"></td><td class="of nowrap"></td><td class="of nowrap"></td></tr>';
					else if ($row->name=='openmeetings')
						$table5 = $table5.'<td class="of nowrap"></td></tr>';
					else if ($row->name=='lesson')
						$table6 = $table6.'<td class="of nowrap"></td><td class="of nowrap"></td><td class="of nowrap"></td><td class="of nowrap"></td><td class="of nowrap"></td></tr>';
					//else echo $row->name;
					//$table1 = $table1.'<td class="of nowrap"></td><td class="of nowrap"></td><td class="of nowrap"></td></tr>';
				}

			}
				
		}

		$table1=$table1."</table>";
		$table2=$table2."</table>";
		$table3=$table3."</table>";
		$table4=$table4."</table>";
		$table5=$table5."</table>";
		$table6=$table6."</table>";

		$table1_imported=$table1_imported."</table>";
		$table2_imported=$table2_imported."</table>";
		$table3_imported=$table3_imported."</table>";				//закрываем теги всех таблиц
		$table4_imported=$table4_imported."</table>";
		$table5_imported=$table5_imported."</table>";
		$table6_imported=$table6_imported."</table>";

		$table1_impochanged=$table1_impochanged."</table>";
		$table2_impochanged=$table2_impochanged."</table>";
		$table3_impochanged=$table3_impochanged."</table>";
		$table4_impochanged=$table4_impochanged."</table>";
		$table5_impochanged=$table5_impochanged."</table>";
		$table6_impochanged=$table6_impochanged."</table>";

		//считаем бонусы
		$sql_bonus = "SELECT count(id) FROM mdl_hours_bonus WHERE course = $cid";
		$par = $DB->get_records_sql($sql_bonus);
		foreach($par as $b)	{
			$bonus = $b->count;
		}
		$bonus_cost = 0;
		if ($bonus > 0) {
			$arr_bonus = array();
			$sql_bonus = "SELECT count,comment FROM mdl_hours_bonus WHERE course = $cid";
			$par = $DB->get_records_sql($sql_bonus);
			$arr_bonus = $par;
			foreach($arr_bonus as $v){
				$bonus_cost += $v->count;
			}
		}

		if ($summary_cost_old > 3) $summary_cost_old = 3; //такой вот норматив
		$summary_cost=round(($summary_cost_new + $summary_cost_old + $summary_cost_mod + $bonus_cost)*1000)/1000; //округляем итог до тысячных
		if (!$silent) {
			echo "<table style='width: 500px; cellspacing: 5px;'>";

			if ((!$table1_null)|(!$table2_null)|(!$table3_null)|(!$table4_null)|(!$table5_null)|(!$table6_null)) { //если хоть одна таблица не пустая, то рисуем блок
				echo "<tr style='border: solid silver 3px;'><td>
				<table style='width: 400px'>";
				echo "<b>".get_string('New','report_detailedreview')."</b>";
				if (!$table5_null) echo "<tr><td>$table5<br></td></tr>";
				if (!$table1_null) echo "<tr><td>$table1<br></td></tr>";
				if (!$table2_null) echo "<tr><td>$table2<br></td></tr>";
				if (!$table3_null) echo "<tr><td>$table3<br></td></tr>";
				if (!$table4_null) echo "<tr><td>$table4<br></td></tr>";
				if (!$table6_null) echo "<tr><td>$table6<br></td></tr>";
				echo "</table>
				<div style='
				border: 1px dotted #BBBBBB;
				border-radius: 3px 3px 3px 3px;
				width: 400px;
				background: none repeat scroll 0 0 #F3F3F3;'><b>".get_string('hours_chapter','report_detailedreview').": $summary_cost_new</b></div>

				</td></tr>";
			}

			if ((!$table1_imported_null)|(!$table2_imported_null)|(!$table3_imported_null)|
					(!$table4_imported_null)|(!$table5_imported_null)|(!$table6_imported_null)) {//если хоть одна таблица не пустая, то рисуем блок
				echo "<tr style='border: solid silver 3px;'><td>
				<table style='width: 400px'>";
				echo "<br><b>".get_string('Imported','report_detailedreview')."</b>";
				if (!$table5_imported_null) echo "<tr><td>$table5_imported<br></td></tr>";
				if (!$table1_imported_null) echo "<tr><td>$table1_imported<br></td></tr>";
				if (!$table2_imported_null) echo "<tr><td>$table2_imported<br></td></tr>";
				if (!$table3_imported_null) echo "<tr><td>$table3_imported<br></td></tr>";
				if (!$table4_imported_null) echo "<tr><td>$table4_imported<br></td></tr>";
				if (!$table6_imported_null) echo "<tr><td>$table6_imported<br></td></tr>";
					
				echo "</table>
				<div style='
				border: 1px dotted #BBBBBB;
				border-radius: 3px 3px 3px 3px;
				width: 400px;
				background: none repeat scroll 0 0 #F3F3F3;'><b>".get_string('hours_chapter','report_detailedreview').": $summary_cost_old</b></div>

				</td></tr>";
			}

			if ((!$table1_impochanged_null)|(!$table2_impochanged_null)|
					(!$table3_impochanged_null)|(!$table4_impochanged_null)|
					(!$table5_impochanged_null)|(!$table6_impochanged_null)) {//если хоть одна таблица не пустая, то рисуем блок
				echo "<tr style='border: solid silver 3px;'><td>
				<table style='width: 400px'>";
				echo "<br><b>".get_string('Changed','report_detailedreview')."</b>";
				if (!$table5_impochanged_null) echo "<tr><td>$table5_impochanged<br></td></tr>";
				if (!$table1_impochanged_null) echo "<tr><td>$table1_impochanged<br></td></tr>";
				if (!$table2_impochanged_null) echo "<tr><td>$table2_impochanged<br></td></tr>";
				if (!$table3_impochanged_null) echo "<tr><td>$table3_impochanged<br></td></tr>";
				if (!$table4_impochanged_null) echo "<tr><td>$table4_impochanged<br></td></tr>";
				if (!$table6_impochanged_null) echo "<tr><td>$table6_impochanged<br></td></tr>";
				echo "</table>
				<div style='
				border: 1px dotted #BBBBBB;
				border-radius: 3px 3px 3px 3px;
				width: 400px;
				background: none repeat scroll 0 0 #F3F3F3;'><b>".get_string('hours_chapter','report_detailedreview').": $summary_cost_mod</b></div>

				</td></tr>";
			}
				
			if ($bonus > 0) {
				echo "<tr style='border: solid silver 3px;'><td>
				<table class='of'>";
				echo "<br><b>".get_string('Bonus','report_detailedreview')."</b>";
				echo "<tr><th class='ofb'>".get_string('report_hours','report_detailedreview')."</th><th class='ofb'>".get_string('comment','report_detailedreview')."</th></tr>";
				foreach ($arr_bonus as $ab) {
					echo "<tr><td class='of nowrap'>".$ab->count."</td><td class='of nowrap'>".$ab->comment."</td></tr>";
				}
					
				echo "</table><br><div style='
				border: 1px dotted #BBBBBB;
				border-radius: 3px 3px 3px 3px;
				width: 400px;
				background: none repeat scroll 0 0 #F3F3F3;'><b>".get_string('hours_chapter','report_detailedreview').": $bonus_cost</b></div>

				</td></tr>";
			}

			echo "</table>";


			echo "<br><div style='
			border: 1px dotted #BBBBBB;
			border-radius: 3px 3px 3px 3px;
			width: 400px;
			background: none repeat scroll 0 0 #F3F3F3;'><b>".get_string('hours_summary','report_detailedreview').": $summary_cost</b></div>
			<script> var hours = $summary_cost</script>";
		}
	}

	//writing in cache table
	$course_exists = false;
	$sql_check_if_course_exists="SELECT count(cid) FROM mdl_hours_courses WHERE cid=$cid";
	$ifce=$DB->get_record_sql($sql_check_if_course_exists);
		
	if ($ifce->count > 0) $course_exists = true;

	if (!$course_exists) { //creating new entry for course
		$vals = new object();
		$vals->cid = $cid;
		$vals->hours = $summary_cost;
		$vals->timeupdated = round(microtime(true));

		$DB->insert_record('hours_courses', $vals, false);
	}
	else { //updating existing course entry

		$exec_sql = "UPDATE mdl_hours_courses SET hours='$summary_cost', timeupdated=".round(microtime(true))." WHERE cid=$cid";
		$DB->execute($exec_sql);

	}

	return $summary_cost;
}


?>