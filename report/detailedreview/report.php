<?php

// get the script execution start time
$time_start = microtime(true);



// all fairly essential ;)
require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/filelib.php');
require_once('lib.php');


$PAGE->set_url($CFG->wwwroot."/report/detailedreview/report.php");

$cid = required_param('cid', PARAM_INT);

$link_course = $CFG->wwwroot."/course/view.php?id=";

require_capability('moodle/course:update', get_context_instance(CONTEXT_COURSE, $cid));


function get_course_name() {
	global $cid;
	global $DB;
	$sql_courses = "SELECT fullname FROM mdl_course WHERE id=$cid";
	$res = $DB->get_field_sql($sql_courses);
	return $res;
}

require_login();
echo $OUTPUT->header();
echo '<link rel="stylesheet" type="text/css" href="styles.css" />'."\n";

// set up the page
echo $OUTPUT->heading(get_string('report_title', 'report_detailedreview').'<a href="'.$link_course.$cid.'">'.get_course_name().'</a>');
echo '<p>&larr; <a href="javascript: history.go(-1)">'.get_string('report_back', 'report_detailedreview').'</a>.</p>'."\n";

get_course_modules($cid);
    
if (is_closed($cid)) {
	$hh = getClosedHours($cid);
	foreach($hh as $chh) $ch=$chh['hours'];
	echo "<div id='closing_flag' style='display: block;'><b>".get_string('closed_msg', 'report_detailedreview')."</b><br><br>
							<div style='
			border: 1px dotted #BBBBBB;
			border-radius: 3px 3px 3px 3px;
			width: 400px;
			background: none repeat scroll 0 0 #F3F3F3;'><b>".get_string('hours_closed','report_detailedreview').": ".$ch."</b></div></div>";
}
else echo "<div id='closing_flag' style='display: none;'><b>".get_string('closed_msg', 'report_detailedreview')."</b></div>";
  if (has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) { //for administrators only
  	echo "<table><tr>";
  	
  	if (getPlanned($cid)!= 0) {
  		foreach (getPlanned($cid) as $gp) {
	  		$planned = $gp['hours'];
  			$half = $gp['half'];
  			$year = $gp['year'];
  		}
  		
  		echo "<script>
	  		var planned = $planned;
  			var hlf = $half;
  			var year = '$year';
  		</script>";
  		
  		echo "<td>".get_string('planned_hours', 'report_detailedreview')."</td>";
  		
  		echo "<td id='hrs'><b>$planned</b></td></tr>";
  		echo "<tr><td id='hlf'>".get_string('for_half', 'report_detailedreview')."&nbsp";
  		
  		if (($half == "1")|($half == "2")) {
  			echo "&nbsp&nbsp<b>$half</b>&nbsp&nbsp&nbsp&nbsp&nbsp";
  			echo "&nbsp".get_string('half', 'report_detailedreview')."&nbsp</td>";
  		}
  		else if ($half == "012") {
  			echo "&nbsp&nbsp<b>".get_string('All', 'report_detailedreview')."</b>&nbsp&nbsp&nbsp&nbsp&nbsp";
  			echo "&nbsp".get_string('halfs', 'report_detailedreview')."&nbsp</td>";
  		}
  		  		
  		echo "<td id='yr'><b>$year</b>";
  		echo "&nbsp".get_string('of_years', 'report_detailedreview')."</td></tr>";
  		
		echo "</td><img id='loading_plan' style='display:none' src='159.gif'/></tr>";
  		 
  		echo "</table>";
  		echo "<div id='res'>";
  		if (!is_closed($cid)) echo "<input type='button' id='resetbtn' onclick='resetPlanned($cid)' value='".get_string('unset_planned', 'report_detailedreview')."'>";
  		echo "</div>";
  		
  	} else 	{
  		if (is_closed($cid)) {
  			//������ �������� �� ���������	
  			$planned = 0;
  			$half = "012";
  			$year = getDateFromClosing($cid);
  			
  			echo "<script>
  			var planned = $planned;
  			var hlf = '012';
  			var year = '$year';
  			</script>";
  			
  			echo "<td>".get_string('planned_hours', 'report_detailedreview')."</td>";
  			
  			echo "<td id='hrs'><b>$planned</b></td></tr>";
  			echo "<tr><td id='hlf'>".get_string('for_half', 'report_detailedreview')."&nbsp";
  			
  				
  				echo "&nbsp&nbsp<b>".get_string('All', 'report_detailedreview')."</b>&nbsp&nbsp&nbsp&nbsp&nbsp";
  				echo "&nbsp".get_string('halfs', 'report_detailedreview')."&nbsp</td>";
  				
  			
  				echo "<td id='yr'><b>$year</b>";
  				echo "&nbsp".get_string('of_years', 'report_detailedreview')."</td></tr>";
  			
  				echo "</td><img id='loading_plan' style='display:none' src='159.gif'/></tr>";
  					
  				echo "</table>";
  			
  				echo "<div id='res'></div>";
  		} else {
  		$planned = 0;
  		
	  	echo "<td>".get_string('planned_hours', 'report_detailedreview')."</td>";
  	  	
  		echo "<td id='hrs'><input type='text' id='plan' value='".$planned."'></td></tr>";
	  	
  		$half_year = explode(" ",get_half_year());
  		$years = explode("/",$half_year[1]);
   		echo "<tr><td id='hlf'>".get_string('for_half', 'report_detailedreview')."&nbsp";
  		echo "<select id='half'>
  		<option value='$half_year[0]'>$half_year[0]</option>";
  		if ($half_year[0] == "1")
	  		echo "<option value='2'>2</option>";
  		else echo "<option value='1'>1</option>";
  		echo "<option value='012'>All</option>";
  		echo "<option value='0'>0</option>";
  		echo "</select>";
  		echo "&nbsp".get_string('half', 'report_detailedreview')."</td>";
	  	
	  	echo "<td id='yr'><select id='years'>
  			<option value='$half_year[1]'>$half_year[1]</option>
  			<option value='".($years[0]-1)."/".($years[1]-1)."'>".($years[0]-1)."/".($years[1]-1)."</option>
  			<option value='".($years[0]+1)."/".($years[1]+1)."'>".($years[0]+1)."/".($years[1]+1)."</option>
  			</select>";
	  	echo "&nbsp".get_string('of_years', 'report_detailedreview')."</td></tr>";
	  	echo "<tr><td id='res'>";
	  	if (!is_closed($cid)) echo "<input type='button' onclick='setPlanned($cid);' value='".get_string('set_planned', 'report_detailedreview')."'>";
	  	
	  	echo "<img id='loading_plan' style='display:none' src='159.gif'/></td></tr>";
	  	 
	  	echo "</table>";
	  	echo "<script>
	  			var year = '$half_year[1]';
	  		</script>";
  		}
  	}

  	echo "<br><br><table>";
  	
  	
  	echo "<tr><td>".get_string('hours_type', 'report_detailedreview')."</td>";
  	echo "<td>".get_string('hours_to_close_stud', 'report_detailedreview')."</td>";
   	echo "<td>".get_string('hours_to_close_method', 'report_detailedreview')."</td></tr>";
  	
   	echo "<tr><td>".get_string('hours_type_qty', 'report_detailedreview')."</td>";
   	
   	$hours_s = 0;
   	$time_s = round(microtime(true));
   	$hours_m = 0;
   	$time_m = round(microtime(true));
   	
   	if (getClosedHours($cid,'stud') != 0) foreach(getClosedHours($cid,'stud') as $CH) {
   		$hours_s = $CH['hours'];
   		$time_s = $CH['time'];
   		//echo $time;
   	}
   	
   	if (getClosedHours($cid,'method') != 0) foreach(getClosedHours($cid,'method') as $CH) {
   		$hours_m = $CH['hours'];
   		$time_m = $CH['time'];
   		//echo $time;
   	}
   	
   	if ($hours_s == "none") echo "<td id='hrs_s'><input type='text' id='close_stud' value=".$hours_s."></td>";
  	else  echo "<td id='hrs_s'><b>$hours_s</b></td>";
  	if ($hours_m == "none") echo "<td id='hrs_m'><input type='text' id='close_method' value='".$hours_m."'></td></tr>";
  	else echo "<td id='hrs_m'><b>$hours_m</b></td></tr>";
  	
  	echo "<tr><td>".get_string('closing_date', 'report_detailedreview')."</td>";
  	if ($hours_s == "none") echo "<td id='dte_s'><input type='text' id='closing_date_stud' value='".date('d', $time_s).".".date('m', $time_s).".".date('Y', $time_s)."'></td>";
  	else  echo "<td id='dte_s'><b>".date('d', $time_s).".".date('m', $time_s).".".date('Y', $time_s)."</b></td>";
  	if ($hours_m == "none") echo "<td id='dte_m'><input type='text' id='closing_date_method' value='".date('d', $time_m).".".date('m', $time_m).".".date('Y', $time_m)."'></td></tr>";
  	else  echo "<td id='dte_m'><b>".date('d', $time_m).".".date('m', $time_m).".".date('Y', $time_m)."</b></td></tr>";
  	
  	echo "<tr><td></td><td id='csc_s'>";
  	if (!is_closed($cid)) {
  		if ($hours_s != "none") echo "<button id='closing_state_change_stud' onclick='UnSetClosed($cid, \"stud\")'>".get_string('undo_close_hours', 'report_detailedreview')."</button>";
  		else echo "<button id='closing_state_change_stud' onclick='setClosed($cid, \"stud\")'>".get_string('close_hours', 'report_detailedreview')."</button>";
  	}
  	echo "</td>";
  	
  	echo "<td id='csc_m'>";
  	if (!is_closed($cid)) {
  		if ($hours_m != "none") echo "<button id='closing_state_change_method' onclick='UnSetClosed($cid, \"method\")'>".get_string('undo_close_hours', 'report_detailedreview')."</button>";
  		else echo "<button id='closing_state_change_method' onclick='setClosed($cid, \"method\")'>".get_string('close_hours', 'report_detailedreview')."</button>";
  	}
  	echo "</td></tr>";
  	
  	echo "</table>";
  	if (is_closed($cid)) { //���� ���� ������
  		echo "<div id='close_course_div'>";
  		echo "<table><tr><td>".get_string('closing_comment', 'report_detailedreview')."</td><td><textarea id='comment' style='width:500px;'>".getComment($cid)."</textarea></td></tr></table><br>";
  		echo "<input type='button' id='close_course_btn' onclick='UnCloseCourse($cid);' value='".get_string('undo_close_hours', 'report_detailedreview')."'/>";
  		echo "</div>";
  	} else {
  		if (($hours_s !="none")&($hours_m !="none")) { //���� ���� ����� � ��������
  			echo "<div id='close_course_div'>";
  			echo "<table><tr><td>".get_string('closing_comment', 'report_detailedreview')."</td><td><textarea id='comment' style='width:500px;'>".getComment($cid)."</textarea></td></tr></table><br>";
  			echo "<input type='button' id='close_course_btn' onclick='CloseCourse($cid);' value='".get_string('close_all_hours', 'report_detailedreview')."'/>";
  			echo "</div>";
  		}
  		else {
  			echo "<div id='close_course_div'>";
  		//	echo get_string('closing_comment', 'report_detailedreview')."<input type='text' id='comment' style='width:500px;' value='".getComment($cid)."'/><br>";
  			//echo "<input type='button' id='close_course_btn' onclick='UnCloseCourse($cid);' value='".get_string('close_all_hours', 'report_detailedreview')."'/>";
  			echo "</div>";
  		}
  	}
  echo "<script>
	  		var hours_s = $hours_s;
  			var hours_m = $hours_m;
  			var date_s = '".date('d', $time_s).".".date('m', $time_s).".".date('Y', $time_s)."';
  			var date_m = '".date('d', $time_m).".".date('m', $time_m).".".date('Y', $time_m)."';
  		</script>";
  
  	 echo "<script>
	
  	 var cid = $cid;
	 var undo_close_hours = '".get_string('undo_close_hours','report_detailedreview')."'
	 var close_hours = '".get_string('close_hours','report_detailedreview')."'
	 var closed_hours = '".get_string('closed_hours','report_detailedreview')."'
	 var planned_hours = '".get_string('planned_hours','report_detailedreview')."'
	 var for_half = '".get_string('for_half','report_detailedreview')."'
	 var half = '".get_string('half','report_detailedreview')."'
	 var halfs = '".get_string('halfs','report_detailedreview')."'
	 var of_years = '".get_string('of_years','report_detailedreview')."'
	 var set_planned = '".get_string('set_planned','report_detailedreview')."'
	 var unset_planned = '".get_string('unset_planned','report_detailedreview')."'
	 var hours_type = '".get_string('hours_type','report_detailedreview')."'
	 var hours_to_close_stud = '".get_string('hours_to_close_stud','report_detailedreview')."'
	 var hours_to_close_method = '".get_string('hours_to_close_method','report_detailedreview')."'
	 var hours_type_qty = '".get_string( 'hours_type_qty','report_detailedreview')."'
	 var closing_date = '".get_string('closing_date','report_detailedreview')."'
	 var All = '".get_string('All','report_detailedreview')."'
	 var closing_comment = '".get_string('closing_comment', 'report_detailedreview')."'
	 var close_all_hours = '".get_string('close_all_hours', 'report_detailedreview')."'
	 var old_comment = '".getComment($cid)."';
	 
	 </script>";
  	echo "<script type='text/javascript' src='".$CFG->wwwroot."/report/detailedreview/admin_report.js'></script>";
  	echo "<script>setValues();</script>";
}

echo $OUTPUT->footer();

?>