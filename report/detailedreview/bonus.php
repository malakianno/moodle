<?php

// get the script execution start time
$time_start = microtime(true);

// all fairly essential ;)
require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/filelib.php');
require_once('lib.php');

require_login();

$PAGE->set_url($CFG->wwwroot."/report/detailedreview/bonus.php");

admin_externalpage_setup('reportdetailedreview');
echo $OUTPUT->header();
echo "\n\n".'<link rel="stylesheet" type="text/css" href="styles.css" />'."\n";

/**
 * set some vars
 */
// number of lines before the header is repeated
$repeat_header = 50;
// links
$link_course        = $CFG->wwwroot.'/course/view.php?id=';
$link_course_files  = $CFG->wwwroot.'/files/index.php?id=';

// set up the page
echo $OUTPUT->heading(get_string('detailedreview', 'report_detailedreview'));

// some useful functions
function of_print_headers() {
    echo '    <tr>'."\n";
    echo '        <th class="of">'.get_string('index_table_no', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_teacher', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_courses', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_hours', 'report_detailedreview').'</th>'."\n";
    echo '    </tr>'."\n";
}

echo "<ul class='ep_tm_menu'>
<li><img id ='recalcer' src='recalc.jpg' onclick='recalc()' style='width: 18px; height: 18px; position:absolute;'/>__</li>
<li><a href='byteacher.php'>".get_string('head_byteacher', 'report_detailedreview')."</a></li>
<li><a href='index.php'>".get_string('head_bycourse', 'report_detailedreview')."</a></li>
<li><a href='bycategory.php'>".get_string('head_bycategory', 'report_detailedreview')."</a></li>
<li><ai>".get_string('head_add', 'report_detailedreview')."</ai></li>
<li><a href='coef.php'>".get_string('head_coef', 'report_detailedreview')."</a></li>
</ul>";


echo "<script>
var asyncRequest;

function recalc()
{
document.getElementById('recalcer').src='recalc1.jpg';
document.getElementById('recalcer').onclick='';
try
{
//document.getElementById('loading').style.display='block';
asyncRequest = new XMLHttpRequest();
asyncRequest.onreadystatechange = stateChange;
asyncRequest.open('POST', 'recalc.php', true);
asyncRequest.send(null);
}

catch(exception)
{
alert('Request Failed.');
}
}

function stateChange()
{
document.getElementById('loading').style.display='block';

if (asyncRequest.responseText == 'Done!') {
document.getElementById('loading').style.display='none';
document.getElementById('recalcer').src='recalc.jpg';
document.location.reload(true);
}
}

</script>";
echo "<img id='loading' style='display:block' src='159.gif'/><br>";

echo "<br><form action='bonus.php' method='POST'>";
echo "<table>";
echo "<tr><td><a style='color: #A00'>".get_string('bonus_cid', 'report_detailedreview')."</a></td><td><input type='text' name='course' id='cid' size='3' onkeyDown='if(((event.keyCode < 8)&&(event.keyCode > 8)&&(event.keyCode < 48))||(event.keyCode > 57)) return false' onkeypress='if(((event.keyCode > 0)&&(event.keyCode < 8)&&(event.keyCode > 8)&&(event.keyCode < 48))||(event.keyCode > 57)) return false'></td></tr>";
//echo "<tr><td><a style='color: #A00'>".get_string('bonus_cid', 'report_detailedreview')."</a></td><td><input type='text' name='course' id='cid' size='3' onkeyDown='alert(event.keyCode)'></td></tr>";
echo "<tr><td><a style='color: #A00'>".get_string('bonus_hours', 'report_detailedreview')."</a></td><td><input type='text' name='hours' id='id_hours' size='5' onkeyDown='if(((event.keyCode < 8)&&(event.keyCode > 8)&&(event.keyCode < 48))||(event.keyCode > 57)) return false' onkeypress='if(((event.keyCode > 0)&&(event.keyCode < 8)&&(event.keyCode > 8)&&(event.keyCode < 48))||(event.keyCode > 57)) return false'><b>.</b><input type='text' name='mins' id='id_mins' size='2' value='0' onkeyDown='if(((event.keyCode < 8)&&(event.keyCode > 8)&&(event.keyCode < 48))||(event.keyCode > 57)) return false' onkeypress='if(((event.keyCode > 0)&&(event.keyCode < 8)&&(event.keyCode > 8)&&(event.keyCode < 48))||(event.keyCode > 57)) return false'></td></tr>";
echo "<tr><td><a style='color: #A00'>".get_string('bonus_comment', 'report_detailedreview')."</a></td><td><input type='text' name='comment' id='id_comment' size='80'></td></tr>";
echo	"<input type='hidden' name='firsttime' id='firsttime' value='false'/>";
echo "<tr><td></td><td><input type='submit' value='".get_string('bonus_commit', 'report_detailedreview')."'></td></tr>";
echo "</table></form>";
echo "<div style='color: #A00;text-align: right;'>".get_string('bonus_req', 'report_detailedreview')."<img alt='������������ ����' src='../../../pix/req.gif'></div>";



$test = preg_match('/[<>\"\"\'\'\bINSERT\bUPDATE\bSET\bDROP\bDELETE\binsert\bupdate\bset\bdrop\bdelete]/',$_POST["comment"]);
//��������� �����
$mins = 0;
$mins = "0.".$_POST["mins"];
$mins = round($mins*100)/100;
$mins = substr($mins,2);
if ($mins=='') $mins=0;
$hours = ltrim($_POST["hours"],'0');
if ($hours=='') $hours=0;

if ((is_number(ltrim($_POST["course"],'0'))!='')&((is_number(ltrim($hours,'0'))!='')|($hours==0))&((is_number(ltrim($mins,'0'))!='')|($mins==0))&($_POST["course"]!='')&($_POST["hours"]!='')&($_POST["comment"]!='')&($test!=1)&(($hours+mins) > 0)) {
	//��������� ����� id
	$sql_get_maxid = "SELECT MAX(id) FROM mdl_hours_bonus";
	$m = $DB->get_records_sql($sql_get_maxid);
	foreach ($m as $mid) $maxid = $mid->max;
	$newid = $maxid + 1;
/*	
	$vals = new object();
	$vals->id = "'".$newid."'";
	$vals->course = $_POST["course"];
	$vals->date = round(microtime(true));
	$vals->count = $hours.$mins;
	$vals->comment = $_POST["comment"];
	*/
	//$DB->insert_record('hours_bonus', $vals, false);

	$bonus_sql="INSERT INTO mdl_hours_bonus VALUES (".$newid.",".$_POST["course"].",".round(microtime(true)).",".$hours.".".$mins.",'".$_POST["comment"]."')";
	$DB->execute($bonus_sql);

} else if ($_POST["firsttime"]=='false') { 
	if ((is_number($_POST["course"])=='')|($_POST["course"]=='')) {
		echo "<div class='box errorbox errorboxcontent'>".get_string('course_error', 'report_detailedreview')."</div>";
	}
	if (((is_number(ltrim($hours,'0'))!='')&($hours==0))|($_POST["hours"]=='')|((is_number(ltrim($mins,'0'))!='')&($mins==0))|($_POST["mins"]=='')|(($hours+mins) <= 0)) {
		echo "<div class='box errorbox errorboxcontent'>".get_string('hours_error', 'report_detailedreview')."</div>";
	}
	if ($test==1) {
		echo "<div class='box errorbox errorboxcontent'>".get_string('comment_error', 'report_detailedreview')."</div>";
	}
	if ($_POST["comment"]=='') {
		echo "<div class='box errorbox errorboxcontent'>".get_string('comment_empty', 'report_detailedreview')."</div>";
	}
	echo "<script>
	document.getElementById('cid').value = '".$_POST['course']."';
	document.getElementById('id_hours').value = '".$_POST['hours']."';
	document.getElementById('id_mins').value = '".$mins."';
	document.getElementById('id_comment').value = '".$_POST['comment']."';
	</script>";
	
}

$j=$repeat_header;
/*
$sql_pgs = "SELECT oldid, newid, timemodified FROM mdl_resource_old WHERE newmodule = 'page'";
$p = $DB->get_records_sql($sql_pgs);
foreach($p as $page) {
	$DB->set_field('hours_import_log', 'mod_id', $page->newid , array('mod_id'=>$page->oldid, 'type'=>'14'));
	$DB->set_field('hours_import_log', 'type', 24 , array('mod_id'=>$page->newid, 'type'=>'14'));
	$DB->set_field('page', 'timemodified', $page->timemodified , array('id'=>$page->newid));
}

$sql_dirs = "SELECT oldid, newid, timemodified FROM mdl_resource_old WHERE newmodule = 'folder'";
$d = $DB->get_records_sql($sql_dirs);
foreach($d as $dir) {
	$DB->set_field('hours_import_log', 'mod_id', $dir->newid , array('mod_id'=>$dir->oldid, 'type'=>'14'));
	$DB->set_field('hours_import_log', 'type', 22 , array('mod_id'=>$dir->newid, 'type'=>'14'));
	$DB->set_field('folder', 'timemodified', $dir->timemodified , array('id'=>$dir->newid));
}

$sql_urls = "SELECT oldid, newid, timemodified FROM mdl_resource_old WHERE newmodule = 'url'";
$u = $DB->get_records_sql($sql_urls);
foreach($u as $url) {
	$DB->set_field('hours_import_log', 'mod_id', $url->newid , array('mod_id'=>$url->oldid, 'type'=>'14'));
	$DB->set_field('hours_import_log', 'type', 25 , array('mod_id'=>$url->newid, 'type'=>'14'));
	$DB->set_field('url', 'timemodified', $url->timemodified , array('id'=>$url->newid));
}

$sql_ress = "SELECT oldid, newid, timemodified FROM mdl_resource_old WHERE newmodule = 'resource'";
$r = $DB->get_records_sql($sql_ress);
foreach($r as $res) {
	$DB->set_field('hours_import_log', 'mod_id', $res->newid , array('mod_id'=>$res->oldid, 'type'=>'14'));
	$DB->set_field('resource', 'timemodified', $res->timemodified , array('id'=>$res->newid));
}


//$DB->set_field('hours_bonus', 'date', 1223, array('course'=>'1') );

*/
//print_footer();
echo "<script type='text/javascript'>
document.getElementById('loading').style.display='none';
</script>";

echo $OUTPUT->footer();

?>