var asyncRequest;

function Check_ready_state() {
	element1 =  document.getElementById('close_stud');
	element2 =  document.getElementById('close_method');
	if ((!element1)&(!element2)) {
		//document.getElementById('close_course_div').style.display = 'block'; 
		document.getElementById('close_course_div').innerHTML = closing_comment+"<textarea id='comment' style='width:500px;'>"+
				old_comment+"</textarea><br><input type='button' id='close_course_btn' onclick='CloseCourse("+cid+");' value='"+close_all_hours+"'/>";
	}
	else {
		//document.getElementById('close_course_div').style.display = 'none';
		document.getElementById('close_course_div').innerHTML = "";
	}
}


function Check_closing_state() {
	try {
		asyncRequest = new XMLHttpRequest();
		asyncRequest.onreadystatechange = ClosingState;
		
		asyncRequest.open('GET', 'reportServlet.php?cid=' + cid + '&act=check_closed', true);
		asyncRequest.send(null);
	}

	catch (exception) {
		alert(exception);
	}
}

function ClosingState() {
	if (asyncRequest.readyState==4 && asyncRequest.status==200) {
		if (asyncRequest.responseText=='1')
			document.getElementById('closing_flag').style.display = 'block';
		else
			document.getElementById('closing_flag').style.display = 'none';
	}
}


function CloseCourse($cid) {
	var comment = document.getElementById('comment').value;
	var t = new Date();
	try {
		asyncRequest = new XMLHttpRequest();
		asyncRequest.onreadystatechange = CourseClosed;
		asyncRequest.open('GET', 'reportServlet.php?cid=' + cid + '&act=close_course&time='+t+'&comment='+comment, true);
		asyncRequest.send(null);
		document.getElementById('close_course_btn').disabled = true;
	}

	catch (exception) {
		alert(exception);
	}
}

function CourseClosed() {
	if (asyncRequest.readyState==4 && asyncRequest.status==200) {
		document.getElementById('closing_flag').style.display = 'block';
		
		var comment_buf = document.getElementById('comment').value;
		block_course();
		document.getElementById('close_course_div').innerHTML = closing_comment+"<textarea id='comment' style='width:500px;'>"+comment_buf+"</textarea><br>" +
	  	"<input type='button' id='close_course_btn' onclick='UnCloseCourse("+cid+");' value='"+undo_close_hours+"'/>";
		
	}
}

function UnCloseCourse($cid) {
	try {
		asyncRequest = new XMLHttpRequest();
		asyncRequest.onreadystatechange = CourseUnClosed;
		
		asyncRequest.open('GET', 'reportServlet.php?cid=' + cid + '&act=undo_close_course', true);
		asyncRequest.send(null);
		document.getElementById('close_course_btn').disabled = true;
	}

	catch (exception) {
		alert(exception);
	}
}

function CourseUnClosed() {
	if (asyncRequest.readyState==4 && asyncRequest.status==200) {
		document.getElementById('closing_flag').style.display = 'none';
		
		
		resetPlanned();
		var comment_buf = document.getElementById('comment').value;
		
		document.getElementById('csc_s').innerHTML = "<button id='closing_state_change_method' onclick='UnSetClosed("+cid+", \"stud\")'>"+undo_close_hours+"</button>";
		document.getElementById('csc_m').innerHTML = "<button id='closing_state_change_method' onclick='UnSetClosed("+cid+", \"method\")'>"+undo_close_hours+"</button>";
		
		document.getElementById('close_course_div').innerHTML = closing_comment+"<textarea id='comment' style='width:500px;'>"+comment_buf+"</textarea><br>"+
	  	"<input type='button' id='close_course_btn' onclick='CloseCourse("+cid+");' value='"+close_all_hours+"'/>";
		
	}
}

function block_course() {
	setPlanned(cid);
	document.getElementById('res').innerHTML = "";
	document.getElementById('csc_s').innerHTML = "";
	document.getElementById('csc_m').innerHTML = "";
}

function setValues() {
	element =  document.getElementById('plan');
	if (element) planned = document.getElementById('plan').value;
	
	element =  document.getElementById('close_stud');
	if (element) hours_s = document.getElementById('close_stud').value;
	
	element =  document.getElementById('close_method');
	if (element) hours_m = document.getElementById('close_method').value;
	
	if ((planned > 0) && (hours > 0)) {
		if ((hours_m == 0) && (hours_s == 0)) {
			document.getElementById('close_stud').value = planned;
			hours_s = document.getElementById('close_stud').value;
			document.getElementById('close_method').value = Math.round((hours - planned)*100)/100;
			hours_m = document.getElementById('close_method').value;
		}
		if ((hours_m > 0) && (hours_s == 0)) {
			if (hours_m < hours) {
				document.getElementById('close_stud').value = Math.round((hours - hours_m)*100)/100;
				hours_s = document.getElementById('close_stud').value;
			} else {
				document.getElementById('close_stud').value = 0;
				hours_s = 0;
			}
		}
		if ((hours_m == 0) && (hours_s > 0)) {
			if (hours_s < hours) {
				document.getElementById('close_method').value = Math.round((hours - hours_s)*100)/100;
				hours_m = document.getElementById('close_method').value;
			} else {
				document.getElementById('close_method').value = 0;
				hours_m = 0;
			}
		}
	}
	
}

function setPlanned(cid) {
	pl_element = document.getElementById('plan');
	if (pl_element) planned = document.getElementById('plan').value;
	else {
		var b = document.getElementById('hrs').innerHTML;
		planned = b.substring(3,b.lastIndexOf('</b>'));
	}
	element =  document.getElementById('loading_plan');
	if (element) document.getElementById('loading_plan').style.display = 'block';
	
	var qty = planned;
	var half = '';
	var year = '';
	element = document.getElementById('half');
	if (element) half = document.getElementById('half').value;
	else {
		var h = document.getElementById('hlf').innerHTML;
		half = h.substring(h.lastIndexOf('<b>')+3,h.lastIndexOf('</b>'));
	}
	element = document.getElementById('years');
	if (element) year = document.getElementById('years').value.split("/");
	else {
		var y = document.getElementById('yr').innerHTML;
		year = y.substring(3,y.lastIndexOf('</b>')).split("/");
	}
	try {
		asyncRequest = new XMLHttpRequest();
		asyncRequest.onreadystatechange = Planned;
		
		asyncRequest.open('GET', 'reportServlet.php?cid=' + cid + '&act=plan&qty='+qty+'&h='+half+'&y0='+year[0]+'&y1='+year[1], true);
		asyncRequest.send(null);
	}

	catch (exception) {
		alert('Request Failed.');
	}

}

function Planned() {
	if (asyncRequest.readyState==4 && asyncRequest.status==200) {
		element =  document.getElementById('loading_plan');
		if (element) document.getElementById('loading_plan').style.display = 'none';
		block_plan();
		element1 =  document.getElementById('close_stud');
		element2 =  document.getElementById('close_method');
		if ((element1)&(element2)) {
			if (hours > planned) {
				document.getElementById('close_stud').value = planned;
				hours_s = document.getElementById('close_stud').value;
				document.getElementById('close_method').value = Math.round((hours - planned)*100)/100;
				hours_m = document.getElementById('close_method').value;
			} else {
				document.getElementById('close_stud').value = hours;
				hours_s = document.getElementById('close_stud').value;
				document.getElementById('close_method').value = 0;
				hours_m = document.getElementById('close_method').value;
			}
		Check_ready_state();
		}
	}
}


function setClosed(cid, type) {
	element =  document.getElementById('close_stud');
	if (element) hours_s = document.getElementById('close_stud').value;
	
	element =  document.getElementById('close_method');
	if (element) hours_m = document.getElementById('close_method').value;
	
	element =  document.getElementById('closing_date_stud');
	if (element) date_s = document.getElementById('closing_date_stud').value;
	
	element =  document.getElementById('closing_date_method');
	if (element) date_m = document.getElementById('closing_date_method').value;
	
	element =  document.getElementById('loading_plan');
	if (element) document.getElementById('loading_plan').style.display = 'block';
	var qty = document.getElementById('close_'+type).value;
	var date = document.getElementById('closing_date_'+type).value;
	try {
		asyncRequest = new XMLHttpRequest();
		if (type == 'stud') {
			asyncRequest.onreadystatechange = Closed_s;
		} else {
			asyncRequest.onreadystatechange = Closed_m;
		}
		
		var arr_date = date.split("."); 
		var date_unix = Date.UTC(arr_date[2],arr_date[1]-1,arr_date[0],00,09,00)/1000;
		asyncRequest.open('GET', 'reportServlet.php?cid=' + cid + '&act=close_'+type+'&qty='+qty+'&d='+date_unix, true);
		asyncRequest.send(null);
	}

	catch (exception) {
		alert('Request Failed.');
	}

}

function Closed_s() {
	if (asyncRequest.readyState==4 && asyncRequest.status==200) {
		element =  document.getElementById('loading_plan');
		if (element) document.getElementById('loading_plan').style.display = 'none';
		block_hours('stud');
		hours_m = Math.round((hours - hours_s)*100)/100;
		if (hours_m >= 0) {
			element =  document.getElementById('close_method');
			if (element) document.getElementById('close_method').value = hours_m;
		} else hours_m = 0;
	Check_ready_state();
	}
}

function Closed_m() {
	if (asyncRequest.readyState==4 && asyncRequest.status==200) {
		element =  document.getElementById('loading_plan');
		if (element) document.getElementById('loading_plan').style.display = 'none';
		block_hours('method');
		Check_ready_state();
	}
}

function UnSetClosed(cid, type) {

	element =  document.getElementById('loading_plan');
	if (element) document.getElementById('loading_plan').style.display = 'block';
	try {
		asyncRequest = new XMLHttpRequest();
		if  (type == 'stud') {
			asyncRequest.onreadystatechange = UnClosed_s;
		} else
			{
			asyncRequest.onreadystatechange = UnClosed_m;
			}
		asyncRequest.open('GET', 'reportServlet.php?cid=' + cid + '&act=unclose_'+type, true);
		asyncRequest.send(null);
	}

	catch (exception) {
		alert('Request Failed.');
	}

}

function UnClosed_s() {
	if (asyncRequest.readyState==4 && asyncRequest.status==200) {
		element =  document.getElementById('loading_plan');
		if (element) document.getElementById('loading_plan').style.display = 'none';
		unblock_hours('stud');
		Check_ready_state();
	}
}

function UnClosed_m() {
	if (asyncRequest.readyState==4 && asyncRequest.status==200) {
		element =  document.getElementById('loading_plan');
		if (element) document.getElementById('loading_plan').style.display = 'none';
		unblock_hours('method');
		Check_ready_state();
	}
}

function block_plan() {
	element = document.getElementById('plan');
	if (element) {
		planned = document.getElementById('plan').value;
		document.getElementById('hrs').innerHTML = "<b>"+document.getElementById('plan').value+"</b>";
	}
	element = document.getElementById('half');
	if (element) {
		if (document.getElementById('half').value == All) {
			document.getElementById('hlf').innerHTML = for_half+"&nbsp<b>"+document.getElementById('half').value+"</b>&nbsp&nbsp&nbsp&nbsp&nbsp</b>&nbsp"+halfs;
		}
		else
			document.getElementById('hlf').innerHTML = for_half+"&nbsp<b>"+document.getElementById('half').value+"</b>&nbsp&nbsp&nbsp&nbsp&nbsp</b>&nbsp"+half;
			document.getElementById('yr').innerHTML = "<b>"+document.getElementById('years').value+"</b>&nbsp"+of_years;
			document.getElementById('res').innerHTML = "<input type='button' onclick='resetPlanned()' value='"+unset_planned+"'>";
	}
}


function block_hours(type) {
	var type_shortname = type.charAt(0);
	document.getElementById('hrs_'+type_shortname).innerHTML = "<b>"+document.getElementById('close_'+type).value+"</b>";
	document.getElementById('dte_'+type_shortname).innerHTML = "<b>"+document.getElementById('closing_date_'+type).value+"</b>";
	document.getElementById('csc_'+type_shortname).innerHTML = "<button id='closing_state_change_method' onclick='UnSetClosed("+cid+", \""+type+"\")'>"+undo_close_hours+"</button>";
}

function unblock_hours(type) {
	var type_shortname = type.charAt(0);
	if (type == 'stud') {
		document.getElementById('hrs_'+type_shortname).innerHTML = "<input type='text' id='close_"+type+"' value="+hours_s+">";
		document.getElementById('dte_'+type_shortname).innerHTML = "<input type='text' id='closing_date_stud' value='"+date_s+"'>";
	} else {
		document.getElementById('hrs_'+type_shortname).innerHTML = "<input type='text' id='close_"+type+"' value="+hours_m+">";
		document.getElementById('dte_'+type_shortname).innerHTML = "<input type='text' id='closing_date_method' value='"+date_m+"'>";
	}
	
	document.getElementById('csc_'+type_shortname).innerHTML = "<button id='closing_state_change_method' onclick='setClosed("+cid+", \""+type+"\")'>"+close_hours+"</button>";
}

function resetPlanned() {
	document.getElementById('hrs').innerHTML = "<input type='text' id='plan' value='"+planned+"'>";
	var hlf_str = for_half+"&nbsp<select id='half'>";
	if (hlf == "1") {
		hlf_str = hlf_str + "<option selected value='"+hlf+"'>"+hlf+"</option>";
		hlf_str = hlf_str + "<option value='2'>2</option>";
		hlf_str = hlf_str + "<option value='0'>0</option>";
		hlf_str = hlf_str + "<option value='012'>"+All+"</option></select>&nbsp"+half;
	}
	else if (hlf == "2") {
		hlf_str = hlf_str + "<option selected value='"+hlf+"'>"+hlf+"</option>";
		hlf_str = hlf_str + "<option value='1'>1</option>";
		hlf_str = hlf_str + "<option value='0'>0</option>";
		hlf_str = hlf_str + "<option value='012'>"+All+"</option></select>&nbsp"+half;
	}
	else if (hlf=="012") {
  		hlf_str = hlf_str + "<option selected value='012'>"+All+"</option>";
  		hlf_str = hlf_str + "<option value='1'>1</option>";
  		hlf_str = hlf_str + "<option value='2'>2</option>";
  		hlf_str = hlf_str + "<option value='0'>0</option></select>&nbsp"+halfs;
  	}
	else if (hlf=="0") {
  		hlf_str = hlf_str + "<option selected value='0'>0</option>";
  		hlf_str = hlf_str + "<option value='1'>1</option>";
  		hlf_str = hlf_str + "<option value='2'>2</option>";
  		hlf_str = hlf_str + "<option value='012'>"+All+"</option></select>&nbsp"+halfs;
  	}
	else {
		hlf_str = hlf_str + "<option value='1'>1</option>";
		hlf_str = hlf_str + "<option value='2'>2</option>";
		hlf_str = hlf_str + "<option value='0'>0</option>";
		hlf_str = hlf_str + "<option value='012'>"+All+"</option></select>&nbsp"+half;
	}
	document.getElementById('hlf').innerHTML = hlf_str;
	
	var y = year.split("/");
	var y_str = "<select id='years'><option value='"+y[0]+"/"+y[1]+"'>"+y[0]+"/"+y[1]+"</option>";
		y_str = y_str + "<option value='"+(parseInt(y[0])-1)+"/"+(parseInt(y[1])-1)+"'>"+(parseInt(y[0])-1)+"/"+(parseInt(y[1])-1)+"</option>";
		y_str = y_str + "<option value='"+(parseInt(y[0])+1)+"/"+(parseInt(y[1])+1)+"'>"+(parseInt(y[0])+1)+"/"+(parseInt(y[1])+1)+"</option>";
		y_str = y_str + "</select>";
	
	document.getElementById('yr').innerHTML = y_str;
	
	document.getElementById('res').innerHTML = "<input type='button' onclick='setPlanned("+cid+")' value='"+set_planned+"'>";
}


function closing_stateChange() {
	document.getElementById('closing_state_change').disabled = true;
	if (asyncRequest.responseText == 'Done!') {
		document.getElementById('closing_state_change').onclick = 'undo_close_h()';
		document.getElementById('closing_state_change').innerHTML = undo_close_hours;
		document.getElementById('closing_state_change').disabled = false;
	}
}

function unclosing_stateChange() {
	document.getElementById('closing_state_change').disabled = true;
	if (asyncRequest.responseText == 'Done!') {
		document.getElementById('closing_state_change').onclick = 'close_h()';
		document.getElementById('closing_state_change').innerHTML = close_hours;
		document.getElementById('closing_state_change').disabled = false;
	}
}