<?php

// get the script execution start time
$time_start = microtime(true);

// all fairly essential ;)
require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/filelib.php');
require_once('lib.php');

admin_externalpage_setup('reportdetailedreview');
admin_externalpage_print_header();
echo "\n\n".'<link rel="stylesheet" type="text/css" href="styles.css" />'."\n";

/**
 * set some vars
 */
// number of lines before the header is repeated

// links
$link_course        = $CFG->wwwroot.'/course/view.php?id=';
$link_course_files  = $CFG->wwwroot.'/files/index.php?id=';

// set up the page
print_heading(get_string('detailedreview', 'report_detailedreview'));

// some useful functions
function of_print_headers() {
    echo '    <tr>'."\n";
    echo '        <th class="of">'.get_string('index_table_no', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_teacher', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_courses', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_hours', 'report_detailedreview').'</th>'."\n";
    echo '    </tr>'."\n";
}

echo "<ul class='ep_tm_menu'>
<li><img id ='recalcer' src='recalc.jpg' onclick='recalc()' style='width: 18px; height: 18px; position:absolute;'/>__</li>
<li><a href='byteacher.php'>".get_string('head_byteacher', 'report_detailedreview')."</a></li>
<li><a href='index.php'>".get_string('head_bycourse', 'report_detailedreview')."</a></li>
<li><a href='bycategory.php'>".get_string('head_bycategory', 'report_detailedreview')."</a></li>
<li><a href='bonus.php'>".get_string('head_add', 'report_detailedreview')."</a></li>
<li><ai>".get_string('head_coef', 'report_detailedreview')."</ai></li>
</ul>";

echo "<script>
var asyncRequest;

function recalc()
{
document.getElementById('recalcer').src='recalc1.jpg';
document.getElementById('recalcer').onclick='';
try
{
//document.getElementById('loading').style.display='block';
asyncRequest = new XMLHttpRequest();
asyncRequest.onreadystatechange = stateChange;
asyncRequest.open('POST', 'recalc.php', true);
asyncRequest.send(null);
}

catch(exception)
{
alert('Request Failed.');
}
}

function stateChange()
{
document.getElementById('loading').style.display='block';

if (asyncRequest.responseText == 'Done!') {
document.getElementById('loading').style.display='none';
document.getElementById('recalcer').src='recalc.jpg';
document.location.reload(true);
}
}

</script>";

echo "<img id='loading' style='display:block' src='159.gif'/><br>";

if ($_POST["firsttime"]=='false') {
	$error=false;
	$sql_type="SELECT type FROM mdl_hours_cost";
	$tps=$DB->get_records_sql($sql_type);
	foreach($tps as $types) {
		if ((!(is_float(floatval($_POST['coef_'.$types->type]))))||((floatval($_POST['coef_'.$types->type] == 0)&&($_POST['coef_'.$types->type] != '0'))))
		{
			echo "<div class='box errorbox errorboxcontent'>".get_string('coef_error_in_val', 'report_detailedreview')." ".get_string($types->type, 'report_detailedreview')." = <b>".$_POST['coef_'.$types->type]."</b></div>";
			$error=true;
		}
	}

	if (!$error) {
		$sql_set="";
		foreach($tps as $types) {
			$sql_set=$sql_set."UPDATE mdl_hours_cost SET cost = ".floatval($_POST['coef_'.$types->type])." WHERE type = '".$types->type."';";
		}
		execute_sql($sql_set);
	}
}


$sql_get="SELECT type,cost FROM mdl_hours_cost ORDER BY type";
$vals=$DB->get_recordset_sql($sql_get);

echo "<br><form action='coef.php' method='POST'>";
echo "<table>";
foreach ($vals as $values) {

	echo "<tr class='ep_tm_coef'><td><b>".get_string('coef_'.$values->type, 'report_detailedreview')."</b></td><td><input type='text' name='coef_".$values->type."' size='3' value='".$values->cost."' onkeypress='if(((event.keyCode < 48)||(event.keyCode > 57))&&(event.keyCode != 46)) return false'>".get_string('coef_h_postfix', 'report_detailedreview')."</td></tr>";
}

echo	"<input type='hidden' name='firsttime' id='firsttime' value='false'/>";
echo "<tr><td></td><td><input type='submit' value='".get_string('coef_commit', 'report_detailedreview')."'></td></tr>";
echo "</table></form>";
//echo "<div style='color: #A00;text-align: right;'>".get_string('bonus_req', 'report_detailedreview')."<img alt='������������ ����' src='../../../pix/req.gif'></div>";



/*

$test = preg_match('/[<>\"\"\'\'\bINSERT\bUPDATE\bSET\bDROP\bDELETE\binsert\bupdate\bset\bdrop\bdelete]/',$_POST["comment"]);
//��������� �����
$mins = "0.".$_POST["mins"];
$mins = round($mins*100)/100;
$mins = substr($mins,2);
if ($mins=='') $mins=0;
$hours = ltrim($_POST["hours"],'0');
if ($hours=='') $hours=0;

if ((is_number(ltrim($_POST["course"],'0'))!='')&((is_number(ltrim($hours,'0'))!='')|($hours==0))&((is_number(ltrim($mins,'0'))!='')|($mins==0))&($_POST["course"]!='')&($_POST["hours"]!='')&($_POST["comment"]!='')&($test!=1)&(($hours+mins) > 0)) {
	//��������� ����� id
	$sql_get_maxid = "SELECT MAX(id) FROM mdl_hours_bonus";
	$m = get_recordset_sql($sql_get_maxid);
	foreach ($m as $mid) $maxid = $mid['max'];
	$newid = $maxid + 1;
	$bonus_sql="INSERT INTO mdl_hours_bonus VALUES (".$newid.",".$_POST["course"].",".round(microtime(true)).",".$hours.".".$mins.",'".$_POST["comment"]."')";
	execute_sql($bonus_sql);

} else if ($_POST["firsttime"]=='false') { 
	if ((is_number($_POST["course"])=='')|($_POST["course"]=='')) {
		echo "<div class='box errorbox errorboxcontent'>".get_string('course_error', 'report_detailedreview')."</div>";
	}
	if (((is_number(ltrim($hours,'0'))!='')&($hours==0))|($_POST["hours"]=='')|((is_number(ltrim($mins,'0'))!='')&($mins==0))|($_POST["mins"]=='')|(($hours+mins) <= 0)) {
		echo "<div class='box errorbox errorboxcontent'>".get_string('hours_error', 'report_detailedreview')."</div>";
	}
	if ($test==1) {
		echo "<div class='box errorbox errorboxcontent'>".get_string('comment_error', 'report_detailedreview')."</div>";
	}
	if ($_POST["comment"]=='') {
		echo "<div class='box errorbox errorboxcontent'>".get_string('comment_empty', 'report_detailedreview')."</div>";
	}
	echo "<script>
	document.getElementById('cid').value = '".$_POST['course']."';
	document.getElementById('id_hours').value = '".$_POST['hours']."';
	document.getElementById('id_mins').value = '".$mins."';
	document.getElementById('id_comment').value = '".$_POST['comment']."';
	</script>";
	
}*/

$j=$repeat_header;


//print_footer();
admin_externalpage_print_footer();
echo "<script type='text/javascript'>
document.getElementById('loading').style.display='none';
</script>";
?>