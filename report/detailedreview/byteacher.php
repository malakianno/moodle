<?php

// get the script execution start time
$time_start = microtime(true);

// all fairly essential ;)
require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/filelib.php');
require_once('lib.php');

require_login();
$PAGE->set_url($CFG->wwwroot."/report/detailedreview/byteacher.php");

admin_externalpage_setup('reportdetailedreview');
echo $OUTPUT->header();
echo "\n\n".'<link rel="stylesheet" type="text/css" href="styles.css" />'."\n";

/**
 * set some vars
 */
// number of lines before the header is repeated
$repeat_header = 50;
// links
$link_course        = $CFG->wwwroot.'/course/view.php?id=';
$link_course_files  = $CFG->wwwroot.'/files/index.php?id=';

// set up the page
echo $OUTPUT->heading(get_string('detailedreview', 'report_detailedreview'));

function check_course_lastaccess($courseid,$userid, $DB) { //���� ��������������, � ������� ���� ����, �� ��� ���� �� ������
	$course_user_lastaccess_sql = "SELECT timeaccess FROM mdl_user_lastaccess WHERE courseid=$courseid AND userid=$userid";
	$lastacc = $DB->get_records_sql($course_user_lastaccess_sql);
	$arr_last=0;
	foreach ($lastacc as $last)
	$arr_last = $arr_last + sizeof($last);
	return $arr_last;
}


// some useful functions
function of_print_headers() {
    echo '    <tr>'."\n";
    echo '        <th class="of">'.get_string('index_table_no', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_teacher', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_courses', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_hours', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of" style="width:50px">'.get_string('by_plan', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of" style="width:50px">'.get_string('closed_stud_hours', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of" style="width:50px">'.get_string('closed_method_hours', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_sum_hours', 'report_detailedreview').'</th>'."\n";
    echo '    </tr>'."\n";
}

// get the courses as a list
//$res = get_records('course', '', '', 'id', 'id, fullname, shortname, timecreated, timemodified, visible', '', '');
echo "<ul class='ep_tm_menu'>
<li><img id ='recalcer' src='recalc.jpg' onclick='recalc()' style='width: 18px; height: 18px; position:absolute;'/>__</li>
<li><ai>".get_string('head_byteacher', 'report_detailedreview')."</ai></li>
<li><a href='index.php'>".get_string('head_bycourse', 'report_detailedreview')."</a></li>
<li><a href='bycategory.php'>".get_string('head_bycategory', 'report_detailedreview')."</a></li>
<li><a href='bonus.php'>".get_string('head_add', 'report_detailedreview')."</a></li>
<li><a href='coef.php'>".get_string('head_coef', 'report_detailedreview')."</a></li>

</ul>";

echo "<script>
var asyncRequest;

function recalc()
{
	document.getElementById('recalcer').src='recalc1.jpg';
	document.getElementById('recalcer').onclick='';
	try
	{
		//document.getElementById('loading').style.display='block';
		asyncRequest = new XMLHttpRequest();
		asyncRequest.onreadystatechange = stateChange;
		asyncRequest.open('POST', 'recalc.php', true);
		asyncRequest.send(null);
	}
	
	catch(exception)
	{
		alert('Request Failed.');
	}
}

function stateChange()
{	
		document.getElementById('loading').style.display='block';
		
		if (asyncRequest.responseText == 'Done!') {
			document.getElementById('loading').style.display='none';
			document.getElementById('recalcer').src='recalc.jpg';
			document.location.reload(true);
		}
}

</script>";

echo '<table class="of">'."\n";

echo "<img id='loading' style='display:block' src='159.gif'/><br>";

$j=$repeat_header;
$k=0;
//foreach($res as $row) {
  /*  if($row->visible == 1) {
        echo '<tr>'."\n";
    } else {
        echo '<tr class="hidden">'."\n";
    }
*/

    // print the table headers every $repeat_header rows
        of_print_headers();
    $sql_name="SELECT DISTINCT (id),lastname,firstname,lastaccess 
				FROM mdl_user WHERE id IN (
						SELECT userid FROM mdl_role_assignments 
						WHERE roleid = 2
						) 
						AND lastaccess <> 0 
					ORDER BY lastname";
    $names = $DB->get_records_sql($sql_name);
    foreach ($names as $nam) {
    	$sum_hours = 0;
    	$pay = 0;
    	//����� �� ���� ��� ����� �������������
    	if ($nam->lastaccess!=0) { //���� ���� ������� �� ������ � �������, �� �� ���� �����
    		$sql_courses="SELECT id,fullname FROM mdl_course WHERE id IN (SELECT instanceid FROM mdl_context WHERE mdl_context.id IN (SELECT contextid FROM mdl_role_assignments WHERE userid=".$nam->id." AND roleid=2) AND mdl_context.contextlevel=50)";
    		$courses = $DB->get_records_sql($sql_courses);
    	$arr_size=0;
  
    	foreach ($courses as $course){
		//	if (check_course_lastaccess($course->id, $nam->id, $DB) > 0) {//���� �� ������ ��� ���� ������� � �����
    	$arr_size = $arr_size + sizeof($course);   	    	
    	//$pay = $pay + get_course_modules($course->id,true, $DB); //� ������� ������� ���������
		//	}
    	}
   	$arr_courses = array();
   	$arr_hours = array();
   	$arr_closed_stud = array();
   	$arr_closed_method = array();
   	$arr_planned = array();
    	
   	echo '    <td class="of center">'.number_format(++$k, 0).'</td>'."\n";
   	echo '    <td class="of"><a href="../../../user/view.php?id='.$nam->id.'&course=1?">'.$nam->lastname.' '.$nam->firstname.'</a></td>'."\n";
   	echo '    <td class="of">';
   	
    	if ($arr_size > 0) {
 	
			foreach ($courses as $course)
			//	if (check_course_lastaccess($course->id, $nam->id, $DB) > 0) 
			{
					$coursename = $course->fullname;
					
					if (strlen($coursename)>60) {
    				$coursename=implode(array_slice(explode('<br>',wordwrap($coursename,60,'<br>',false)),0,1));
						$coursename .="...";
					}
					if (is_closed($course->id)) {
						$arr_courses[] = "<div style='width: 100%; height: 0px;border-color: #DDD; border-width: 0 0 1px;; border-style: solid;'><a id='course_".$course->id."' style='color: #2e8b57;' class='closed' href='../../../course/view.php?id=".$course->id."'>".$coursename."</a></div><br>";
						
					} else {
						$arr_courses[] = "<div style='width: 100%; height: 0px;border-color: #DDD; border-width: 0 0 1px;; border-style: solid;'><a id='course_".$course->id."'  href='../../../course/view.php?id=".$course->id."'>".$coursename."</a></div><br>";
					}
			}
    		//preparing SQL
			
			
    		foreach ($courses as $course)
    		//	if (check_course_lastaccess($course->id, $nam->id, $DB) > 0) 
    			{
    				if (is_closed($course->id)) {
    					$pay = get_course_hours($course->id);
    					$sum_hours+=$pay;
    					
    					$hours_s = 0;
    					$hours_m = 0;
    					$gcsh = getClosedHours($course->id,'stud');
    					if ($gcsh)
    					foreach($gcsh as $CH) $hours_s = $CH['hours'];
    					$gcsh = getClosedHours($course->id,'method');
    					if ($gcsh)
   						foreach($gcsh as $CH) $hours_m = $CH['hours'];
   						
   						$planned  = 0;
   						$gpl = getPlanned($course->id);
   						if ($gpl)
   						foreach ($gpl as $pl) $planned = $pl['hours'];

//    					$arr_hours[] =  "<div style='width: 100%; height: 0px;border-color: #DDD; border-width: 0 0 1px; border-style: solid;'><a id='hours_".$course->id."' oncontextmenu='return false;' class='closed' style='color: #2e8b57;' href='report.php?cid=".$course->id."'>".$pay."</a></div><br>";
    					$arr_hours[] =  "<div style='width: 100%; height: 0px;border-color: #DDD; border-width: 0 0 1px; border-style: solid;'><a id='hours_".$course->id."' class='closed' style='color: #2e8b57;' href='report.php?cid=".$course->id."'>".$pay."</a></div><br>";
    					$arr_closed_stud[] = "<div style='width: 100%; height: 0px;border-color: #DDD; border-width: 0 0 1px; border-style: solid;'><a id='cl_s_".$course->id."' class='closed' style='color: #2e8b57;' href='report.php?cid=".$course->id."'>".$hours_s."</a></div><br>";
    					$arr_closed_method[] = "<div style='width: 100%; height: 0px;border-color: #DDD; border-width: 0 0 1px; border-style: solid;'><a id='cl_m_".$course->id."' class='closed' style='color: #2e8b57;' href='report.php?cid=".$course->id."'>".$hours_m."</a></div><br>";
    					$arr_planned[] = "<div style='width: 100%; height: 0px;border-color: #DDD; border-width: 0 0 1px; border-style: solid;'><a id='cl_s_".$course->id."' class='closed' style='color: #2e8b57;' href='report.php?cid=".$course->id."'>".$planned."</a></div><br>";
    				} else {
    				$pay = get_course_hours($course->id);
    				$sum_hours+=$pay;
    				$hours_s = 0;
    				$hours_m = 0;
    					$gcsh = getClosedHours($course->id,'stud');
    					if ($gcsh)
    					foreach($gcsh as $CH) $hours_s = $CH['hours'];
    					$gcsh = getClosedHours($course->id,'method');
    					if ($gcsh)
   						foreach($gcsh as $CH) $hours_m = $CH['hours'];
    				
    				$planned  = 0;
    					$gpl = getPlanned($course->id);
    					if ($gpl)
   						foreach ($gpl as $pl) $planned = $pl['hours'];
    				
//   					$arr_hours[] =  "<div style='width: 100%; height: 0px;border-color: #DDD; border-width: 0 0 1px;; border-style: solid;'><a id='hours_".$course->id."' name='hours' oncontextmenu='return false;' href='report.php?cid=".$course->id."'>".$pay."</a></div><br>";
   					$arr_hours[] =  "<div style='width: 100%; height: 0px;border-color: #DDD; border-width: 0 0 1px;; border-style: solid;'><a id='hours_".$course->id."' name='hours' href='report.php?cid=".$course->id."'>".$pay."</a></div><br>";
    				$arr_closed_stud[] = "<div style='width: 100%; height: 0px;border-color: #DDD; border-width: 0 0 1px;; border-style: solid;'><a id='hours_".$course->id."' name='hours' href='report.php?cid=".$course->id."'>".$hours_s."</a></div><br>";
    				$arr_closed_method[] = "<div style='width: 100%; height: 0px;border-color: #DDD; border-width: 0 0 1px;; border-style: solid;'><a id='hours_".$course->id."' name='hours' href='report.php?cid=".$course->id."'>".$hours_m."</a></div><br>";
    				$arr_planned[] = "<div style='width: 100%; height: 0px;border-color: #DDD; border-width: 0 0 1px;; border-style: solid;'><a id='hours_".$course->id."' name='hours' href='report.php?cid=".$course->id."'>".$planned."</a></div><br>";
    				}
    			}

    	}
    	
    	foreach($arr_courses as $ac) echo $ac;
    	echo "</td>";
    	 echo "<td class='of center nowrap'>";
    	 foreach($arr_hours as $ah) echo $ah;
    	 echo "</td>";
    	 echo "<td class='of center nowrap'>";
    	 foreach($arr_planned as $cs) echo $cs;
    	 echo "</td>";
    	 echo "<td class='of center nowrap'>";
    	 foreach($arr_closed_stud as $cs) echo $cs;
    	 echo "</td>";
    	 echo "<td class='of center nowrap'>";
    	 foreach($arr_closed_method as $cm) echo $cm;
    	 echo "</td><td class='of center nowrap'>".$sum_hours."</td>";
    	 echo '</tr>'."\n";
    }
	}

echo '</table>'."\n";

echo "<script type='text/javascript'>
document.getElementById('loading').style.display='none';
</script>";

//echo "<script type='text/javascript' src='".$CFG->wwwroot."/report/detailedreview/script.js'></script>";


// get the script execution end time (more or less)
$time_end = microtime(true);
echo '<p>'.get_string('execution1', 'report_detailedreview').number_format(($time_end-$time_start), 3).get_string('execution2', 'report_detailedreview').'</p>'."\n";

//print_footer();
echo $OUTPUT->footer();

?>