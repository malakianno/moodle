<?php

// get the script execution start time
$time_start = microtime(true);

// all fairly essential ;)
require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/filelib.php');

require_login();

require_capability('moodle/site:viewreports', get_context_instance(CONTEXT_SYSTEM));

admin_externalpage_setup('reportdetailedreview');
echo $OUTPUT->header();
echo "\n\n".'<link rel="stylesheet" type="text/css" href="styles.css" />'."\n";

/**
 * set some vars
 */
// number of lines before the header is repeated
$repeat_header = 500;
// links
$link_course        = $CFG->wwwroot.'/course/view.php?id=';
$link_course_files  = $CFG->wwwroot.'/files/index.php?id=';

// set up the page
echo $OUTPUT->heading(get_string('detailedreview', 'report_detailedreview'));



// some useful functions
function of_print_headers() {
    echo '    <tr>'."\n";
    echo '        <th class="of">'.get_string('index_table_no', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_course', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_full', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_teacher', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_time', 'report_detailedreview').'</th>'."\n";
    echo '        <th class="of">'.get_string('index_table_goto', 'report_detailedreview').'</th>'."\n";
    echo '    </tr>'."\n";
}

// get the courses as a list

echo "<ul class='ep_tm_menu'>
<li><img id ='recalcer' src='recalc.jpg' onclick='recalc()' style='width: 18px; height: 18px; position:absolute;'/>__</li>
<li><a href='byteacher.php'>".get_string('head_byteacher', 'report_detailedreview')."</a></li>
<li><ai>".get_string('head_bycourse', 'report_detailedreview')."</ai></li>
<li><a href='bycategory.php'>".get_string('head_bycategory', 'report_detailedreview')."</a></li>
<li><a href='bonus.php'>".get_string('head_add', 'report_detailedreview')."</a></li>
<li><a href='coef.php'>".get_string('head_coef', 'report_detailedreview')."</a></li>
</ul>";

echo "<script>
var asyncRequest;

function recalc()
{
document.getElementById('recalcer').src='recalc1.jpg';
document.getElementById('recalcer').onclick='';
try
{
//document.getElementById('loading').style.display='block';
asyncRequest = new XMLHttpRequest();
asyncRequest.onreadystatechange = stateChange;
asyncRequest.open('POST', 'recalc.php', true);
asyncRequest.send(null);
}

catch(exception)
{
alert('Request Failed.');
}
}

function stateChange()
{
document.getElementById('loading').style.display='block';

if (asyncRequest.responseText == 'Done!') {
document.getElementById('loading').style.display='none';
document.getElementById('recalcer').src='recalc.jpg';
document.location.reload(true);
}
}

</script>";

echo "<img id='loading' style='display:block' src='159.gif'/><br>";

echo '<table class="of">'."\n";

$j=$repeat_header;
$k=0;
$sql_courses = "SELECT id, fullname, shortname, timecreated, timemodified, visible FROM mdl_course";
$res = $DB->get_records_sql($sql_courses);
foreach($res as $row) {
	if($row->visible == 1) {
        echo '<tr>'."\n";
    } else {
        echo '<tr class="hidden">'."\n";
    }

    // print the table headers every $repeat_header rows
    if($j == $repeat_header) {
        of_print_headers();
        $j = 1;
    } else {
        $j++;
    }
    //getting teachers, assigned right onto the course
    $sql_name="SELECT lastname,firstname FROM mdl_user WHERE id IN (SELECT userid FROM mdl_role_assignments WHERE contextid IN (SELECT id FROM mdl_context WHERE instanceid=$row->id AND contextlevel=50) AND roleid =2)";
    $names = $DB->get_records_sql($sql_name);

    $name= '';
    $i=0;
    foreach ($names as $nam) 
	$name=$name.$nam->lastname.' '.$nam->firstname.'<br>';
 
    if ($name=='') {
    	//getting course path
    	$sql_path = "SELECT path 
					FROM mdl_course_categories
					WHERE id IN (
						SELECT category 
						FROM mdl_course
						WHERE id = $row->id)";
	    //getting and parsing path
    	$path = explode("/",$DB->get_field_sql($sql_path));
	    foreach($path as $category) {
    		if ($category!='') {
    		$sql_name="SELECT lastname,firstname
    					FROM mdl_user
    					WHERE id IN (
	    					SELECT userid
    						FROM mdl_role_assignments
    						WHERE contextid IN (
	    						SELECT id
    							FROM mdl_context
    							WHERE instanceid=$category)
    						AND roleid=2)";
    		$names = $DB->get_records_sql($sql_name);
    		foreach ($names as $nam)
	    		$name=$name.$nam->lastname.' '.$nam->firstname.'<br>';
    		}
    	} 
    }
    
    
    
    echo '    <td class="of center">'.number_format(++$k, 0).'</td>'."\n";
    echo '    <td class="of center">'.$row->id.'</td>'."\n";
    echo '    <td class="of"><a href="report.php?cid='.$row->id.'">'.$row->fullname.'</a></td>'."\n";
    echo '    <td class="of">'.$name.'</td>'."\n";
    echo '    <td class="of center nowrap">'.date(get_string('date_format', 'report_detailedreview'), $row->timemodified).'</td>'."\n";
    echo '    <td class="of center nowrap"><a title="'.get_string('index_table_hover1', 'report_detailedreview').'" href="'.$link_course.$row->id.'">'.get_string('index_table_link', 'report_detailedreview').'</a> &rarr;</td>'."\n";

    echo '</tr>'."\n";
}
echo '</table>'."\n";

echo "<script type='text/javascript'>
document.getElementById('loading').style.display='none';
</script>";

// get the script execution end time (more or less)
$time_end = microtime(true);
echo '<p>'.get_string('execution1', 'report_detailedreview').number_format(($time_end-$time_start), 3).get_string('execution2', 'report_detailedreview').'</p>'."\n";

//print_footer();
echo $OUTPUT->footer();

?>