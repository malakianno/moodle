<?php
require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/filelib.php');
require_login();
require_once('lib.php');
global $DB;
$cid = required_param('cid', PARAM_INT);
$action = required_param('act', PARAM_TEXT); 

if ($action == "plan") {
	echo plan($cid);
} 

if ($action == "close_stud") {
	close_hours($cid, 'stud');
}

if ($action == "close_method") {
	close_hours($cid, 'method');
}

if ($action == "unclose_stud") {
	unClose_hours($cid, 'stud');
}

if ($action == "unclose_method") {
	unClose_hours($cid, 'method');
}

if ($action == "close_course") {
	CClose($cid);
}

if ($action == "undo_close_course") {
	undo_close_course($cid);
}

if ($action == "check_closed") {
	echo is_closed($cid);
}

function CClose($cid) {
	global $USER;
	$comment = required_param("comment", PARAM_TEXT);
	$time =  required_param("time", PARAM_INT);
	
	close_course($cid, $USER->id, $comment, $time);
}

function plan($cid) {
	
	$count = required_param("qty", PARAM_FLOAT);
	$half = required_param("h", PARAM_FLOAT);
	$year0 = required_param("y0", PARAM_FLOAT);
	$year1 = required_param("y1", PARAM_FLOAT);
	if (strlen($half) == 1)	$half_year = $half." ".$year0."/".$year1;
	else $half_year = $year0."/".$year1;
	
	$report = setPlanned($cid, $count, $half_year);
	
	return $report;
}

function close_hours($cid, $type='auto') {
	global $USER;
	$qty = required_param("qty", PARAM_FLOAT);
	$time = required_param("d", PARAM_TEXT);  
	close_course_hours($cid, $USER->id, $qty, $time, $type); 
}

function unClose_hours($cid, $type) {
		undo_close_course_hours($cid, $type);
}


?>