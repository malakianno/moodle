<?php
require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/filelib.php');
require_login();
require_once('lib.php');
global $DB;
$cid = required_param('cid', PARAM_INT);
$uid = $USER->id;

if (($cid!=null)&($cid!='')) {
	$qty = get_course_hours($cid);
	close_course_hours($cid,$uid,$qty);
}
echo "Done!";
?>