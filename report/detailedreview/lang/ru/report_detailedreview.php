﻿<?php

// general stuff
$string['date_format'] = 'M d Y, H:i';
$string['execution1'] = 'Время выполнения запроса ';
$string['execution2'] = ' секунд.';

$string['recalc']    = 'Пересчитать';

// admin panel string
$string['detailedreview']    = 'Подробные отчеты по курсам';
$string['pluginname'] = 'Подробные отчеты по курсам';
$string['detailedreview:view']    = 'Подробные отчеты по курсам';
// course modules 
$string['choice'] = 'Опрос';
$string['data'] = 'База данных';
$string['glossary'] = 'Глоссарий';
$string['hotpot'] = 'hotpot';
$string['journal'] = 'journal';
$string['label'] = 'Пояснение';
$string['lams'] = 'LAMS';
$string['lesson'] = 'Лекция';
$string['openmeetings'] = 'Вебинар';
$string['quiz'] = 'Тест';
$string['html'] = 'Веб-страница';
$string['text'] = 'Текстовая страница';
$string['file'] = 'Файл';
$string['link'] = 'Ссылка';
$string['directory'] = 'Папка';
$string['survey'] = 'Анкета';
$string['workshop'] = 'Семинар';
$string['wiki'] = 'Wiki';
$string['chat'] = 'Чат';
$string['scorm'] = 'SCORM';
$string['forum'] = 'Форум';
$string['assignment'] = 'Задание';
$string['flv'] = 'Проигрыватель';
$string['questionnaire'] = 'Анкетирование';
$string['page'] = 'Страница';
$string['url'] = 'Ссылка';
$string['folder'] = 'Папка';
$string['resource'] = 'Файл';

//table names
$string['New'] = 'Новое';
$string['Imported'] = 'Импортированное';
$string['Changed'] = 'Импортированное + измененное';
$string['Bonus'] = 'Дополнительно';

$string['course_hours']= 'Часы: ';
$string['report_hours']= 'Часы';
$string['comment']= 'Комментарий';

// table elements
$string['type'] = 'Тип';
$string['number'] = 'Количество';
$string['qty'] = 'Записей';
$string['img'] = 'Картинок';
$string['href'] = 'Ссылок';
$string['teacher_msgs'] = 'Сообщений преподавателя';
$string['student_msgs'] = 'Сообщений студентов';
$string['quiz_cat'] = 'Категорий вопросов';
$string['quest'] = 'Обычных вопросов';
$string['stud_quest'] = 'Обучающих вопросов';
$string['quiz_href'] = 'Ссылок в вопросах';
$string['quiz_img'] = 'Картинок в вопросах';
$string['card_rub'] = 'Карточек-рубрикаторов';
$string['card_quest'] = 'Карточек с вопросами';
$string['hours'] = 'Часов';
$string['hours_summary'] = 'Итого часов';
$string['hours_chapter'] = 'Итого по разделу';

//bonus page strings
$string['bonus_hours'] = 'Часы* ';
$string['bonus_cid'] = 'ID курса* ';
$string['bonus_comment'] = 'Комментарий* ';
$string['bonus_commit'] = 'Начислить ';
$string['course_error'] = 'Неверно заполнено поле <b>ID курса</b>';
$string['hours_error'] = 'Неверно заполнено поле <b>Часы</b>';
$string['comment_error'] = 'Неверно заполнено поле <b>Комментарий</b>';
$string['comment_empty'] = 'Не заполнено поле <b>Комментарий</b>';
$string['bonus_req'] = 'Обязательные для заполнения поля в этой форме помечены';

// index page strings

$string['index_table_no']       = '#';
$string['index_table_course']   = 'ID курса';
$string['index_table_full']     = 'Полное название';
$string['index_table_short']    = 'Сокр. наименование';
$string['index_table_time']     = 'Время изменения';
$string['index_table_goto']     = 'Переход';
$string['index_table_teacher']     = 'Преподаватель';
$string['index_table_link']     = 'Перейти к курсу';
$string['index_table_courses']     = 'Курсы';
$string['index_table_hours']     = 'Часы';
$string['index_table_sum_hours']     = 'Итого часов';
$string['index_table_hover1']    = 'Click to go to the Moodle course';
$string['index_table_hover2']    = 'Click to go to the Moodle course\'s files area';

//bycategory page strings
$string['bycat_table_category'] = 'Категория';
$string['bycat_table_course'] = 'Курсы';
$string['bycat_table_hours'] = 'Часы';
$string['bycat_table_teachers'] = 'Преподаватели';

// report page strings
$string['head_byteacher']     = 'По преподавателям';
$string['head_bycourse']     = 'По курсам';
$string['head_bycategory']     = 'По категориям';
$string['head_add']     = 'Доп. часы';
$string['head_coef']     = 'Нормативы';


//coef page strings
$string['coef_error_in_val']     = 'Ошибка в значении';
$string['coef_commit']     = 'Сохранить';

$string['coef_glossary']     = 'Глоссарий';
$string['coef_label']     = 'Пояснение';
$string['coef_lams']     = 'LAMS';
$string['coef_lesson']     = 'Лекция';
$string['coef_openmeetings']     = 'Вебинар';
$string['coef_quiz']     = 'Тест';
$string['coef_html']     = 'Веб-страница';
$string['coef_text']     = 'Текстовая страница';
$string['coef_file']     = 'Файл';
$string['coef_link']     = 'Ссылка';
$string['coef_directory']     = 'Папка';
$string['coef_survey']     = 'Анкета';
$string['coef_workshop']     = 'Семинар';
$string['coef_wiki']     = 'Wiki';
$string['coef_chat']     = 'Чат';
$string['coef_scorm']     = 'SCORM';
$string['coef_forum']     = 'Форум';
$string['coef_assignment']     = 'Задание';
$string['coef_wiki_qty']     = 'Запись в Wiki';
$string['coef_glossary_qty']     = 'Запись в глоссарии';
$string['coef_img']     = 'Изображение в элементе курса';
$string['coef_href']     = 'Ссылка в элементе курса';
$string['coef_forum_teacher_msgs']     = 'Cообщение в форуме (преподаватель)';
$string['coef_forum_student_msgs']     = 'Cообщение в форуме (студент)';
$string['coef_chat_teacher_msgs']     = 'Cообщение в чате (преподаватель)';
$string['coef_chat_student_msgs']     = 'Cообщение в чате (студент)';
$string['coef_quiz_cat']     = 'Категория вопросов в тесте';
$string['coef_quest']     = 'Обычный вопрос в тесте';
$string['coef_stud_quest']     = 'Обучающий вопрос в тесте';
$string['coef_quiz_href']     = 'Ссылка в тесте';
$string['coef_quiz_img']     = 'Изображение в тесте';
$string['coef_card_rub']     = 'Карточка-рубрикатор';
$string['coef_card_quest']     = 'Карточка с вопросами';
$string['coef_changed']     = 'Модификация элемента курса после импорта';
$string['coef_flv']     = 'Проигрыватель (FLV-модуль)';
$string['coef_imported']     = 'Импорт элемента курса';
$string['coef_choice']     = 'Опрос';
$string['coef_data']     = 'База данных';
$string['coef_page']     = 'Веб-страниц(page)';
$string['coef_questionnaire']     = 'Анкетирование(устар)';
$string['coef_h_postfix'] = 'ч.';


$string['report_title']     = 'Обзор курса ';

$string['report_back']      = 'Назад';
$string['report_intro']     = 'If you want to change courses quickly, replace the number on the end of the URL.';
$string['report_note']      = 'The following files are probably orphaned, as they exist on disc but are not referenced from the course (database). If you want to open the file, simply click on it. :)';

$string['report_none']      = 'No orphaned files were found for this course. Excellent.';

$string['report_table_no']      = '#';
$string['report_table_file']    = 'File Path & Name';
$string['report_table_filesize']    = 'File Size';

$string['report_saving']    = 'If deleted, the following amount of space would be saved: ';
$string['report_saving_note']    = '. (Note that this does not include course backups or the \'moddata\' folder, which can hold gigabytes of data.)';

$string['report_gotocourse']    = 'Go to Course Files';

$string['close_hours'] = 'Закрыть почасовку';

?>