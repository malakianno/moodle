<?php

// get the script execution start time
$time_start = microtime(true);

// all fairly essential ;)
require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/filelib.php');
require_once('lib.php');

admin_externalpage_setup('reportdetailedreview');
echo $OUTPUT->header();
echo "\n\n".'<link rel="stylesheet" type="text/css" href="styles.css" />'."\n";

/**
 * set some vars
 */
// number of lines before the header is repeated

// links
$link_course        = $CFG->wwwroot.'/course/view.php?id=';

// set up the page
echo $OUTPUT->heading(get_string('detailedreview', 'report_detailedreview'));

// some useful functions

echo "<ul class='ep_tm_menu'>
<li><img id ='recalcer' src='recalc.jpg' onclick='recalc()' style='width: 18px; height: 18px; position:absolute;'/>__</li>
<li><a href='byteacher.php'>".get_string('head_byteacher', 'report_detailedreview')."</a></li>
<li><a href='index.php'>".get_string('head_bycourse', 'report_detailedreview')."</a></li>
<li><ai>".get_string('head_bycategory', 'report_detailedreview')."</ai></li>
<li><a href='bonus.php'>".get_string('head_add', 'report_detailedreview')."</a></li>
<li><a href='coef.php'>".get_string('head_coef', 'report_detailedreview')."</a></li>
</ul>
";

echo "<script>
var asyncRequest;

function recalc()
{
document.getElementById('recalcer').src='recalc1.jpg';
document.getElementById('recalcer').onclick='';
try
{
//document.getElementById('loading').style.display='block';
asyncRequest = new XMLHttpRequest();
asyncRequest.onreadystatechange = stateChange;
asyncRequest.open('POST', 'recalc.php', true);
asyncRequest.send(null);
}

catch(exception)
{
alert('Request Failed.');
}
}

function stateChange()
{
document.getElementById('loading').style.display='block';

if (asyncRequest.responseText == 'Done!') {
document.getElementById('loading').style.display='none';
document.getElementById('recalcer').src='recalc.jpg';
document.location.reload(true);
}
}

</script>";

$avg_hours = 0;
$avg_num = 0;

function has_children($catid) {
	global $DB;
	$sql_child = "SELECT count(id) FROM mdl_course_categories WHERE parent = $catid"; 
	$ch = $DB->get_recordset_sql($sql_child);
	foreach($ch as $chil)
		$childs = $chil->count;
	if ($childs > 0) return true;
	else return false;
}

function has_courses($catid) {
	global $DB;
	$sql_child = "SELECT count(id) FROM mdl_course WHERE category = $catid";
	$ch = $DB->get_recordset_sql($sql_child);
	foreach($ch as $chil)
	$childs = $chil->count;
	if ($childs > 0) return true;
	else return false;
}

function show_courses($catid,$depth) {
	global $DB;
	global $avg_hours;
	global $avg_num;
	$space = $depth * 16;
	$sql_courses = "SELECT id, fullname, visible FROM mdl_course WHERE category = $catid";
	$cour = $DB->get_recordset_sql($sql_courses);
	echo "<div style='display:none;  margin-left:".$space."px' id='cor_".$catid."'><table class='of'>";
	echo "<tr><th class='of'>".get_string('bycat_table_course', 'report_detailedreview')."</th><th class='of'>".get_string('bycat_table_teachers', 'report_detailedreview')."</th><th class='of'>".get_string('bycat_table_hours', 'report_detailedreview')."</th></tr>";
	foreach ($cour as $course) {
		if ($course->visible == 1) echo "<tr>";
		else echo "<tr class='hidden'>";
		echo "<td class='of'><a href='../../../course/view.php?id=".$course->id."'>".$course->fullname."</a></td><td class='of'>";
		/*$sql_teachers = "SELECT 
  							mdl_user.id, 
  							  mdl_user.firstname, 
  							  mdl_user.lastname
  							FROM 
  							  public.mdl_user, 
  							  public.mdl_role_assignments, 
  							  public.mdl_course, 
  							  public.mdl_context
  							WHERE 
  							  mdl_role_assignments.userid = mdl_user.id AND
  							  mdl_role_assignments.contextid = mdl_context.id AND
						      mdl_role_assignments.roleid = 2 AND
						      mdl_context.contextlevel = 50 AND
  							  mdl_course.id = ".$course->id." AND
  							  mdl_context.instanceid = ".$course->id;
		$t = $DB->get_recordset_sql($sql_teachers);*/
		$context = get_context_instance(CONTEXT_COURSE, $course->id);
		$t = get_role_users(2, $context);
		$teachersnum = 0;
		foreach ($t as $teacher) {
			echo "<a href='../../../user/view.php?id=".$teacher->id."'>".$teacher->lastname." ".$teacher->firstname."</a><br>";
			$teachersnum++;
		}
	/*	if ($teachersnum==0) {
			//getting course path
			$sql_path = "SELECT path
			FROM mdl_course_categories
			WHERE id IN (
			SELECT category
			FROM mdl_course
			WHERE id = $course->id)";
			//getting and parsing path
			$path = explode("/",$DB->get_field_sql($sql_path));
					foreach($path as $category) {
			if ($category!='') {
			$sql_name="SELECT id,lastname,firstname
				FROM mdl_user
				WHERE id IN (
				SELECT userid
				FROM mdl_role_assignments
				WHERE contextid IN (
				SELECT id
				FROM mdl_context
				WHERE instanceid=$category)
				AND roleid = 2)";
				$names = $DB->get_records_sql($sql_name);
				foreach ($names as $nam)
					echo "<a href='../../../user/view.php?id=".$nam->id."'>".$nam->lastname." ".$nam->firstname."</a><br>";
				//$name=$name.$nam->lastname.' '.$nam->firstname.'<br>';
			}
			}
			}*/
		
		$gch = get_course_hours($course->id);
		if ($gch > 0.15) {
			$avg_hours += $gch;
			$avg_num++;
		}
		echo "</td><td class='of'><a id='course_".$course->id."'href='report.php?cid=".$course->id."'>".$gch."</a></td></tr>";
		
			

	}
	echo "</table></div>";
}

function show_children($catid,$depth) {
	global $DB;
	$space1 = "&nbsp&nbsp&nbsp&nbsp";
	if ($depth >= 2) {
		$space='';
		for ($s=2;$s<=$depth;$s++) {
			$space = $space.$space1;
		}
	}	
	$sql_srows="SELECT id,name FROM mdl_course_categories WHERE depth=$depth AND parent=$catid ORDER BY name";
	$scat=$DB->get_recordset_sql($sql_srows);
	echo "<div style='display:none' id='cat_".$catid."'>";
	if (has_courses($catid)) show_courses($catid,$depth);
	foreach ($scat as $scats) {
		echo "<div style='display:block'>$space<img id ='img_cat_".$scats->id."' value='closed' onclick=rotate(".$scats->id.")  src='arrow_closed.png'/>&nbsp<a href='/course/category.php?id=".$scats->id."'>".$scats->name."</a></div>";
		if (has_children($scats->id)) show_children($scats->id,$depth+1);
		else if (has_courses($scats->id)) show_courses($scats->id,$depth);
	}
	echo "</div>";
}

echo "<img id='loading' style='display:block' src='159.gif'/><br>";	
	$sql_rows="SELECT id,name FROM mdl_course_categories WHERE depth=1 ORDER BY name";
	$cat=$DB->get_recordset_sql($sql_rows);
	
	foreach ($cat as $cats){
		echo "<div style='display:block'>&nbsp<img id ='img_cat_".$cats->id."' value='closed' onclick=rotate(".$cats->id.")  src='arrow_closed.png'/>&nbsp<a href='/course/category.php?id=".$cats->id."'>".$cats->name."</a>";
		if (has_children($cats->id)) show_children($cats->id,2);
		if (has_courses($cats->id)) show_courses($cats->id,1);
	echo "</div>";	
	}
echo "<script type='text/javascript'>

document.getElementById('loading').style.display='none';

function rotate(num) {
	//alert(document.getElementById('course_121').innerHTML);
	if (document.getElementById('img_cat_'+num).src=='$CFG->wwwroot/report/detailedreview/arrow_closed.png') {
		document.getElementById('img_cat_'+num).src='arrow_open.png';
		try {document.getElementById('cor_'+num).style.display='block';}
		catch (e) {};
		try {document.getElementById('cat_'+num).style.display='block';}
		catch (e) {};
		
	}
	else {
		document.getElementById('img_cat_'+num).src='arrow_closed.png';
		try {document.getElementById('cat_'+num).style.display='none';}
		catch (e) {};
		try {document.getElementById('cor_'+num).style.display='none';}
		catch (e) {};
	}
}
</script>";	
	
// get the script execution end time (more or less)
$time_end = microtime(true);
echo '<p>'.get_string('execution1', 'report_detailedreview').number_format(($time_end-$time_start), 3).get_string('execution2', 'report_detailedreview').'</p>'."\n";
echo '<p>average_hours : '.$avg_hours/$avg_num.'</p>';
//print_footer();
echo $OUTPUT->footer();

?>